$(document).ready(function () {
    /*String.prototype.endsWith = function(pattern) {
     var d = this.length - pattern.length;
     return d >= 0 && this.lastIndexOf(pattern) === d;
     };
     // If cookie is set, scroll to the position saved in the cookie.
     if(((window.location.href.prototype.endsWith("/agents"))&&(document.referrer.includes("/agents/listing/page-")))||//from 1 to 2,3,4...
     ((window.location.href.includes("/agents/listing/page-"))&&(document.referrer.prototype.endsWith("/agents")))||//from 2,3,4... to 1
     ((window.location.href.includes("/agents/listing/page-"))&&(document.referrer.includes("/agents/listing/page-"))))//from 2,3,4... to 2,3,4...
     if ( $.cookie("agents_scroll") !== null )
     $(document).scrollTop( $.cookie("agents_scroll") );


     // When an anchor in pagination is clicked
     $(".pages").find("a").on("click", function() {
     // Set a cookie that holds the scroll position.
     if(window.location.href.includes("/agents"))
     $.cookie("agents_scroll", $(document).scrollTop() );
     });*/

    var dragging = true;
    var owlElementID = "#owl-testimonials";

    function fadeInReset() {
        if (!dragging) {
            $(owlElementID + " .caption .fadeIn-1, " + owlElementID + " .caption .fadeIn-2, " + owlElementID + " .caption .fadeIn-3").stop().delay(800).animate({opacity: 0}, {
                duration: 400,
                easing: "easeInCubic"
            });
        }
        else {
            $(owlElementID + " .caption .fadeIn-1, " + owlElementID + " .caption .fadeIn-2, " + owlElementID + " .caption .fadeIn-3").css({opacity: 0});
        }
    }

    function fadeInDownReset() {
        if (!dragging) {
            $(owlElementID + " .caption .fadeInDown-1, " + owlElementID + " .caption .fadeInDown-2, " + owlElementID + " .caption .fadeInDown-3").stop().delay(800).animate({
                opacity: 0,
                top: "-15px"
            }, {duration: 400, easing: "easeInCubic"});
        }
        else {
            $(owlElementID + " .caption .fadeInDown-1, " + owlElementID + " .caption .fadeInDown-2, " + owlElementID + " .caption .fadeInDown-3").css({
                opacity: 0,
                top: "-15px"
            });
        }
    }

    function fadeInUpReset() {
        if (!dragging) {
            $(owlElementID + " .caption .fadeInUp-1, " + owlElementID + " .caption .fadeInUp-2, " + owlElementID + " .caption .fadeInUp-3").stop().delay(800).animate({
                opacity: 0,
                top: "15px"
            }, {duration: 400, easing: "easeInCubic"});
        }
        else {
            $(owlElementID + " .caption .fadeInUp-1, " + owlElementID + " .caption .fadeInUp-2, " + owlElementID + " .caption .fadeInUp-3").css({
                opacity: 0,
                top: "15px"
            });
        }
    }

    function fadeInLeftReset() {
        if (!dragging) {
            $(owlElementID + " .caption .fadeInLeft-1, " + owlElementID + " .caption .fadeInLeft-2, " + owlElementID + " .caption .fadeInLeft-3").stop().delay(800).animate({
                opacity: 0,
                left: "15px"
            }, {duration: 400, easing: "easeInCubic"});
        }
        else {
            $(owlElementID + " .caption .fadeInLeft-1, " + owlElementID + " .caption .fadeInLeft-2, " + owlElementID + " .caption .fadeInLeft-3").css({
                opacity: 0,
                left: "15px"
            });
        }
    }

    function fadeInRightReset() {
        if (!dragging) {
            $(owlElementID + " .caption .fadeInRight-1, " + owlElementID + " .caption .fadeInRight-2, " + owlElementID + " .caption .fadeInRight-3").stop().delay(800).animate({
                opacity: 0,
                left: "-15px"
            }, {duration: 400, easing: "easeInCubic"});
        }
        else {
            $(owlElementID + " .caption .fadeInRight-1, " + owlElementID + " .caption .fadeInRight-2, " + owlElementID + " .caption .fadeInRight-3").css({
                opacity: 0,
                left: "-15px"
            });
        }
    }

    function fadeIn() {
        $(owlElementID + " .active .caption .fadeIn-1").stop().delay(500).animate({opacity: 1}, {
            duration: 800,
            easing: "easeOutCubic"
        });
        $(owlElementID + " .active .caption .fadeIn-2").stop().delay(700).animate({opacity: 1}, {
            duration: 800,
            easing: "easeOutCubic"
        });
        $(owlElementID + " .active .caption .fadeIn-3").stop().delay(1000).animate({opacity: 1}, {
            duration: 800,
            easing: "easeOutCubic"
        });
    }

    function fadeInDown() {
        $(owlElementID + " .active .caption .fadeInDown-1").stop().delay(500).animate({
            opacity: 1,
            top: "0"
        }, {duration: 800, easing: "easeOutCubic"});
        $(owlElementID + " .active .caption .fadeInDown-2").stop().delay(700).animate({
            opacity: 1,
            top: "0"
        }, {duration: 800, easing: "easeOutCubic"});
        $(owlElementID + " .active .caption .fadeInDown-3").stop().delay(1000).animate({
            opacity: 1,
            top: "0"
        }, {duration: 800, easing: "easeOutCubic"});
    }

    function fadeInUp() {
        $(owlElementID + " .active .caption .fadeInUp-1").stop().delay(500).animate({
            opacity: 1,
            top: "0"
        }, {duration: 800, easing: "easeOutCubic"});
        $(owlElementID + " .active .caption .fadeInUp-2").stop().delay(700).animate({
            opacity: 1,
            top: "0"
        }, {duration: 800, easing: "easeOutCubic"});
        $(owlElementID + " .active .caption .fadeInUp-3").stop().delay(1000).animate({
            opacity: 1,
            top: "0"
        }, {duration: 800, easing: "easeOutCubic"});
    }

    function fadeInLeft() {
        $(owlElementID + " .active .caption .fadeInLeft-1").stop().delay(500).animate({
            opacity: 1,
            left: "0"
        }, {duration: 800, easing: "easeOutCubic"});
        $(owlElementID + " .active .caption .fadeInLeft-2").stop().delay(700).animate({
            opacity: 1,
            left: "0"
        }, {duration: 800, easing: "easeOutCubic"});
        $(owlElementID + " .active .caption .fadeInLeft-3").stop().delay(1000).animate({
            opacity: 1,
            left: "0"
        }, {duration: 800, easing: "easeOutCubic"});
    }

    function fadeInRight() {
        $(owlElementID + " .active .caption .fadeInRight-1").stop().delay(500).animate({
            opacity: 1,
            left: "0"
        }, {duration: 800, easing: "easeOutCubic"});
        $(owlElementID + " .active .caption .fadeInRight-2").stop().delay(700).animate({
            opacity: 1,
            left: "0"
        }, {duration: 800, easing: "easeOutCubic"});
        $(owlElementID + " .active .caption .fadeInRight-3").stop().delay(1000).animate({
            opacity: 1,
            left: "0"
        }, {duration: 800, easing: "easeOutCubic"});
    }





   

   

    $('.toTop ').click(function () {
        $("html, body").animate({scrollTop: 0}, 600);
        return false;
    });


    $(window).scroll(function () {
        if ($(window).scrollTop() > 1000) {
            $(".toTop").addClass("enabled");
        } else {
            $(".toTop").removeClass("enabled");
        }
    });


});

var dd_to;

var isHandHeld = false;
function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function isPhoneAnyLength(phone) {
    var regex = /^[0-9 +]+$/;
    return regex.test(phone);
}

function isName(name) {
    var regex = /^[A-Za-z\s]{1,}[\.]{0,1}[A-Za-z\s]{0,}$/;
    return regex.test(name);
}

function onlyLetters(text) {
    var regex = /^[a-zA-Z\ \']+$/;
    return regex.test(text);
}

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function resetForm(f) {
    f.find("input[type=text],input[type=email], textarea,select").val("");
    //f.find("select").prop('selectedIndex', 0);
    f.find('.slct_dd').find('b').each(function () {
        $(this).text($(this).attr('data-prev-val'));
    });

    f.find('.slct_dd').find('input[type=hidden]').not('input[name*=ph_ext]').val("");

    //$('input[type=hidden]').val('');
    f.find('input[type=checkbox]').prop('checked', false);
    if (typeof grecaptcha != 'undefined') {
        grecaptcha.reset();
    }

}
function isMultipleEmail(email) {
    var emails = email.split(',');
    var $return = true;
    $(emails).each(function (i, em) {
        if (!isEmail(em)) {
            $return = false;
            console.log(em)
        }
    });
    return $return;
}

function isPhone(phone) {
    var regex = /^[0-9-+]+$/;
    return regex.test(phone);
}
function createCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
jQuery.fn.outerHTML = function (s) {
    return s
        ? this.before(s).remove()
        : jQuery("<p>").append(this.eq(0).clone()).html();
};

var home_txt_move = 0;
if ($(window).width() <= 1024) {
    $('#mobileSearch').html($('#searchBox').outerHTML());
    $('#wrapper > #searchBox').remove();
}

function sizeBoxes() {

    $('.boxes .box').removeAttr('style');

    var win_w = $(window).width();

    if (win_w < 960) {
        return;
    }

    var box1_w = $('.boxes .box1:first').outerWidth();
    var box2_h = $('.boxes .box1:first').outerHeight();

    if ((box1_w % 10 != 0) && (box1_w % 2 != 0)) {
        box1_w = box1_w + 1;
    }

    if ((box2_h % 10 != 0) && (box2_h % 2 != 0)) {
        box2_h = box2_h + 1;
    }

    $('.boxes .box1').each(function () {
        $(this).css('paddingBottom', '0').width(box1_w - 1).height(box2_h);
    });

    $('.boxes .box2').each(function () {
        var tmp = parseInt((win_w - box1_w - 1) / 2);
        $(this).css('paddingBottom', '0').width(tmp).height(parseInt(box2_h / 2));
    });

    $('.boxes .box3').each(function () {
        var tmp = parseInt(win_w - box1_w - 1);
        $(this).css('paddingBottom', '0').width(tmp).height(parseInt(box2_h / 2));
    });

}

function numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

$.fn.is_on_screen = function () {

    var win = $(window);

    var viewport = {
        top: win.scrollTop(),
        left: win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();

    var bounds = this.offset();
    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();

    //return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top));

};

$.fn.extend({
    list2Columns: function (numCols) {
        var listItems = $(this).find('li');
        var listHeader = $(this);
        var numListItems = listItems.length;
        var numItemsPerCol = Math.ceil(numListItems / numCols);
        var currentColNum = 1, currentItemNumber = 1, returnHtml = '', i = 0;

        for (i = 1; i <= numCols; i++) {
            $(this).parent().append('<ul class="column list-column-' + i + '"></ul>');
        }

        $.each(listItems, function (i, v) {
            if (currentItemNumber <= numItemsPerCol) {
                currentItemNumber++;
            } else {
                currentItemNumber = 1;
                currentColNum++;
            }
            $('.list-column-' + currentColNum).append(v);
        });
        $(this).remove();
    }
});

$(document).ready(function () {

    $(".btn_contact_me").click(function () {
        $('html,body').animate({
                scrollTop: $("#contact").offset().top
            },
            'slow');
    });

    $(".btn_my_listing").click(function () {
        $('html,body').animate({
                scrollTop: $("#agent_detail_listing").offset().top
            },
            'slow');
    });

    if ($('#topMenu .li_dd').length > 0) {

        $('#topMenu .li_dd').on("click", function (e) {
            e.preventDefault();
            clearTimeout(dd_to);
            $('.li_dd.act').not(this).removeClass('act').parents('li').removeClass('act');
            $(this).toggleClass('act').parents('li').toggleClass('act');
            if ($('#mobileMenu').is(':visible')) {
                $('.li_dd').not(this).parents('li').find('.dd').slideUp(400);
                $(this).parents('li').find('.dd').slideToggle(400);
            } else {
            }
        });

        $(document).on('click', function (e) {
            if (!$('#mobileMenu').is(':visible')) {
                $('.li_dd.act').removeClass('act').parents('li').removeClass('act');
            }
        });

        $('#topMenu .li_dd, #topMenu .dd').hover(function () {
            clearTimeout(dd_to);
        }, function () {
            dd_to = setTimeout(function () {
                $('.li_dd.act').removeClass('act').parents('li').removeClass('act');
            }, 3000);
        });

        $('.li_dd, #topMenu ul .dd').on("click", function (e) {
            e.stopPropagation();
        });
    }


    $('#topMenu .currency a').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        var tmp = $(this).find('span').text();
        if ($(this).is(':contains("Euro")')) {
            tmp = 'EUR'
        }
        if ($(this).is(':contains("Pound")')) {
            tmp = 'GBP'
        }
        if ($(this).is(':contains("US Dollar")')) {
            tmp = 'USD'
        }
        $('#btnCurrency').text(tmp).attr('title', tmp).click();
        $('#topMenu .currency a').removeClass('act');
        $(this).addClass('act')
    });

    $('#mobileMenu').click(function (e) {
        e.preventDefault();
        //$('body').toggleClass('showMenu');
        if (!$('body').hasClass('showMenu')) {
            $('#wrapper').width($('#wrapper').width());
            //$('#searchBox').width( $('#wrapper').width() );
            //$('#topMenu .topMenu').css('margin-top',$('#searchBox').width()+'px');
            $('body').addClass('showMenu');
            $('#mobileMenu').addClass('act');
        } else {

            $('body').removeClass('showMenu');
            setTimeout(function () {
                $('#wrapper').removeAttr('style');
                $('#topMenu li').removeClass('act');
                $('#topMenu .dd').removeAttr('style')
            }, 300);
            $('#mobileMenu').removeClass('act');
        }

    });


    $('#mobilePageMenu').click(function (e) {
        //alert('hi');
        e.preventDefault();
        //$('body').toggleClass('showPageMenu');
        if (!$('body').hasClass('showPageMenu')) {
            $('body').addClass('showPageMenu');
            $('#mobilePageMenu').addClass('act');
        } else {

            $('body').removeClass('showPageMenu');
            setTimeout(function () {
                $('#wrapper').removeAttr('style');
                $('#pageMenu li').removeClass('act');
                $('#pageMenu .dd').removeAttr('style')
            }, 300);
            $('#mobilePageMenu').removeClass('act');
        }

    });

    // Home boxes switch
    $('.filters1 a').click(function (e) {
        e.preventDefault();
        if ($(this).hasClass('act')) {
            return;
        }

        var boats = home_boxes[$(this).attr('data-filter')];
        $('.filters a').removeClass('act');
        $(this).addClass('act');

        $('.boxes .box').addClass('load');

        setTimeout(function () {

            $('.boxes .box').each(function (ind) {
                var box = $(this);
                if (typeof boats[ind] !== "undefined") {

                    box.show();
                    box.find('img').attr('src', boats[ind][0]);
                    box.find('.txt b').text(boats[ind][1]);
                    box.find('.txt u').text(boats[ind][2]);
                    box.find('.price b').text(boats[ind][3]);
                    box.find('a').attr('href', boats[ind][4]);
                    //box.find('.price span').text(currentCurrency);
                    if (boats[ind][3]) {
                        box.find('.price').show();
                    } else {
                        box.find('.price').hide();
                    }

                } else {
                    box.hide();
                }

                var delay = 100 * Math.floor(Math.random() * 10);
                setTimeout(function () {
                    box.removeClass('load');
                }, delay);

            });

        }, 1000);

        //var scroll_to = $(this).parents('.filterBox').offset().top - $('#header').height() - $('.filterBox').height();
        var scroll_to = $(this).parents('#content .filterBox').offset().top - $('#header').height();
        if ($('#searchBox').is(':visible')) {
            scroll_to -= $('#searchBox').height();
        }
        $('body,html').animate({scrollTop: scroll_to}, 'slow');

    });


    if ($("#homeHeader .slider").length > 0) {

        var hh_slides = $('#homeHeader .slider > div').length;
        var hh_start = Math.floor((Math.random() * hh_slides));

        $('#homeHeader .slider > div').each(function () {
            var slide = $(this);
            var img = slide.find("img").attr("src");
            //slide.css('background','url('+slide.find("img").attr("src")+')');
            slide.find('img').wrap('<div class="div"></div>')
            slide.find('.div').css('backgroundImage', 'url(' + img + ')');
            slide.find("img").remove();
        });

        home_txt_move = $('#homeHeader .txtBoxes li').width();
        if ($(window).width() <= 700) {
            home_txt_move = $(window).width();
            $('#homeHeader .txtBoxes li').width(home_txt_move);
            tmp = home_txt_move * $('#homeHeader .txtBoxes li').size();
            $('#homeHeader .txtBoxes ul').width(tmp);
        }


        $(window).resize(function () {
            home_txt_move = $('#homeHeader .txtBoxes li').width();
            if ($(window).width() <= 700) {
                home_txt_move = $(window).width();
                $('#homeHeader .txtBoxes ul').css('width', 'auto');
                $('#homeHeader .txtBoxes li').css('width', 'auto').width(home_txt_move);
                tmp = home_txt_move * $('#homeHeader .txtBoxes li').size();
                $('#homeHeader .txtBoxes ul').width(tmp);
                tmp = $('#homeHeader .slick-active').attr('data-slick-index') * home_txt_move * -1;
                $('#homeHeader .txtBoxes ul').css('left', tmp + 'px');
            } else {
                $('#homeHeader .txtBoxes ul, #homeHeader .txtBoxes li').css('width', 'auto');
            }
        });

        $('#homeHeader .slider')
            .slick({
                //initialSlide: hh_start,
                fade: true,
                adaptiveHeight: false,
                dots: false,
                infinite: true,
                accessibility: false,
                speed: 800,
                //lazyLoad: 'ondemand',
                arrows: false,
                pauseOnHover: false,
                autoplay: false,
                autoplaySpeed: 7000,
                rtl: false
            });

        $('#homeHeader .slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
            home_txt_to = home_txt_move * nextSlide * -1;
            $('#homeHeader .txtBoxes ul').css('left', home_txt_to + 'px');
            $('#homeHeader .txtBoxes li').removeClass('act');
            $('#homeHeader .txtBoxes li:eq("' + nextSlide + '")').addClass('act');
        });

        $('#homeHeader .txtBoxes a').click(function (e) {
            e.preventDefault();
            var ind = $(this).parents('li').index();
            $('#homeHeader .slider').slick('slickGoTo', ind);
        });

        $('#homeHeader .txtBoxes a u').click(function (e) {
            e.preventDefault();
            var href = $(this).parents('a').attr('href');
            window.location = href;
            e.stopPropagation();
        });

        $('#homeHeader .controls .btnPrev').click(function (e) {
            e.preventDefault();
            $('#homeHeader .slider').slick('slickPrev');
        });

        $('#homeHeader .controls .btnNext').click(function (e) {
            e.preventDefault();
            $('#homeHeader .slider').slick('slickNext');
        });

    }


    if ($(".bigSlider").length > 0) {

        $('.bigSlider').each(function () {
            var slider = $(this);

            $(this).find('> div').each(function () {
                var img = $(this).find("img").attr("src");
                $(this).find('img').wrap('<div class="div"></div>')
                $(this).find('.div').css('backgroundImage', 'url(' + img + ')');
                $(this).find("img").remove();
            });

            var bs_slides = slider.find('> div').length;
            var bs_start = Math.floor((Math.random() * bs_slides));
            var bs_dots = false;

            if ($(this).hasClass('wDots')) {
                bs_dots = true;
            }

            $(this)
                .slick({
                    initialSlide: bs_start,
                    fade: true,
                    adaptiveHeight: false,
                    dots: bs_dots,
                    infinite: true,
                    accessibility: false,
                    speed: 800,
                    //lazyLoad: 'ondemand',
                    arrows: false,
                    pauseOnHover: false,
                    autoplay: true,
                    autoplaySpeed: 7000,
                    rtl: false
                });

            $(this).parent().find('.controls .btnPrev').click(function (e) {
                e.preventDefault();
                slider.slick('slickPrev');
            });

            $(this).parent().find('.controls .btnNext').click(function (e) {
                e.preventDefault();
                slider.slick('slickNext');
            });

        });

    }

    $(".actionsBottom .share > a").on("click", function (e) {
        e.preventDefault();
        $(this).parent().toggleClass('act')
    });

    $(".actionsBottom .share input[type='text']").on("click", function () {
        $(this).select();
        $(this).parents('.actionsBottom .share').toggleClass('act')
    });


    if ($("#consultants .slider").length > 0) {

        var cn_slides = $('#consultants .slider > div').length;
        var cn_start = Math.floor((Math.random() * cn_slides));

        $('#consultants .slider')
            .slick({
                initialSlide: cn_start,
                //fade: true,
                adaptiveHeight: true,
                dots: true,
                slidesToShow: 1,
                infinite: true,
                accessibility: false,
                speed: 800,
                //lazyLoad: 'ondemand',
                arrows: false,
                pauseOnHover: true,
                autoplay: true,
                autoplaySpeed: 6000,
                rtl: false
            });

        $('#consultants .controls .btnPrev').click(function (e) {
            e.preventDefault();
            $('#consultants .slider').slick('slickPrev');
        });

        $('#consultants .controls .btnNext').click(function (e) {
            e.preventDefault();
            $('#consultants .slider').slick('slickNext');
        });

    }


    if ($("#newListings .newListings").length > 0) {

        var sl_to_show = 4;
        if ($(window).width() < 1200) {
            sl_to_show = 3
        }
        if ($(window).width() < 970) {
            sl_to_show = 2
        }
        if ($(window).width() < 750) {
            sl_to_show = 1
        }

        $('#newListings .newListings')
            .slick({
                //initialSlide: cn_start,
                //fade: true,
                adaptiveHeight: true,
                swipeToSlide: true,
                dots: false,
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: true,
                accessibility: false,
                speed: 400,
                //lazyLoad: 'ondemand',
                arrows: false,
                pauseOnHover: false,
                autoplay: false,
                autoplaySpeed: 6000,
                rtl: false,
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 970,
                        settings: {
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 750,
                        settings: {
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 500,
                        settings: {
                            slidesToShow: 1
                        }
                    }
                ]
            });

        $('#newListings .controls .btnPrev').click(function (e) {
            e.preventDefault();
            $('#newListings .newListings').slick('slickPrev');
        });

        $('#newListings .controls .btnNext').click(function (e) {
            e.preventDefault();
            $('#newListings .newListings').slick('slickNext');
        });

    }

    $('<div class="overlay"></div>').insertBefore('.browse .inpt')

    $('.browse .btn, .browse .overlay').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).parents('.browse').find('[type="file"]').click();
    });


    $('.browse [type="file"], .browseBig [type="file"]').click(function (e) {
        e.stopPropagation();
    });

    $(document).on('change', '.browse [type="file"], .browseBig [type="file"]', function () {
        var tmp = $(this).val();
        $(this).parent().find('.inpt').val(tmp);
    });


    $('.inpt').each(function () {

        var default_value = $(this).val();
        if ($(this).attr('[data-default]')) {
            default_value = $(this).attr('[data-default]');
        }

        $(this).focus(function () {
            if ($(this).val() == default_value) {
                $(this).val('').removeClass('default').addClass('act')
            }
        });

        $(this).blur(function () {
            if ($(this).val() == '') {
                $(this).val(default_value).addClass('default').removeClass('act')
            }
        });

    });


    $(document).on('change', '.slct select', function (e) {
        e.stopPropagation();
        var tmp = $(this).val();
        $(this).parents('.slct').find('b').text(tmp);
    });

    $('#mobileSearch1').on('click', '.slct_dd', function (e) {
        //alert('hi');
        e.preventDefault();

        $('.slct_dd.act').not(this).removeClass('act');
        $(this).toggleClass('act');
    });

    if ($('.slct_dd').length) {

        $('.slct_dd .dd').wrapInner("<div class='scroll'></div>");

        $('.slct_dd').on("click", function (e) {
            //$('.slct_dd').click(function(e){
            //$(document).on('click', '.slct_dd', function(e) {
            //alert('hi');
            e.preventDefault();

            $('.slct_dd.act').not(this).removeClass('act');
            $(this).toggleClass('act');
        });

        $('.slct_dd .scroll').each(function () {
            //tmp = $(this).parents('.slct_dd').width();
            //$(this).width(tmp);
            
            if ($(this).parent().is("[data-height]")) {
                //alert($(this).parent().attr("data-height"));
                $(this).height($(this).parent().attr("data-height"));
            }
        });


        $('.slct_dd .dd li a').on("click", function (e) {
            //alert('hi');
            var shouldStop = $(this).parents('.custom_click_event').length;
            if (shouldStop) {
                return;
            }

            e.preventDefault();
            var txt = $(this).text();
            var val = $(this).attr('data-val');
            //alert(val);
            $(this).parents('form').find('.slct_dd').removeClass('act');
            var cls = '';
            if ($(this).hasClass('act')) {
                return;
            } else {
                //alert('hh');
                if ($(this).hasClass('flag')) {
                    cls = $(this).attr('class');
                }

                $(this).parents('.slct_dd').find('li a').removeClass('act');
                $(this).addClass('act');
                $(this).parents('.slct_dd').find('b').text(txt);
                $(this).parents('.slct_dd').find('b').text(txt).removeAttr('class').addClass(cls);
                $(this).parents('.slct_dd').find('input').val(txt);
                $(this).parents('.slct_dd').click();
                $('.slct_dd.act').removeClass('act');
            }

        });

        $('.slct_dd').each(function () {
            var $t = $(this);
            if ($t.find('.filter_selected').val()) {
                $t.find('b').text($t.find('.filter_selected').val()).removeAttr('class');
            } else if ($t.find('.search_hidden_ip').val()) {
                $t.find('b').text($t.find('.search_hidden_ip').val()).removeAttr('class');
            }
        });

    }


    $('#pageMenu nav > ul > li > a').click(function (e) {
        return true;
    });

    $(document).on('click', function (e) {
        $('.slct_dd.act').removeClass('act');
        if ($('body').hasClass('showMenu')) {
            $('#mobileMenu').click();
        }
        if ($('body').hasClass('showPageMenu')) {
            $('#mobilePageMenu').click();
        }
        if ($('body').hasClass('showFilters')) {
            $('#filterBy').click()
        }
        $('#pageMenu nav > ul > li.act').removeClass('act')
        //$('.pp .close').click();
    });


    $('.slct, .slct_dd, .slct_dd .dd, #topMenu, #topMenu, #mobileMenu, .pp, #filterBy, .btnFilter, .filterBy, #pageMenu, #mobilePageMenu, #sidebar').on("click", function (e) {
        //$('.slct, .slct_dd, .slct_dd .dd, #topMenu, #topMenu *, #mobileMenu, .pp, #filterBy, .btnFilter, .filterBy, #pageMenu, #mobilePageMenu, #sidebar').on( "click", function (e) {
        //alert('hi');
        e.stopPropagation();
    });

    $('.pp').on("click", function (e) {
        $('.slct_dd.act').removeClass('act');
    });

    $('.pp .content > .close, .pp .btnClose, .pp .btnClose *, .ppVideo .close, .pp > .close').click(function (e) {
        e.preventDefault();
        $('.pp:visible').find('iframe').attr('src', '');
        $('.pp:visible').fadeOut(400, function () {
            $('.pp:hidden').removeClass('act');
        })
    });


    $('.actionsBottom .icon_email1').click(function (e) {
        e.preventDefault();
        $('#ppEmailFriend').find('textarea').text($(this).attr('data-textarea'));
        $('#ppEmailFriend').fadeIn(400).addClass('act');
    });

    $('.actionsBottom .icon_print').click(function (e) {
        e.preventDefault();
        window.print();
    });

    $('#detailsHeader .navList .icon_brochure1, .actionsBottom .icon_pdf, .btnBrochure1').click(function (e) {
        e.preventDefault();
        $('#ppBrochure').fadeIn(400).addClass('act');
    });

    $('.item .addWish, .icon_wishlist').click(function (e) {
        e.preventDefault();
        $('.addedWishlist').fadeIn(400, function () {
            setTimeout(function () {
                $('.addedWishlist').fadeOut(400)
            }, 1000);
        })
    });

    $('.item .calculator').click(function (e) {
        e.preventDefault();
        $('#ppCalculator').fadeIn(400).addClass('act');
    });

    $('.btnMakeOffer').click(function (e) {
        e.preventDefault();
        $('#ppMakeOffer').fadeIn(400).addClass('act');
        setTimeout(function () {
            $('.pp:visible .item .slider').slick('setPosition');
        }, 500);
    });

    $('.btnArrangeViewing').click(function (e) {
        e.preventDefault();

        var $t = $(this);

        $('#ppArrangeViewing .photo').attr('src', $t.attr('data-image'));
        $('#ppArrangeViewing .sublocation').html($t.attr('data-sublocation'));
        $('#ppArrangeViewing .location').html($t.attr('data-location'));
        $('#ppArrangeViewing .emirate').html($t.attr('data-emirate'));
        $('#ppArrangeViewing .price').html($t.attr('data-price'));
        $('#ppArrangeViewing .size').html($t.attr('data-size'));
        $('#ppArrangeViewing .reference').html($t.attr('data-reference'));
        $('#ppArrangeViewing .property_category').html($t.attr('data-category'));
        $('#ppArrangeViewing .sublocation').html($t.attr('data-sublocation'));

        $('#ppArrangeViewing a').attr('href', $t.attr('data-link'));

        $('#ppArrangeViewing .reference_ip').val($t.attr('data-reference'));

        $('#ppArrangeViewing').fadeIn(400).addClass('act');
        $('#ppArrangeViewing .message_buy_rent').hide();
        $('#ppArrangeViewing .message_' + $t.attr('data-type')).show();
        if ($t.attr('data-type') == 'buy') {
            $('.is_finace_need_popup').show();
        } else {
            $('.is_finace_need_popup').hide();
        }
        setTimeout(function () {
            $('.pp:visible .item .slider').slick('setPosition');
        }, 500);
    });

    $('.videoBox a, .videoLink').click(function (e) {
        e.preventDefault();
        var vid = $(this).attr('data-video');

        $('#ppVideo').find('iframe').attr('src', vid);
        $('#ppVideo').fadeIn(400).addClass('act');
    });

    $('.ppFrameLink').click(function (e) {
        e.preventDefault();
        var target = $(this).attr('href');
        $(target).fadeIn(400).addClass('act');
    });

    $('.pp .btnArrange').click(function (e) {
        e.preventDefault();
        $('.ppSlider:visible').addClass('showSidebar')
    });

    $('.ppSlider .closeSidebar').click(function (e) {
        e.preventDefault();
        $('.ppSlider:visible').removeClass('showSidebar')
    });

    $('#detailsHeader .icon_photos').click(function (e) {
        e.preventDefault();
        $('#ppPhotos').fadeIn(400).addClass('act');

        $('#ppPhotos .slider').slick('setPosition');
    });

    $('#detailsHeader .icon_videos').click(function (e) {
        e.preventDefault();

        $('#ppVideo').find('.cell').html('<iframe width="100%" height="402" src="https://www.youtube.com/embed/' + $(this).attr('data-video') + '" frameborder="0" allowfullscreen></iframe>');

        $('#ppVideo').fadeIn(400).addClass('act');

        //$('#ppVideo .slider').slick('setPosition');
    });

    $('#detailsHeader .icon_floorplan1').click(function (e) {
        e.preventDefault();
        $('#ppFloorplans').fadeIn(400).addClass('act');
        $('#ppFloorplans .slider').slick('setPosition');
    });


    $('#devsMenu a, .btnAnchor').click(function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        if (href.length <= 1) {
            return;
        }
        var sc_to = $(href).offset().top - $('#header').height() - 32;
        if ($('#searchBox').is(':visible')) {
            sc_to = sc_to - $('#searchBox').height() - 1;
        }
        $('body,html').animate({scrollTop: sc_to}, 'slow');
    });

    $(".scroll").mCustomScrollbar({
        theme: "dark-thick",
        setHeight: function(){
            //alert($(this).parent().attr('class'));
            if(parseInt($(this).parent().attr('data-height'))){ 
                return $(this).parent().attr('data-height'); 
                //alert('new');
                
            } 
            
        }
    });


    if ($(".item .slider").length > 0) {

        $(".item .slider").each(function () {

            $(this).slick({
                adaptiveHeight: false,
                dots: false,
                infinite: true,
                accessibility: false,
                speed: 400,
                //lazyLoad: 'ondemand',
                arrows: true,
                pauseOnHover: false,
                autoplay: false,
                autoplaySpeed: 7000,
                rtl: false
            });

        });

    }


    if ($(".slideShow").length > 0) {


        $('.slideShow > div').each(function () {
            var slide = $(this);
            var img = slide.find("img").attr("src");
            slide.find('img').wrap('<div class="div"></div>')
            slide.find('.div').css('backgroundImage', 'url(' + img + ')');
            slide.find("img").remove();
        });

        $(".slideShow").each(function () {

            var ss_slides = $(this).find('> div').length;
            var ss_start = Math.floor((Math.random() * ss_slides));

            $(this).slick({
                initialSlide: ss_start,
                adaptiveHeight: false,
                fade: true,
                dots: false,
                infinite: true,
                accessibility: false,
                speed: 400,
                //lazyLoad: 'ondemand',
                arrows: false,
                pauseOnHover: false,
                autoplay: true,
                autoplaySpeed: 6000,
                rtl: false
            });

        });
    }


    if ($("#priceRange").length > 0 && 0) {

        var priceRange = document.getElementById('priceRange');

        noUiSlider.create(priceRange, {
            start: 5000000,
            connect: 'lower',
            step: 100000,
            range: {
                'min': 0,
                'max': 50000000
            }
        });

        var priceRangeText = document.getElementById('priceRangeText');

        priceRange.noUiSlider.on('update', function (values, handle) {
            priceRangeText.innerHTML = numberWithCommas(parseInt(values[handle]));
        });

    }


    if ($("#lengthRange").length > 0 && 0) {

        var lengthRange = document.getElementById('lengthRange');

        noUiSlider.create(lengthRange, {
            start: 32000,
            connect: 'lower',
            step: 500,
            range: {
                'min': 0,
                'max': 100000
            }
        });

        var lengthRangeText = document.getElementById('lengthRangeText');

        lengthRange.noUiSlider.on('update', function (values, handle) {
            lengthRangeText.innerHTML = numberWithCommas(parseInt(values[handle]));
        });

    }

    $('.similarItems .seeAll a').click(function (e) {
        e.preventDefault();
        $(this).parent().remove();
        $('.similarItems .hide').slideDown(800, function () {
            $('.similarItems .slider').slick('setPosition');
        })
        $('.similarItems .slider').slick('setPosition');
    });

    $('.heritage1').click(function (e) {
        e.preventDefault();
        window.open('http://www.sothebys.com/en.html', '_blank');
    });

    $('a.showMore').click(function (e) {
        e.preventDefault();

        if ($(this).parents().hasClass('showMore')) {
            $(this).closest('div.showMore').prev().slideToggle('normal');
        } else {
            $(this).parents('fieldset').find('.hide_block').slideToggle('normal');
        }
        $(this).toggleClass('act');

        var tmp = $(this).text();
        $(this).text(tmp == "View more" ? "View less" : "View more");
    });

    $('#filterBy, .btnFilter').click(function (e) {
        e.preventDefault();
        if ($('body').hasClass('showFilters')) {
            $('body').removeClass('showFilters');
            $('#middle, body, #wrapper, html').removeAttr('style');
            $('body.no-scroll').unbind('touchmove');
        } else {

            if ($('#filterBy').is(':hidden')) {

                $('html, body').css('overflow', 'hidden').addClass('no-scroll');
                $('#wrapper, body').height($('#wrapper').height());
                $('body.no-scroll').bind('touchmove', function (e) {
                    e.preventDefault()
                })
                $('#sidebar.filterBy').scrollTop(0);

            } else {
                $('#middle').removeAttr('style').width($('#middle').width());
            }
            $('body').addClass('showFilters');

            $(window).resize(function () {

                if ($(window).width() > 1210) {
                    $('body').removeClass('showFilters');
                    $('#middle, body, #wrappe, htmlr').removeAttr('style');
                    $('body.no-scroll').unbind('touchmove');
                } else {
                    $('.showFilters #middle, .showFilters #wrapper').removeAttr('style').width($('#middle').width());
                    $('body.no-scroll').unbind('touchmove');
                }

            });

        }
    });

    $('#sidebar.filterBy .btns .btn').click(function (e) {
        e.preventDefault();
        $('.filterBtm .btn').click();
    });

    $('#views a').click(function (e) {
        e.preventDefault();
        var tmp = $(this);
        var viewClass = tmp.attr('class');
        viewClass = '.' + viewClass;
        var viewClass = viewClass.replace("act", "");
        if (getCookie('view_type') != viewClass) {
            createCookie('view_type', viewClass, 30);
        }
        if (tmp.hasClass('act')) {
            return;
        }

        if (tmp.hasClass('icon_list')) {

            $('.listings .mapView:visible').fadeOut(600);
            $('.listings .items').fadeOut(600);
            $('.listings .pages').fadeOut(300);

            $('#views a').removeClass('act');
            tmp.addClass('act');

            setTimeout(function () {

                $('.listings #middle .loading').fadeIn(400)

                setTimeout(function () {

                    $('.items').addClass('listView');
                    $('.listings #sidebar, .listings #middle').removeClass('wMap');

                    setTimeout(function () {
                        $('.listings #middle .loading').fadeOut(400)
                        $('.listings .items').slideDown(800);
                        $('.listings .pages').delay(600).fadeIn(300);

                        $("#sidebar.wMap").mCustomScrollbar("destroy");
                        $(".scroll").mCustomScrollbar({
                            theme: "dark-thick"
                        });

                        $(window).trigger('resize');
                    }, 400);

                }, 2000);

            }, 600);

        }

        if (tmp.hasClass('icon_grid')) {

            $('.listings .mapView:visible').fadeOut(600);
            $('.listings .items').fadeOut(600);
            $('.listings .pages').fadeOut(300);

            $('#views a').removeClass('act');
            tmp.addClass('act');

            setTimeout(function () {

                $('.listings #middle .loading').fadeIn(400)

                setTimeout(function () {

                    $('.items').removeClass('listView');
                    $('.listings #sidebar, .listings #middle').removeClass('wMap');

                    setTimeout(function () {
                        $('.listings #middle .loading').fadeOut(400)
                        $('.listings .items').slideDown(800);
                        $('.listings .pages').delay(600).fadeIn(300);

                        $("#sidebar.wMap").mCustomScrollbar("destroy");
                        $(".scroll").mCustomScrollbar({
                            theme: "dark-thick"
                        });

                        $(window).trigger('resize');
                    }, 200);

                }, 2000);

            }, 600);
        }

        if (tmp.hasClass('icon_map')) {

            $('.listings .items').fadeOut(600);
            $('.listings .pages').fadeOut(300);

            $('#views a').removeClass('act');
            tmp.addClass('act');

            //setTimeout(function() {

            $('.listings #middle .loading').fadeIn(400)

            //setTimeout(function() {

            $('.listings #sidebar, .listings #middle').addClass('wMap');

            //setTimeout(function() {
            $('.listings #middle .loading').fadeOut(400)
            $('.listings .mapView').fadeIn(600);

            $("#sidebar.wMap").mCustomScrollbar({
                theme: "dark-thick"
            });
            //}, 200);

            //}, 2000);

            //}, 600);
        }
    });


    $('#backTop').click(function (e) {
        e.preventDefault();
        $('body,html').animate({scrollTop: 0}, 'slow');
    });


    /* Correct year near copyright sign   */
    $("#currYear").text((new Date).getFullYear());


    $('.email').each(function () {
        var $email = $(this);
        var address = $email.text()
            .replace(/\s*\[at\]\s*/, '@')
            .replace(/\s*\[dot\]\s*/g, '.');
        $email.html('<a href="mailto:' + address + '" title="' + address + '">' + address + '</a>');
    });

    $('#topLinks a, #topMenu a, .shareIcons a, a.btn, .footer ul a').each(function () {
        var tmp = $(this).attr('title');
        if (tmp !== undefined) {
            return;
        }
        $(this).attr('title', $(this).text())
    });

    $('#subscribe1 .btn').click(function (e) {
        // e.preventDefault();
        $('#subscribe .wrap').removeAttr('style').height($('#subscribe .wrap').height());
        $('#subscribe .form').fadeOut(400, function () {
            $('#subscribe .loading').fadeIn(400);
            setTimeout(function () {
                $('#subscribe .loading').fadeOut(400, function () {

                    setTimeout(function () {
                        $('#subscribe .thanks').fadeOut(400, function () {
                            $('#subscribe .form').fadeIn(400);
                            $('#subscribe .wrap').removeAttr('style')
                        });
                    }, 3000);

                    $('#subscribe .thanks').fadeIn(400);
                });
            }, 2000);
        });

    });


    $('#sirMobile1 .btnSend').click(function (e) {
        //   e.preventDefault();
        var box = $(this).parents('.holder');
        box.removeAttr('style').height(box.height());
        box.find('.form').fadeOut(400, function () {
            box.find('.loading').fadeIn(400);
            setTimeout(function () {
                box.find('.loading').fadeOut(400, function () {

                    setTimeout(function () {
                        box.find('.thanks').fadeOut(400, function () {
                            box.find('.form').fadeIn(400);
                            box.find('.holder').removeAttr('style')
                        });
                    }, 3000);

                    box.find('.thanks').fadeIn(400);
                });
            }, 2000);
        });

    });


    $('#developments .col').each(function () {
        var img = $(this).find("> img").attr("src");
        $(this).css('backgroundImage', 'url(' + img + ')');
        $(this).find("> img").remove();
    });


    $(".expand a").on("click", function (e) {
        e.preventDefault();
        var tmp = $(this).parents('#middle').find('.mobile_hide');
        if (tmp.is(':hidden')) {
            tmp.hide().attr('data-old', 'mobile_hide').removeClass('mobile_hide').slideDown(400);
            $(this).text('Less').parent().addClass('act')
        } else {
            $('[data-old="mobile_hide"]').slideUp(400, function () {
                $(this).addClass('mobile_hide').removeAttr('style');
            });
            $(this).text('Read more').parent().removeClass('act');

            sc_to = $('[data-old="mobile_hide"]:eq(0)').parents('section').offset().top - parseInt($('#wrapper').css('paddingTop'));
            $('body,html').animate({scrollTop: sc_to}, 'slow');
        }
    });


    // Sothebys worldwide network columns
    if (( $(window).width() > 700 ) && ( $(window).width() < 1350 )) {
        $('.network ul').list2Columns(2);
    } else if ($(window).width() >= 1350) {
        $('.network ul').list2Columns(3);
    }


    $('.collapse h3 a').click(function (e) {
        e.preventDefault();
        var box = $(this).parent().next('.hide');
        box.slideToggle(400);
    });

    $('#footer .btnMoreInfo').click(function (e) {
        e.preventDefault();
        var tmp = $(this).parents('.moreInfo').find('.mobile_hide');
        if (tmp.is(':hidden')) {
            tmp.hide().attr('data-old', 'mobile_hide').removeClass('mobile_hide').slideDown(400);
            $(this).addClass('act').find('span').text('LESS');
            $(this).parents('.moreInfo').addClass('act')
        } else {
            $('[data-old="mobile_hide"]').slideUp(400, function () {
                $(this).addClass('mobile_hide').removeAttr('style');
            });
            $(this).removeClass('act').find('span').text('MORE INFORMATION');
            $(this).parents('.moreInfo').removeClass('act')
        }
    });


    // Various adjustments
    if (( $('.filterBtm1').length > 0 ) && ( $('.filterBtm').is(':visible') )) {
        if ($('.filterBtm').is_on_screen()) {
            $('.filterBtm').addClass('static');
        } else {
            $('.filterBtm').removeClass('static');
        }
    }

    if ($(window).width() <= 900) {
        if ($('.items').hasClass('listView')) {
            $('.items').removeClass('listView').attr('data-view', 'listView');
            $('#views a').removeClass('act');
            $('#views .icon_list').hide();
            $('#views .icon_grid').addClass('act');

            setTimeout(function () {
                $('.slider').slick('setPosition');
            }, 800);
        }
    } else {
        if ($('.items').attr('data-view')) {
            $('.items').addClass($('.items').attr('data-view')).removeAttr('data-view');
            $('#views a').removeClass('act');
            $('#views .icon_list').addClass('act').show();

            setTimeout(function () {
                $('.slider').slick('setPosition');
            }, 800);
        }
    }


});

$(window).scroll(function () {

    if (( $('.filterBtm1').length > 0 ) && ( $('.filterBtm').is(':visible') )) {
        if ($('.filterBtm').is_on_screen()) {
            $('.filterBtm').addClass('static');
        } else {
            $('.filterBtm').removeClass('static');
        }
    }

    /*if ($(this).scrollTop() > 0) {
     $('#searchBox').fadeOut();
     }
     else {
     $('#searchBox').fadeIn();
     }*/


})

$('.pageList').each(function () {


    var comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',')
    $(this).appear(function () {

        $('#office').animateNumber({number: 850}, 2000);
        $('#sales').animateNumber({
            number: 20000,
            numberStep: comma_separator_number_step
        }, 2000);
        $('#countries').animateNumber({number: 65}, 2000);

    });
});

$(window).resize(function () {
    sizeBoxes();

    if ($(window).width() > 1210 && 1) {
        $('body').removeClass('showFilters showMenu showPageMenu');
        $('#middle, body, #wrapper, html').removeAttr('style');
        $('body.no-scroll').unbind('touchmove');
        $('#mobileMenu, #mobilePageMenu').removeClass('act');
    }

    if ($(window).width() <= 900) {
        if ($('.items').hasClass('listView')) {
            $('.items').removeClass('listView').attr('data-view', 'listView');
            $('#views a').removeClass('act');
            $('#views .icon_list').hide();
            $('#views .icon_grid').addClass('act');
        }
    } else {
        if ($('.items').attr('data-view')) {
            $('.items').addClass($('.items').attr('data-view')).removeAttr('data-view');
            $('#views a').removeClass('act');
            $('#views .icon_list').addClass('act').show();
            setTimeout(function () {
                $('.slider').slick('setPosition');
            }, 800);
        }
    }

    if ($(window).width() >= 768) {
        $('body').removeClass('showMenu showPageMenu');
        $('#topMenu li, #topMenu .li_dd, #pageMenu li').removeClass('act');
        $('#wrapper, #topMenu .dd, #pageMenu .dd').removeAttr('style')
    }

    if ($('.slider').length > 0) {
        setTimeout(function () {
            $('.item .slider').slick('setPosition');
        }, 800);
    }

    if ($('.pp').length > 0) {
        $('.pp').height($(window).height())
    }

    if (( $(window).width() <= 1023 ) && ($('.ppSlider').length > 0)) {
        $('.pp .sliderHolder').width($(window).width())
    }

});

$(window).load(function () {

    sizeBoxes();

    if ($(window).width() <= 1024 && 0) {
        $('.slct_dd').each(function () {
            var tmp = $(this);
            tmp.append('<select></select>')
            tmp.find('.dd li').each(function () {
                txt = $(this).text();
                tmp.find('select').append('<option value="' + txt + '">' + txt + '</option>')
            });

            tmp.find('.dd').css('display', 'none');
        });
    }

    if ($(window).width() <= 650) {
        if ($('#sidebar.filterBy').length > 0) {
            $('#sidebar.filterBy').appendTo('#wrapper');
        }
    }

    if ($('.pp').length > 0) {
        $('.pp').height($(window).height())
    }
    if (( $(window).width() <= 1023 ) && ($('.ppSlider').length > 0)) {
        $('.pp .sliderHolder').width($(window).width())
    }

    if (( $(window).width() <= 700 ) && ($('.ppSlider').length > 0)) {
        $('.pp .content > .sidebar').appendTo('.ppSlider');
    }

    $('.slct_dd').find('b').each(function () {
        /*if ($(this).find('span') && $(this).find('span').hasClass('code'))
         $(this).attr('data-prev-val',$(this).find('span').text());
         else
         $(this).attr('data-prev-val', $(this).text());*/
    });
});

$(document).ready(function () {
    $(".datepicker").datepicker({
        changeMonth: true,
        dateFormat: "yy-mm-dd",
        minDate: 0
    });
});

$(window).resize(function () {
    sizeBoxes();

    if ($(window).width() > 650) {
        $('body').removeClass('showFilters showMenu showPageMenu');
        $('#middle, body, #wrapper, html').removeAttr('style');
        $('body.no-scroll').unbind('touchmove');
        $('#mobileMenu, #mobilePageMenu').removeClass('act');
    }

    if ($(window).width() <= 900) {
        if ($('.items').hasClass('listView')) {
            $('.items').removeClass('listView').attr('data-view', 'listView');
            $('#views a').removeClass('act');
            $('#views .icon_list').hide();
            $('#views .icon_grid').addClass('act');
        }
    } else {
        if ($('.items').attr('data-view')) {
            $('.items').addClass($('.items').attr('data-view')).removeAttr('data-view');
            $('#views a').removeClass('act');
            $('#views .icon_list').addClass('act').show();
            setTimeout(function () {
                $('.slider').slick('setPosition');
            }, 800);
        }
    }

    if ($(window).width() >= 768) {
        $('body').removeClass('showMenu showPageMenu');
        $('#topMenu li, #topMenu .li_dd, #pageMenu li').removeClass('act');
        $('#wrapper, #topMenu .dd, #pageMenu .dd').removeAttr('style')
    }

    if ($('.slider').length > 0) {
        setTimeout(function () {
            $('.item .slider').slick('setPosition');
        }, 800);
    }

    if ($('.pp').length > 0) {
        $('.pp').height($(window).height())
    }

    if (( $(window).width() <= 1023 ) && ($('.ppSlider').length > 0)) {
        $('.pp .sliderHolder').width($(window).width())
    }

    if ($(window).width() < 1024) {
        isHandHeld = true;
        $('body').addClass('is_handheld');
    } else {
        isHandHeld = false;
        $('body').removeClass('is_handheld');
    }

});

$(window).load(function () {

    sizeBoxes();

    if ($(window).width() <= 1024 && 0) {
        $('.slct_dd').each(function () {
            var tmp = $(this);
            tmp.append('<select></select>')
            tmp.find('.dd li').each(function () {
                txt = $(this).text();
                tmp.find('select').append('<option value="' + txt + '">' + txt + '</option>')
            });

            tmp.find('.dd').css('display', 'none');
        });
    }

    if ($(window).width() <= 650) {
        if ($('#sidebar.filterBy').length > 0) {
            $('#sidebar.filterBy').appendTo('#wrapper');
        }
    }

    if ($('.pp').length > 0) {
        $('.pp').height($(window).height())
    }
    $('.weUseCookies .close').click(function (e) {
        e.preventDefault();
        createCookie('weUseCookies', '1', 365);
        $('.weUseCookies').slideUp()
    });
    if ($('.weUseCookies').length > 0) {
        if (readCookie('weUseCookies') == null) {
            setTimeout(function () {
                $('.weUseCookies').slideDown();
            }, 400);
        }
    }

    if (( $(window).width() <= 1023 ) && ($('.ppSlider').length > 0)) {
        $('.pp .sliderHolder').width($(window).width())
    }

    if (( $(window).width() <= 700 ) && ($('.ppSlider').length > 0)) {
        $('.pp .content > .sidebar').appendTo('.ppSlider');
    }


});
/**
 * Effects
 */

$(document).ready(function () {

    if ($(window).width() <= 1024) {
        $('#pageMenu li.dd a').click(function () {
            var $t = $(this);
            if ($t.parent().hasClass('showmobilemenu')) {
                $t.parent().removeClass('showmobilemenu');
            } else {
                $t.parent().addClass('showmobilemenu');
            }

        });
    }

    $('.slct_dd.phone_prefix .dd li a').on("click", function (e) {


        e.preventDefault();
        var txt = $(this).find('.code').text();
        var val = $(this).attr('data-val');
        //alert(val);
        $(this).parents('form').find('.slct_dd').removeClass('act');
        cls = '';
        if ($(this).hasClass('act')) {
            return;
        } else {
            //alert('hh');
            if ($(this).hasClass('flag')) {
                cls = $(this).attr('class');
            }

            $(this).parents('.slct_dd').find('li a').removeClass('act');
            $(this).addClass('act');
            $(this).parents('.slct_dd').find('b').text(txt).removeAttr('class').addClass(cls);
            $(this).parents('.slct_dd').find('input').val(txt);
            $(this).parents('.slct_dd').click();
            $('.slct_dd.act').removeClass('act');
        }

    });

    if (isHandHeld == false) {
        var animatedElems = $('[data-animated]');
        animatedElems.each(function () {
            var elem = $(this);
            elem.waypoint({
                triggerOnce: true,
                offset: '100%',
                handler: function () {
                    if (elem.data('anim-delay') !== 'undefined') {
                        setTimeout(function () {
                            elem.css('visibility', 'visible').addClass('animated ' + elem.data('anim-effect'));
                        }, elem.data('anim-delay'));
                    } else {
                        elem.css('visibility', 'visible').addClass('animated ' + elem.data('anim-effect'));
                    }
                }
            });
        });
    }


    if ($(".imgSlider").length > 0) {

        $('.imgSlider').each(function () {
            var slider = $(this);

            var is_slides = slider.find('> div').length;
            var is_start = Math.floor((Math.random() * is_slides));
            var is_dots = false;

            if ($(this).hasClass('wDots')) {
                bs_dots = true;
            }

            $(this)
                .slick({
                    //initialSlide: is_start,
                    //fade: true,
                    adaptiveHeight: false,
                    dots: is_dots,
                    infinite: true,
                    accessibility: false,
                    speed: 800,
                    arrows: false,
                    pauseOnHover: false,
                    autoplay: false,
                    autoplaySpeed: 7000,
                    rtl: false
                });

            $(this).parent().find('.controls .btnPrev').click(function (e) {
                e.preventDefault();
                slider.slick('slickPrev');
            });

            $(this).parent().find('.controls .btnNext').click(function (e) {
                e.preventDefault();
                slider.slick('slickNext');
            });

        });

    }

    function scrollToElement(e) {
        $('html, body').animate({
            scrollTop: e.offset().top - 100
        }, 2000);

    }

    
    function validationForm($form) {
        $('p.errorInput').remove();
        $('.ip-error').removeClass('ip-error');
        $('.error').removeClass('error');
        // $('.inpt').addClass('valid');
        var $return = true;
        if ($form.find("input[name*='[full_name]']").length
            && (
                $form.find("input[name*='[full_name]']").val().length == 0
                || !onlyLetters($form.find("input[name*='[full_name]']").val())
                || $form.find("input[name*='[full_name]']").val().length > 100
            )
        ) {
            console.log(1);
            $form.find("input[name*='[full_name]']").addClass('ip-error');
            $form.find("input[name*='[full_name]']").addClass('error');
            $form.find("input[name*='[full_name]']").removeClass('valid');
            $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Full name should not be blank, Only letters are allowed,Max 100 characters are allowed</p>")
                .insertBefore($form.find("input[name*='[full_name]']"));
            $return = false;
        }
        else {
            $form.find("input[name*='[full_name]']").removeClass('ip-error');
            $form.find("input[name*='[full_name]']").removeClass('error');
            $form.find("input[name*='[full_name]']").addClass('valid');
            if ($form.find("input[name*='[fullname]']").length
                && (
                    $form.find("input[name*='[fullname]']").val().length == 0
                    || !onlyLetters($form.find("input[name*='[fullname]']").val())
                    || $form.find("input[name*='[fullname]']").val().length > 100
                )
            ) {
                console.log(2);
                $form.find("input[name*='[fullname]']").addClass('ip-error');
                $form.find("input[name*='[fullname]']").addClass('error');
                $form.find("input[name*='[full_name]']").removeClass('valid');
                $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Full name should not be blank, Only letters are allowed,Max 100 characters are allowed</p>")
                    .insertBefore($form.find("input[name*='[fullname]']"));
                $return = false;
            } else {
                $form.find("input[name*='[fullname]']").removeClass('ip-error');
                $form.find("input[name*='[fullname]']").removeClass('error');
                $form.find("input[name*='[fullname]']").addClass('valid');

                if ($form.find("input[name*='[first_name]']").length
                    && (
                        $form.find("input[name*='[first_name]']").val().length == 0
                        || !onlyLetters($form.find("input[name*='[first_name]']").val())
                        || $form.find("input[name*='[first_name]']").val().length > 100
                    )
                ) {
                    console.log(3);
                    $form.find("input[name*='[first_name]']").addClass('ip-error');
                    $form.find("input[name*='[first_name]']").addClass('error');
                    $form.find("input[name*='[first_name]']").removeClass('valid');
                    $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>First name should not be blank, Only letters are allowed,Max 100 characters are allowed</p>")
                        .insertBefore($form.find("input[name*='[first_name]']"));
                    $return = false;
                } else {
                    $form.find("input[name*='[first_name]']").removeClass('ip-error');
                    $form.find("input[name*='[first_name]']").removeClass('error');
                    $form.find("input[name*='[first_name]']").addClass('valid');
                    if ($form.find("input[name*='[last_name]']").length
                        && (
                            $form.find("input[name*='[last_name]']").val().length == 0
                            || !onlyLetters($form.find("input[name*='[last_name]']").val())
                            || $form.find("input[name*='[last_name]']").val().length > 100
                        )
                    ) {
                        console.log(4);
                        $form.find("input[name*='[last_name]']").addClass('ip-error');
                        $form.find("input[name*='[last_name]']").addClass('error');
                        $form.find("input[name*='[last_name]']").removeClass('valid');
                        $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Last name should not be blank, Only letters are allowed,Max 100 characters are allowed</p>")
                            .insertBefore($form.find("input[name*='[last_name]']"));
                        $return = false;
                    } else {
                        $form.find("input[name*='[last_name]']").removeClass('ip-error');
                        $form.find("input[name*='[last_name]']").removeClass('error');
                        $form.find("input[name*='[last_name]']").addClass('valid');
                        if ($form.find("input[name*='[name]']").length
                            && (
                                $form.find("input[name*='[name]']").val().length == 0
                                || !onlyLetters($form.find("input[name*='[name]']").val())
                                || $form.find("input[name*='[name]']").val().length > 100
                            )
                        ) {
                            console.log(5);
                            $form.find("input[name*='[name]']").addClass('ip-error');
                            $form.find("input[name*='[name]']").addClass('error');
                            $form.find("input[name*='[name]']").removeClass('valid');
                            $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Name should not be blank, Only letters are allowed,Max 100 characters are allowed</p>")
                                .insertBefore($form.find("input[name*='[name]']"));
                            $return = false;
                        } else {
                            $form.find("input[name*='[name]']").removeClass('ip-error');
                            $form.find("input[name*='[name]']").removeClass('error');
                            $form.find("input[name*='[name]']").addClass('valid');
                            if ($form.find("input[name*='SendEmail[username]']").length
                                && (
                                    $form.find("input[name*='SendEmail[username]']").val().length == 0
                                    || !onlyLetters($form.find("input[name*='SendEmail[username]']").val())
                                    || $form.find("input[name*='SendEmail[username]']").val().length > 100
                                )
                            ) {
                                console.log(6);
                                $form.find("input[name*='SendEmail[username]']").addClass('ip-error');
                                $form.find("input[name*='SendEmail[username]']").addClass('error');
                                $form.find("input[name*='SendEmail[username]']").removeClass('valid');
                                $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Name should not be blank, Only letters are allowed,Max 100 characters are allowed</p>")
                                    .insertBefore($form.find("input[name*='SendEmail[username]']"));
                                $return = false;
                            } else {
                                $form.find("input[name*='[username]']").removeClass('ip-error');
                                $form.find("input[name*='[username]']").removeClass('error');
                                $form.find("input[name*='[username]']").addClass('valid');
                                if ($form.find("input[name*='[email]']").length
                                    && (
                                        $form.find("input[name*='[email]']").val().length == 0
                                        || !isEmail($form.find("input[name*='[email]']").val())
                                        || $form.find("input[name*='[email]']").val().length > 100
                                    )

                                ) {
                                    console.log(7);
                                    $form.find("input[name*='[email]']").addClass('ip-error');
                                    $form.find("input[name*='[email]']").addClass('error');
                                    $form.find("input[name*='[email]']").removeClass('valid');
                                    $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Email address should be valid, Max 100 characters are allowed</p>")
                                        .insertBefore($form.find("input[name*='[email]']"));
                                    $return = false;
                                } else {
                                    $form.find("input[name*='[email]']").removeClass('ip-error');
                                    $form.find("input[name*='[email]']").removeClass('error');
                                    $form.find("input[name*='[email]']").addClass('valid');
                                    if ($form.find("input[name*='[phone]']").length
                                        && (
                                            $form.find("input[name*='[phone]']").val().length == 0
                                            || !isPhone($form.find("input[name*='[phone]']").val())
                                            || $form.find("input[name*='[phone]']").val().length > 15
                                        )
                                    ) {
                                        console.log(8);
                                        $form.find("input[name*='[phone]']").next().addClass('ip-error');
                                        $form.find("input[name*='[phone]']").next().addClass('error');
                                        $form.find("input[name*='[phone]']").addClass('error');
                                        $form.find("input[name*='[phone]']").removeClass('valid');
                                        $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Phone no. should not be blank,Only numbers - and + are allowed,Max 15 characters are allowed</p>")
                                            .insertBefore($form.find("input[name*='[phone]']").parent());
                                        $return = false;
                                    } else {
                                        $form.find("input[name*='[phone]']").removeClass('ip-error');
                                        $form.find("input[name*='[phone]']").removeClass('error');
                                        $form.find("input[name*='[phone]']").addClass('valid');
                                        if ($form.find("input[name*='[number]']").length
                                            && (
                                                $form.find("input[name*='[number]']").val().length == 0
                                                || !isPhone($form.find("input[name*='[number]']").val())
                                                || $form.find("input[name*='[number]']").val().length > 15
                                            )
                                        ) {
                                            console.log(9);
                                            $form.find("input[name*='[number]']").next().addClass('ip-error');
                                            $form.find("input[name*='[number]']").next().addClass('error');
                                            $form.find("input[name*='[number]']").removeClass('valid');
                                            $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Phone no. should not be blank,Only numbers - and + are allowed,Max 15 characters are allowed</p>")
                                                .insertBefore($form.find("input[name*='[number]']").parent());
                                            $return = false;
                                        } else {
                                            $form.find("input[name*='[number]']").removeClass('ip-error');
                                            $form.find("input[name*='[number]']").removeClass('error');
                                            $form.find("input[name*='[number]']").addClass('valid');
                                            if ($form.find("input[name*='data[Contact][subject]']").length
                                                && (
                                                    $form.find("input[name*='data[Contact][subject]']").val().length == 0
                                                )
                                            ) {
                                                console.log(10);
                                                $form.find("input[name*='data[Contact][subject]']").parent().addClass('ip-error');
                                                $form.find("input[name*='data[Contact][subject]']").parent().addClass('error');
                                                $form.find("input[name*='data[Contact][subject]']").removeClass('valid');
                                                $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Please select subject</p>")
                                                    .insertBefore($form.find("input[name*='data[Contact][subject]']").parent());
                                                $return = false;
                                            } else {
                                                $form.find("input[name*='data[Contact][subject]']").removeClass('ip-error');
                                                $form.find("input[name*='data[Contact][subject]']").removeClass('error');
                                                $form.find("input[name*='data[Contact][subject]']").addClass('valid');
                                                if ($form.find("input[name*='Booking[messages]']").length
                                                    && (
                                                        $form.find("input[name*='Booking[message]']").val().length == 0
                                                    )
                                                ) {
                                                    console.log(11);
                                                    $form.find("input[name*='Booking[messages]']").parent().addClass('ip-error');
                                                    $form.find("input[name*='Booking[message]']").parent().addClass('error');
                                                    $form.find("input[name*='Booking[message]']']").removeClass('valid');
                                                    $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Please select subject</p>")
                                                        .insertBefore($form.find("input[name*='Booking[message]']"));
                                                    $return = false;
                                                } else {
                                                    $form.find("input[name*='data[Contact][subject]']").removeClass('ip-error');
                                                    $form.find("input[name*='data[Contact][subject]']").removeClass('error');
                                                    $form.find("input[name*='data[Contact][subject]']").addClass('valid');
                                                    if ($form.find("input[name*='OfferRequest[message]']").length
                                                        && (
                                                            $form.find("input[name*='OfferRequest[message]']").val().length == 0
                                                        )
                                                    ) {
                                                        console.log(12);
                                                        /*$form.find("input[name*='data[Career][title]']").parent().addClass('ip-error');
                                                         $form.find("input[name*='data[Career][title]']").parent().addClass('error');
                                                         $form.find("input[name*='data[Career][title]']']").removeClass('valid');
                                                         $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Please select title</p>")
                                                         .insertBefore($form.find("input[name*='data[Career][title]']").parent());
                                                         $return = false;*/
                                                    } else {
                                                        $form.find("input[name*='OfferRequest[message]']").removeClass('ip-error');
                                                        $form.find("input[name*='OfferRequest[message]']").removeClass('error');
                                                        $form.find("input[name*='OfferRequest[message]']").addClass('valid');
                                                        if ($form.find("input[name*='OfferRequest[message]']").length
                                                            && (
                                                                $form.find("input[name*='OfferRequest[message]']").val().length == 0
                                                            )
                                                        ) {
                                                            console.log(13);
                                                            $form.find("input[name*='OfferRequest[message]']").parent().addClass('ip-error');
                                                            $form.find("input[name*='OfferRequest[message]']").parent().addClass('error');
                                                            $form.find("input[name*='OfferRequest[message]']']").removeClass('valid');
                                                            $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Please select subject</p>")
                                                                .insertBefore($form.find("input[name*='OfferRequest[message]']").parent());
                                                            $return = false;
                                                        } else {
                                                            $form.find("input[name*='OfferRequest[message]']").removeClass('ip-error');
                                                            $form.find("input[name*='OfferRequest[message]']").removeClass('error');
                                                            $form.find("input[name*='OfferRequest[message]']").addClass('valid');
                                                            if ($form.find("input[name*='Booking[times]']").length
                                                                && (
                                                                    $form.find("input[name*='Booking[time]']").val().length == 0
                                                                )
                                                            ) {
                                                                console.log(14);
                                                                $form.find("input[name*='Booking[time]']").parent().addClass('ip-error');
                                                                $form.find("input[name*='Booking[time]']").parent().addClass('error');
                                                                $form.find("input[name*='Booking[time]']']").removeClass('valid');
                                                                $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Please select preferred time</p>")
                                                                    .insertBefore($form.find("input[name*='Booking[time]']").parent());
                                                                $return = false;
                                                            } else {
                                                                $form.find("input[name*='Booking[time]']").removeClass('ip-error');
                                                                $form.find("input[name*='Booking[time]']").removeClass('error');
                                                                $form.find("input[name*='Booking[time]']").addClass('valid');
                                                                if ($form.find("input[name*='Booking[relocating_date]']").length
                                                                    && (
                                                                        $form.find("input[name*='Booking[relocating_date]']").val().length == 0

                                                                    )
                                                                ) {
                                                                    console.log(15);
                                                                    $form.find("input[name*='Booking[relocating_date]']").addClass('ip-error');
                                                                    $form.find("input[name*='Booking[relocating_date]']").addClass('error');
                                                                    $form.find("input[name*='Booking[relocating_date]']']").removeClass('valid');
                                                                    $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Please select preferred date</p>")
                                                                        .insertBefore($form.find("input[name*='Booking[relocating_date]']"));
                                                                    $return = false;
                                                                } else {
                                                                    $form.find("input[name*='Booking[relocating_date]']").removeClass('ip-error');
                                                                    $form.find("input[name*='Booking[relocating_date]']").removeClass('error');
                                                                    $form.find("input[name*='Booking[relocating_date]']").addClass('valid');
                                                                    if ($form.find("input[name*='SendEmail[useremail]']").length
                                                                        && (
                                                                            $form.find("input[name*='SendEmail[useremail]']").val().length == 0
                                                                            || !isEmail($form.find("input[name*='SendEmail[useremail]']").val())
                                                                            || $form.find("input[name*='SendEmail[useremail]']").val().length > 100
                                                                        )

                                                                    ) {
                                                                        console.log(16);
                                                                        $form.find("input[name*='SendEmail[useremail]']").addClass('ip-error');
                                                                        $form.find("input[name*='SendEmail[useremail]']").addClass('error');
                                                                        $form.find("input[name*='SendEmail[useremail]']").removeClass('valid');
                                                                        $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Email address should be valid, Max 100 characters are allowed</p>")
                                                                            .insertBefore($form.find("input[name*='SendEmail[useremail]']"));
                                                                        $return = false;
                                                                    } else {
                                                                        $form.find("input[name*='SendEmail[useremail]']").removeClass('ip-error');
                                                                        $form.find("input[name*='SendEmail[useremail]']").removeClass('error');
                                                                        $form.find("input[name*='SendEmail[useremail]']").addClass('valid');
                                                                        if ($form.find("input[name*='SendEmail[friendemails]']").length
                                                                            && (
                                                                                $form.find("input[name*='SendEmail[friendemails]']").val().length == 0
                                                                                || !isMultipleEmail($form.find("input[name*='SendEmail[friendemails]']").val())
                                                                                || $form.find("input[name*='SendEmail[friendemails]']").val().length > 100
                                                                            )

                                                                        ) {
                                                                            console.log(17);
                                                                            $form.find("input[name*='SendEmail[friendemails]']").addClass('ip-error');
                                                                            $form.find("input[name*='SendEmail[friendemails]']").addClass('error');
                                                                            $form.find("input[name*='SendEmail[friendemails]']").removeClass('valid');
                                                                            $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Email addresses should be valid, Max 100 characters are allowed</p>")
                                                                                .insertBefore($form.find("input[name*='SendEmail[friendemails]']"));
                                                                            $return = false;
                                                                        } else {
                                                                            $form.find("input[name*='SendEmail[friendemails]']").removeClass('ip-error');
                                                                            $form.find("input[name*='SendEmail[friendemails]']").removeClass('error');
                                                                            $form.find("input[name*='SendEmail[friendemails]']").addClass('valid');
                                                                            /** Career form */
                                                                            if (typeof grecaptcha != 'undefined' && $form.find('.g-recaptcha').length > 0 && grecaptcha.getResponse().length === 0) {
                                                                                console.log(26);
                                                                                $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Please fill captcha</p>")
                                                                                    .insertBefore($form.find('.g-recaptcha'));
                                                                                grecaptcha.reset();
                                                                                $return = false;
                                                                            } else {
                                                                                if ($form.attr('id') == 'careerApplicationForm') {
                                                                                    var $cvupload = $form.find('input[type=file]');
                                                                                    if ($cvupload.get(0).files.length === 0) {
                                                                                        console.log(18);
                                                                                        $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Please upload your CV</p>")
                                                                                            .insertBefore($form.find(".browse"));
                                                                                        $return = false;
                                                                                    } else if ($cvupload.get(0).files[0].size > 5242880) {
                                                                                        console.log(19);
                                                                                        $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Upload CV size limit exceeded, Max 5MB file is allowed</p>")
                                                                                            .insertBefore($form.find(".browse"));
                                                                                        $return = false;
                                                                                    } else if ($.inArray($cvupload.get(0).files[0].type, allowedFileTypes) == -1) {
                                                                                        console.log(20);
                                                                                        $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Invalid file selected only .doc .docx .jpg .pdf . rtf .png are allowed</p>")
                                                                                            .insertBefore($form.find(".browse"));
                                                                                        $return = false;
                                                                                    } else {
                                                                                        /** Career form */
                                                                                        if (typeof grecaptcha != 'undefined' && $form.find('.g-recaptcha').length > 0 && grecaptcha.getResponse().length === 0) {
                                                                                            console.log(21);
                                                                                            $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Please fill captcha</p>")
                                                                                                .insertBefore($form.find('.g-recaptcha'));
                                                                                            grecaptcha.reset();
                                                                                            $return = false;
                                                                                        }
                                                                                    }
                                                                                } else if ($form.hasClass('EnquireForm') || $form.attr('class') == 'EnquireForm form_ajax_validate') {
                                                                                    if ($form.find('input[name*=FullName]').length
                                                                                        && (
                                                                                            $form.find("input[name*=FullName]").val().length == 0
                                                                                            || !onlyLetters($form.find("input[name*=FullName]").val())
                                                                                            || $form.find("input[name*=FullName]").val().length > 100
                                                                                        )
                                                                                    ) {
                                                                                        console.log(22);
                                                                                        $form.find("input[name*=FullName]").addClass('ip-error');
                                                                                        $form.find("input[name*=FullName]").addClass('error');
                                                                                        $form.find("input[name*=FullName]").removeClass('valid');
                                                                                        $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Full name should not be blank, Only letters are allowed,Max 100 characters are allowed</p>")
                                                                                            .insertBefore($form.find("input[name*=FullName]"));
                                                                                        $return = false;
                                                                                    } else {
                                                                                        $form.find("input[name*=FullName]").removeClass('ip-error');
                                                                                        $form.find("input[name*=FullName]").removeClass('error');
                                                                                        $form.find("input[name*=FullName]").addClass('valid');
                                                                                        if ($form.find("input[name*=Email]").length
                                                                                            && (
                                                                                                $form.find("input[name*=Email]").val().length == 0
                                                                                                || !isEmail($form.find("input[name*=Email]").val())
                                                                                                || $form.find("input[name*=Email]").val().length > 100
                                                                                            )

                                                                                        ) {
                                                                                            console.log(23);
                                                                                            $form.find("input[name*=Email]").addClass('ip-error');
                                                                                            $form.find("input[name*=Email]").addClass('error');
                                                                                            $form.find("input[name*=Email]").removeClass('valid');
                                                                                            $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Email address should be valid, Max 100 characters are allowed</p>")
                                                                                                .insertBefore($form.find("input[name*=Email]"));
                                                                                            $return = false;
                                                                                        } else {
                                                                                            $form.find("input[name*=Email]").removeClass('ip-error');
                                                                                            $form.find("input[name*=Email]").removeClass('error');
                                                                                            $form.find("input[name*=Email]").addClass('valid');
                                                                                            if ($form.find("input[name*=MobileNumber]").length
                                                                                                && (
                                                                                                    $form.find("input[name*=MobileNumber]").val().length == 0
                                                                                                    || !isPhone($form.find("input[name*=MobileNumber]").val())
                                                                                                    || $form.find("input[name*=MobileNumber]").val().length > 15
                                                                                                )
                                                                                            ) {
                                                                                                console.log(24);
                                                                                                $form.find("input[name*=MobileNumber]").addClass('ip-error');
                                                                                                $form.find("input[name*=MobileNumber]").addClass('error');
                                                                                                $form.find("input[name*=MobileNumber]").removeClass('valid');
                                                                                                $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Phone no. should not be blank,Only numbers - and + are allowed,Max 15 characters are allowed</p>")
                                                                                                    .insertBefore($form.find("input[name*=MobileNumber]"));
                                                                                                $return = false;
                                                                                            } else {
                                                                                                $form.find("input[name*=MobileNumber]").removeClass('ip-error');
                                                                                                $form.find("input[name*=MobileNumber]").removeClass('error');
                                                                                                $form.find("input[name*=MobileNumber]").addClass('valid');
                                                                                                if ($form.find("input[name*=MobileNumber]").length
                                                                                                    && (
                                                                                                        $form.find("input[name*=Nationality]").val().length == 0
                                                                                                        || $form.find("input[name*=Nationality]").val().length > 100
                                                                                                    )
                                                                                                ) {
                                                                                                    console.log(25);
                                                                                                    $form.find("input[name*=Nationality]").next().addClass('ip-error');
                                                                                                    $form.find("input[name*=Nationality]").next().addClass('error');
                                                                                                    $form.find("input[name*=Nationality]").removeClass('valid');
                                                                                                    $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Nationality should not be blank</p>")
                                                                                                        .insertBefore($form.find("input[name*=Nationality]"));
                                                                                                    $return = false;
                                                                                                } else {
                                                                                                    $form.find("input[name*=Nationality]").removeClass('ip-error');
                                                                                                    $form.find("input[name*=Nationality]").removeClass('error');
                                                                                                    $form.find("input[name*=Nationality]").addClass('valid');
                                                                                                    $return = true;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        //remove errors after 10 seconds
        setTimeout(function () {
            $('p.errorInput').remove();
            $('.ip-error').removeClass('ip-error');
            $('.error').removeClass('error');
            $return = true;
        }, 10000);

        //remove error on focus on a field
        $("form").on('focusin', function (event) {
            $(this).find(':focus').removeClass('ip-error');
            $(this).find(':focus').removeClass('error');
            $(this).find(':focus').prev('.errorInput').remove();
            $(this).find(':focus').next('div').removeClass('ip-error');
            $(this).find(':focus').next('div').removeClass('error');
            $(this).find(':focus').parent().prev('.errorInput').remove();
            // $(this).find('act').find(':focus').prev('.errorInput').remove();
        });
        if (!$return) {
            scrollToElement($form)
        }
        // console.log($return);
        return $return;
    }

    function validateForm($form) {
        $('p.errorInput').remove();
        $('.ip-error').removeClass('ip-error')
        var $return = true;
        if ($form.find("input[name*='[full_name]']").length
            && (
                $form.find("input[name*='[full_name]']").val().length == 0
                || !onlyLetters($form.find("input[name*='[full_name]']").val())
                || $form.find("input[name*='[full_name]']").val().length > 100
            )
        ) {
            $form.find("input[name*='[full_name]']").addClass('ip-error');
            $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Full name should not be blank, Only letters are allowed,Max 100 characters are allowed</p>")
                .insertBefore($form.find("input[name*='[full_name]']"));
            $return = false;
        }


        if ($form.find("input[name*='[fullname]']").length
            && (
                $form.find("input[name*='[fullname]']").val().length == 0
                || !onlyLetters($form.find("input[name*='[fullname]']").val())
                || $form.find("input[name*='[fullname]']").val().length > 100
            )
        ) {
            $form.find("input[name*='[fullname]']").addClass('ip-error');
            $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Full name should not be blank, Only letters are allowed,Max 100 characters are allowed</p>")
                .insertBefore($form.find("input[name*='[fullname]']"));
            $return = false;
        }


        if ($form.find("input[name*='[first_name]']").length
            && (
                $form.find("input[name*='[first_name]']").val().length == 0
                || !onlyLetters($form.find("input[name*='[first_name]']").val())
                || $form.find("input[name*='[first_name]']").val().length > 100
            )
        ) {
            $form.find("input[name*='[first_name]']").addClass('ip-error');
            $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>First name should not be blank, Only letters are allowed,Max 100 characters are allowed</p>")
                .insertBefore($form.find("input[name*='[first_name]']"));
            $return = false;
        }
        if ($form.find("input[name*='[last_name]']").length
            && (
                $form.find("input[name*='[last_name]']").val().length == 0
                || !onlyLetters($form.find("input[name*='[last_name]']").val())
                || $form.find("input[name*='[last_name]']").val().length > 100
            )
        ) {
            $form.find("input[name*='[last_name]']").addClass('ip-error');
            $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Last name should not be blank, Only letters are allowed,Max 100 characters are allowed</p>")
                .insertBefore($form.find("input[name*='[last_name]']"));
            $return = false;
        }
        if ($form.find("input[name*='[name]']").length
            && (
                $form.find("input[name*='[name]']").val().length == 0
                || !onlyLetters($form.find("input[name*='[name]']").val())
                || $form.find("input[name*='[name]']").val().length > 100
            )
        ) {
            $form.find("input[name*='[name]']").addClass('ip-error');
            $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Name should not be blank, Only letters are allowed,Max 100 characters are allowed</p>")
                .insertBefore($form.find("input[name*='[name]']"));
            $return = false;
        }
        if ($form.find("input[name*='SendEmail[username]']").length
            && (
                $form.find("input[name*='SendEmail[username]']").val().length == 0
                || !onlyLetters($form.find("input[name*='SendEmail[username]']").val())
                || $form.find("input[name*='SendEmail[username]']").val().length > 100
            )
        ) {
            $form.find("input[name*='SendEmail[username]']").addClass('ip-error');
            $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Name should not be blank, Only letters are allowed,Max 100 characters are allowed</p>")
                .insertBefore($form.find("input[name*='SendEmail[username]']"));
            $return = false;
        }

        if ($form.find("input[name*='[phone]']").length
            && (
                $form.find("input[name*='[phone]']").val().length == 0
                || !isPhone($form.find("input[name*='[phone]']").val())
                || $form.find("input[name*='[phone]']").val().length > 15
            )
        ) {
            $form.find("input[name*='[phone]']").next().addClass('ip-error');
            $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Phone no. should not be blank,Only numbers - and + are allowed,Max 15 characters are allowed</p>")
                .insertBefore($form.find("input[name*='[phone]']").parent());
            $return = false;
        }

        if ($form.find("input[name*='[number]']").length
            && (
                $form.find("input[name*='[number]']").val().length == 0
                || !isPhone($form.find("input[name*='[number]']").val())
                || $form.find("input[name*='[number]']").val().length > 15
            )
        ) {
            $form.find("input[name*='[number]']").next().addClass('ip-error');
            $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Phone no. should not be blank,Only numbers - and + are allowed,Max 15 characters are allowed</p>")
                .insertBefore($form.find("input[name*='[number]']").parent());
            $return = false;
        }

        if ($form.find("input[name*='data[Contact][subject]']").length
            && (
                $form.find("input[name*='data[Contact][subject]']").val().length == 0
            )
        ) {
            $form.find("input[name*='data[Contact][subject]']").parent().addClass('ip-error');
            $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Please select subject</p>")
                .insertBefore($form.find("input[name*='data[Contact][subject]']").parent());
            $return = false;
        }

        if ($form.find("input[name*='Booking[message]']").length
            && (
                $form.find("input[name*='Booking[message]']").val().length == 0
            )
        ) {
            $form.find("input[name*='Booking[message]']").parent().addClass('ip-error');
            $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Please select subject</p>")
                .insertBefore($form.find("input[name*='Booking[message]']").parent());
            $return = false;
        }

        if ($form.find("input[name*='data[Career][title]']").length
            && (
                $form.find("input[name*='data[Career][title]']").val().length == 0
            )
        ) {
            $form.find("input[name*='data[Career][title]']").parent().addClass('ip-error');
            $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Please select title</p>")
                .insertBefore($form.find("input[name*='data[Career][title]']").parent());
            $return = false;
        }

        if ($form.find("input[name*='OfferRequest[message]']").length
            && (
                $form.find("input[name*='OfferRequest[message]']").val().length == 0
            )
        ) {
            $form.find("input[name*='OfferRequest[message]']").parent().addClass('ip-error');
            $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Please select subject</p>")
                .insertBefore($form.find("input[name*='OfferRequest[message]']").parent());
            $return = false;
        }

        if ($form.find("input[name*='Booking[time]']").length
            && (
                $form.find("input[name*='Booking[time]']").val().length == 0
            )
        ) {
            $form.find("input[name*='Booking[time]']").parent().addClass('ip-error');
            $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Please select preferred time</p>")
                .insertBefore($form.find("input[name*='Booking[time]']").parent());
            $return = false;
        }

        if ($form.find("input[name*='Booking[relocating_date]']").length
            && (
                $form.find("input[name*='Booking[relocating_date]']").val().length == 0

            )
        ) {
            $form.find("input[name*='Booking[relocating_date]']").addClass('ip-error');
            $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Please select preferred date</p>")
                .insertBefore($form.find("input[name*='Booking[relocating_date]']"));
            $return = false;
        }

        if ($form.find("input[name*='[email]']").length
            && (
                $form.find("input[name*='[email]']").val().length == 0
                || !isEmail($form.find("input[name*='[email]']").val())
                || $form.find("input[name*='[email]']").val().length > 100
            )

        ) {
            $form.find("input[name*='[email]']").addClass('ip-error');
            $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Email address should be valid, Max 100 characters are allowed</p>")
                .insertBefore($form.find("input[name*='[email]']"));
            $return = false;
        }
        if ($form.find("input[name*='SendEmail[useremail]']").length
            && (
                $form.find("input[name*='SendEmail[useremail]']").val().length == 0
                || !isEmail($form.find("input[name*='SendEmail[useremail]']").val())
                || $form.find("input[name*='SendEmail[useremail]']").val().length > 100
            )

        ) {
            $form.find("input[name*='SendEmail[useremail]']").addClass('ip-error');
            $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Email address should be valid, Max 100 characters are allowed</p>")
                .insertBefore($form.find("input[name*='SendEmail[useremail]']"));
            $return = false;
        }

        if ($form.find("input[name*='SendEmail[friendemails]']").length
            && (
                $form.find("input[name*='SendEmail[friendemails]']").val().length == 0
                || !isMultipleEmail($form.find("input[name*='SendEmail[friendemails]']").val())
                || $form.find("input[name*='SendEmail[friendemails]']").val().length > 100
            )

        ) {
            $form.find("input[name*='SendEmail[friendemails]']").addClass('ip-error');
            $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Email addresses should be valid, Max 100 characters are allowed</p>")
                .insertBefore($form.find("input[name*='SendEmail[friendemails]']"));
            $return = false;
        }

        /** Career form */
        if ($form.attr('id') == 'careerApplicationForm') {
            var $cvupload = $form.find('input[type=file]');
            if ($cvupload.get(0).files.length === 0) {
                $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Please upload your CV</p>")
                    .insertBefore($form.find(".browse"));
                $return = false;
            } else if ($cvupload.get(0).files[0].size > 5242880) {
                $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Upload CV size limit exceeded, Max 5MB file is allowed</p>")
                    .insertBefore($form.find(".browse"))
                $return = false;
            } else if ($.inArray($cvupload.get(0).files[0].type, allowedFileTypes) == -1) {
                $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Invalid file selected only .doc .docx .jpg .pdf . rtf .png are allowed</p>")
                    .insertBefore($form.find(".browse"));
                $return = false;
            }
        }
        if (typeof grecaptcha != 'undefined' && $form.find('.g-recaptcha').length > 0 && grecaptcha.getResponse().length === 0) {
            $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Please fill captcha</p>")
                .insertBefore($form.find('.g-recaptcha'));
            grecaptcha.reset();
            $return = false;
        }


        if (!$return) {
            scrollToElement($form)
        }
//        console.log($return)
        return $return;
    }

    var allowedImageTypes = ['image/jpeg'];
    var allowedFileTypes = ['image/png', 'application/msword', 'application/pdf', 'image/jpeg', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'];

    function submitForm($form) {
        if ($form.attr('id') == 'careerApplicationForm') {

            var formData = new FormData();
            formData.append('data[Career][resume]', $('input[type=file]').get(0).files[0]);
            $($form.serializeArray()).each(function (i, field) {
                formData.append(field.name, field.value);
            });
            $.ajax({
                url: $form.attr('action'),
                type: 'POST',
                data: formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success: function (data) {
                }
            });
        }
        else {
            $.post($form.attr('action'), $form.serialize(), function (data) {

                // ---vmfa
                var formName = $form.data('form-name');
                var eventCategory = "Form";
                // console.log('form-name', formName);
                if (!formName) {
                  eventCategory = "FormDev";
                  var pathSegments = location.pathname.split('/');
                  if (pathSegments > 0) {
                    formName = pathSegments[pathSegments.length-1];
                  } else {
                    formName = $form.attr('id');
                  }
                }

                if (formName && ga) {
                  // console.log('we have form-name and ga');
                  ga('send', {
                    hitType: 'event',
                    eventCategory: eventCategory,
                    eventAction: 'Submit',
                    eventLabel: formName
                  });
                }
                // ---vmfa

                //$form.find('input').each(function () {
                //    if ($(this).attr('type') != 'hidden')
                //        $(this).val('');
                //});
                //// if (data == 'contact_us') {
                //    if ($('#DevelopmentProjectName').val() == 'Cayan Cantara'
                //        || $('#DevelopmentProjectName').val() == 'Cayan Arjaan'
                //        || $('#DevelopmentProjectName').val() == 'The Sustainable City'
                //        || $('#DevelopmentProjectName').val() == 'ONE PALM'
                //        || $('#DevelopmentProjectName').val() == 'XXIICarat'
                //        || $('#DevelopmentProjectName').val() == 'The_118'
                //        || $('#DevelopmentProjectName').val() == 'Jumeirah Hills The Palaces'
                //        || $('#DevelopmentProjectName').val() == '1JBR'
                //    ) {
                //        var str = window.location.href;
                //        var n = str.indexOf("?");
                //        //setTimeout(function () {
                //            if (n == -1) {
                //                window.location.href = window.location.href + '?thanks';
                //            } else {
                //                window.location.href = window.location.href + '&thanks';
                //            }
                //        //}, 2000);
                //    }
                // }
            });
        }
        console.log('resetForm.......');
        resetForm($form);
    }

    $("form").on('focusout', function (event) {
        if ($("form").attr('action') != '/fortheunique/contact' && $("form").attr('id') != 'LeadForm') {
            // $("input").on('change',function () {
            validationForm($(this));
            // });
        }
    });

    /*    $( "input[name*='[email]']" ).focusout(
     function() {
     var em = $(this).val();
     var $form = $(this).closest("form");
     var $action = $form.attr('action') + "?update=email";
     if(isEmail(em)&&($action.includes("/developments/contact_us")))
     $.post($action, $form.serialize(), function (data) {});
     });
     $( "input[name*='[phone]']" ).focusout(
     function() {
     var ph = $(this).val();
     var $form = $(this).closest("form");
     var $action = $form.attr('action') + "?update=phone";
     if(isPhone(ph)&&($action.includes("/developments/contact_us")))
     $.post($action, $form.serialize(), function (data) {});
     });*/
    var errorStyles = {
        border: "1px solid red",
    };
    var removeErrorStyles = {
        border: "",
    };
    //    Show form on Enquire now button click

    $('.EnquireBtn').on('click', function () {
        var $dataId = $(this).attr('data-id');
        if ($(this).parents('.contain').hasClass($dataId)) {
            $('.formEnquire-' + $dataId).slideUp();
            $(this).parents('.contain').removeClass($dataId);
            $(this).parents('.contain').removeClass('opened');
        } else {
            if ($(this).parents('#middle, .similarItems').find('.contain').hasClass('opened')) {
                $('.contactForm').slideUp();
                $(this).parents('#middle, .similarItems').find('.contain').attr('class', 'contain');
            }
            $('.formEnquire-' + $dataId).slideDown();
            $(this).parents('.contain').addClass($dataId);
            $(this).parents('.contain').addClass('opened');
        }
    });

    var siteUrl = window.location.origin + '/';
    var emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var letters = /^[A-Za-z ]+$/;
    var phoneRegex = /^\+?[\d ]+$/;

    function scrollToElement(e, p, m) {

        /*if ($(document).height() > $(window).height()) {
         if (e.parents(p).length != 0) {
         $('html, body').animate({
         scrollTop: e.offset().top - 120
         }, 500);
         }
         }*/
        e.focus();
        if (m.length > 0 && e.prev().attr('class') != 'errorInput') {
            $("<p class='errorInput' style='color:#CC3300;padding: 0 0 10px;'>" + m + "</p>").insertBefore(e);
        } else {
            $("<p class='errorInput' style='color:#CC3300;padding: 0 0 10px;'>" + m + "</p>").remove(e);
        }
    }


    function resetForm(f) {
        var lang = f.find("input[name=lang]").val();
        f.find("input[type=text], textarea,select").val("");
        f.find("input[type=email]").val("");
        f.find("input[type=tel]").val("");
        f.find("select").prop('selectedIndex', 0);
        if (f.find('.shortInput').length) {
            f.find('.shortInput').find('b').text('Select Title')
        }
        $('input[type=hidden]').val('');
        f.find("input[name=lang]").val(lang);
        f.find('input[type=checkbox]').prop('checked', false)
        f.find('input[type=radio]').prop('checked', false)
        // f.find('.slct_dd').find('b').text('Select option');
        f.find('.dd').find('ul li a').removeClass('act');
        $('p.errorInput').remove();
        $('.ip-error').removeClass('ip-error');
        $('.error').removeClass('error');
        $('.valid').removeClass('valid');
        f.find("input[type=text],input[type=email], textarea,select").val("");
        //f.find("select").prop('selectedIndex', 0);
        f.find('.slct_dd').find('b').each(function () {
            $(this).text($(this).attr('data-prev-val'));
        });
        f.find('.slct_dd').find('input[type=hidden]').not('input[name*=ph_ext]').val("");
        //$('input[type=hidden]').val('');
        f.find('input[type=checkbox]').prop('checked', false);
        if (typeof grecaptcha != 'undefined') {
            grecaptcha.reset();
        }
    }

    $('input[name=MobileNumber]').change(save_before_submit);
    $('input[name=Email]').change(save_before_submit);

    function save_before_submit() {
        var $formId = $('#' + $(this).parents('.EnquireForm').attr('id'));
        if ($('#project_category').val() != 'listing_form') return;
        var ph = $(this).val();
        var $action = "/properties/enquirenow?update=1";
        if (isPhone(ph) || isEmail(ph)) {
            $.post($action, $formId.serialize(), function (data) {
            });
        }
    }

    $('.EnquireForm').on('submit', function (e) {
        e.preventDefault();
        var $form = $(this);
        var $dataId = $(this).attr('data-key');
        // var formId = '#' + $('.formIdField-' + $dataId).val();
        // var $parent = $(this).parents(formId);
        var $first_name = $form.find('input[name=FullName]');
        var $mob_number = $form.find('input[name=MobileNumber]');
        var $email = $form.find('input[name=Email]');
        var $country = $form.find('input[name=Nationality]');
        var $message = $form.find('textarea[name=Message]');
        var $propertyRefNo = $form.find('input[name=propertyRefNo]');

        // ---vmfa
        var propRefNo = $form.find('input[name=propertyRefNo]').val();
        // console.log('propRefNo', propRefNo);
        // ---vmfa

        /*if ($first_name.val().length === 0 || $first_name.val().length > 100 || !onlyLetters($first_name.val())) {
         $first_name.css(errorStyles);
         scrollToElement($first_name, formId, 'Full name not valid');
         return false;
         } else {
         $($first_name).prev('p.errorInput').remove();
         $($first_name).css(removeErrorStyles);
         }
         if ($email.val().length === 0 || !isEmail($email.val()) || $email.val().length > 100) {
         $email.css(errorStyles);
         scrollToElement($email, formId, 'Email address not valid');
         return false;
         } else {
         $($email).prev('p.errorInput').remove();
         $($email).css(removeErrorStyles);
         }
         if ($mob_number.val().length === 0 || $mob_number.val().length > 15 || !isPhoneAnyLength($mob_number.val())) {
         $mob_number.css(errorStyles);
         // scrollToElement($mob_number, '');
         scrollToElement($mob_number, formId, 'Phone no. not valid');
         return false;
         } else {
         $($mob_number).prev('p.errorInput').remove();
         $($mob_number).css(removeErrorStyles);
         }*/
        /* if ($country.val().length === 0 || $country.val().length > 100) {
         $country.css(errorStyles);
         scrollToElement($country, formId, 'Nationality not valid');
         return false;
         } else {
         $($country).prev('p.errorInput').remove();
         $($country).css(removeErrorStyles);
         }*/
        if (validationForm($form)) {
            $form.addClass('ThanksLoading').append('<div class="ThanksLoader"></div>');
            $.ajax({
                type: 'post',
                url: siteUrl + 'properties/enquirenow',
                data: $form.serialize(),
                success: function () {
                    // Then reset the form
                    $('.thanks').show();
                    $form.removeClass('ThanksLoading');
                    $form.find('.ThanksLoader').remove();

                    // ---vmfa
                    if (propRefNo && ga){
                      // console.log('enquire form ga malo');
                      ga('send', {
                        hitType: 'event',
                        eventCategory: 'FormEnquire',
                        eventAction: 'Submit',
                        eventLabel: propRefNo
                      });
                    }
                    // ---vmfa

                    setTimeout(function () {
                        goog_report_conversion(window.location.href);
                        $("input[type=text],input[type=email]").val("");
                        $(".contactForm textarea").val("I would like to enquire about this property " + $propertyRefNo.val() + ". Please contact me at your earliest convenience.");
                        $('.thanks').hide();
                        $('.formEnquire-' + $dataId).slideToggle();
                        $('.formEnquire-' + $dataId).parents('.contain').removeClass($dataId);
                        $('.formEnquire-' + $dataId).parents('.contain').removeClass('opened');
                        resetForm($form);
                    }, 3000);
                }, error: function (error) {
                    console.log(error);
                }
            });
        }
    });

    $(".EnquireForm textarea")
        .focus(function () {
            if (this.value === '') {
                this.value = this.defaultValue;
            }
        })
        .blur(function () {
            if (this.value === '') {
                this.value = this.defaultValue;
            }
        });

    $('.agentsUpdateForm').submit(function (e) {
        var $form = $(this);
        e.preventDefault();
        $valid = true;
        var $cvupload = $form.find('input[type=file]');
        if ($cvupload.get(0).files.length > 0) {
            if ($cvupload.get(0).files[0].size > 2097152) {
                $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Upload image size limit exceeded, Max 2MB file is allowed</p>")
                    .insertBefore($form.find(".browse"))
                $valid = false;
            } else if ($.inArray($cvupload.get(0).files[0].type, allowedImageTypes) == -1) {
                $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Invalid file selected only .jpg is allowed</p>")
                    .insertBefore($form.find(".browse"));
                $valid = false;
            }
        }

        if ($valid) {
            var formData = new FormData();
            formData.append('data[Agent][image]', $form.find('input[type=file]').get(0).files[0]);
            $($form.serializeArray()).each(function (i, field) {
                formData.append(field.name, field.value);
            });
            $.ajax({
                url: '/agents/update_save',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function (data) {
                    alert('Agent updated successfully');
                    var agent_image_id = '#' + data + '-img';
                    var agent_image_src = $(agent_image_id).attr("src");
                    var a = agent_image_src.split('?');
                    var new_agent_image_src = a[0] + '?v' + new Date().getTime();
                    $(agent_image_id).removeAttr("src").attr("src", new_agent_image_src);
                }
            });
        }
        return false;
    });
    $('.homesliderUpdateForm').submit(function (e) {
        var $form = $(this);
        var allowedImageTypes = ['video/mp4'];
        e.preventDefault();
        $valid = true;
        var $cvupload = $form.find('input[type=file]');
        if ($cvupload.get(0).files.length > 0) {
            if ($cvupload.get(0).files[0].size > 2097152) {
                $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Upload image size limit exceeded, Max 2MB file is allowed</p>")
                    .insertBefore($form.find(".browse"))
                $valid = false;
            } else if ($.inArray($cvupload.get(0).files[0].type, allowedImageTypes) == -1) {
                $("<p class='errorInput' style='font-size: smaller;color: red;padding: 0;text-align:left;'>Invalid file selected only .jpg is allowed</p>")
                    .insertBefore($form.find(".browse"));
                $valid = false;
            }
        }

        if ($valid) {
            var formData = new FormData();
            formData.append('data[Homeslider][video]', $form.find('input[type=file]').get(0).files[0]);
            $($form.serializeArray()).each(function (i, field) {
                formData.append(field.name, field.value);
            });
            $($form).fadeOut();
            $($form).next('.sending').fadeIn();
            $.ajax({
                url: '/properties/update_save',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function (data) {
                    $($form).next('.sending').fadeOut();
                    $($form).next('.sending').next('.sent').html($.trim(data));
                    $($form).next('.sending').next('.sent').fadeIn();
                    setTimeout(function () {
                        $($form).next('.sending').next('.sent').fadeOut();
                        $($form).fadeIn();
                    }, 4000);
                    // alert('Slider updated successfully');
                    /*var slider_video_id = '#' + data + '-video';
                     var slider_video_src = $(slider_video_id).attr("src");
                     var a = slider_video_src.split('?');
                     var new_slider_video_src = a[0] + '?v' + new Date().getTime();
                     $(slider_video_id).removeAttr("src").attr("src", new_slider_video_src);*/
                }
            });
        }
        return false;
    });

    $('.form_ajax_validate, #form_ajax_validate_subsribe').submit(function (e) {
        var $form = $(this);
        e.preventDefault();
        var isValidForm = true;
        if (!$form.hasClass('EnquireForm')) {
            if ($($form).attr('action') != '/fortheunique/contact') {
                if (validationForm($form)) {
                    // console.log(isValidForm);
                    // debugger;
                    //goog_report_conversion (window.location.href);
                    var holder = $(this).parents('.formBox').find('.holder');
                    if (holder.length) {
                        holder.removeAttr('style').height(holder.height());
                        holder.find('.form').fadeOut(400, function () {
                            holder.find('.sending').fadeIn(400);
                            setTimeout(function () {
                                holder.find('.sending').fadeOut(400, function () {
                                    setTimeout(function () {
                                        holder.find('.sent').fadeOut(400, function () {
                                            holder.find('.form').fadeIn(400);
                                            holder.removeAttr('style')
                                            holder.find('.sending .progress div').width('5%')
                                            holder.find('.sending span b').text('5')
                                        });
                                    }, 4000);

                                    holder.find('.sent').fadeIn(400, function () {
                                        setTimeout(function () {
                                            if (typeof goog_report_conversion !== 'undefined' && $.isFunction(goog_report_conversion)) {
                                                goog_report_conversion(window.location.href);
                                            }
                                        }, 2000);

                                    });
                                });

                                clearInterval(prog_to);
                            }, 5000);
                            var curr_pos = parseInt(holder.find('.sending span b').text());
                            var prog_to = setInterval(function () {

                                curr_pos = parseInt(curr_pos + 5);
                                holder.find('.sending .progress div').width(curr_pos + '%')
                                holder.find('.sending span b').text(curr_pos)
                                if (curr_pos == 100) {
                                    //clicklearInterval(prog_to);
                                    clearInterval(prog_to);
                                }
                            }, 250);
                        });
                    }
                    else {

                        var box = $(this).parents('.holder');
                        box.removeAttr('style').height(box.height());
                        box.find('.form').fadeOut(400, function () {
                            box.find('.loading').fadeIn(400);
                            setTimeout(function () {
                                box.find('.loading').fadeOut(400, function () {

                                    setTimeout(function () {
                                        box.find('.thanks').fadeOut(400, function () {
                                            box.find('.form').fadeIn(400);
                                            box.find('.holder').removeAttr('style')
                                        });
                                    }, 3000);

                                    box.find('.thanks').fadeIn(400);
                                });
                            }, 2000);
                        });
                    }
                    // console.log('revolver 1 before submitForm()');
                    submitForm($form);
                    resetForm($form);
                }
            }
        }

        return false;
        // }//all elements checked and form valid
    });


    $('#btnWish').click(function () {
        $.get($(this).attr('href'), function (data) {
            $('#wishlistContent').html(data);
        });
        return false;
    });

    $("#allfeatures").hide();

    $("#see_less_btn").click(function () {
        $("#allfeatures").hide();
        $("#features").show();
    });

    $("#see_all_btn").click(function () {
        $("#features").hide();
        $("#allfeatures").show();
    });

    $("#list_your_property_btn").click(function () {
        $('html, body').animate({
            scrollTop: $("#content").offset().top
        }, 2000);
    });
});

$(document).ready(function () {

    $('#menuTopL > a').click(function () {
        var $t = $(this);
        var $p = $(this).parent();

        var win_w = $(window).width();

        if (win_w > 960) {
            return true;
        }
        if ($p.find('.dropdown.open').length) {
            $p.find('.dropdown').removeClass('open');
        } else {
            $p.find('.dropdown').addClass('open');
        }
        return false;
    });

    $('.property_list_ad').prev('div.item').css("border-bottom", "1px solid transparent");

    $('.calculator').click(function () {

        var amount = $(this).attr('data-amount');
        $('#calculatorMaxPrice').val(amount);
        var maxAmount = 50000000;

        if (amount > maxAmount) {
            maxAmount = 200000000;
        }

        if (amount) {

            valueRange.noUiSlider.updateOptions({
                start: amount,
                connect: 'lower',
                step: 1,
                range: {
                    'min': 0,
                    'max': maxAmount
                }
            });
        }
        onChangeAmountTextCalculator();
    });

    $('#valueRangeText').keyup(function () {
        var x = $(this).val();
        $(this).val(x.toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ","));

    });

    $('#valueRangeText').change(function () {
        onChangeAmountTextCalculator();
    });

    $('.currency_container a').click(function () {

        var currencyCode = $(this).attr('data-code');
        if (currencyCode) {
            createCookie('currencySotheby', currencyCode, 365);
            location.reload();
        }
    });
    $('.addWish').click(function () {
        var $t = $(this);
        var propertyId = $(this).attr('data-id');

        if ($t.hasClass('act')) {
            $('.addedWishlist span').text('REMOVED FROM WISHLIST');
            $('.wishlist_property_' + propertyId).removeClass('act');
            removeWishList(propertyId);
        } else {
            $('.addedWishlist span').text('ADDED TO WISHLIST');
            var existIds = getCookie('wishListSotheby');

            if (existIds) {
                propertyId = propertyId + ',' + existIds;
            }
            //alert(propertyId);

            document.cookie = "wishListSotheby=" + propertyId + ";path=/";

            $('.wishlist_property_' + propertyId).addClass('act');
        }


        return false;
    });


});

$(document).on('click', '.remove_wishlist', function () {

//$('.remove_wishlist').on( "click", function (e) {
    var propertyId = $(this).attr('data-id');
    removeWishList(propertyId);

    return false;

});


$(document).ready(function () {

    $(".icon_bookmark").click(function () {
        // Mozilla Firefox Bookmark
        if ('sidebar' in window && 'addPanel' in window.sidebar) {
            window.sidebar.addPanel(location.href, document.title, "");
        } else if (/*@cc_on!@*/false) { // IE Favorite
            window.external.AddFavorite(location.href, document.title);
        } else { // webkit - safari/chrome
            alert('Press ' + (navigator.userAgent.toLowerCase().indexOf('mac') != -1 ? 'Command/Cmd' : 'CTRL') + ' + D to bookmark this page.');
        }
    });
});

function removeURLParameter(url, parameter) {
    //prefer to use l.search if you have a location/link object
    var urlparts = url.split('?');
    if (urlparts.length >= 2) {

        var prefix = encodeURIComponent(parameter) + '=';
        var pars = urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i = pars.length; i-- > 0;) {
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        url = urlparts[0] + '?' + pars.join('&');
        return url;
    } else {
        return url;
    }
}

function onChangeAmountTextCalculator() {
    var currentValue = $('#valueRangeText').val();
    currentValue = currentValue.replace(/,/g, "");
    currentValue = parseInt(currentValue);
    //console.log(currentValue);

    valueRange.noUiSlider.updateOptions({
        start: currentValue,
        connect: 'lower',
        step: 1,
        range: {
            'min': 0,
            'max': parseInt($('#calculatorMaxPrice').val()),
        }
    });
    calculateEMI();
}

function calculateEMI() {
    var propertyValue = $('#valueRangeText').val();
    propertyValue = propertyValue.replace(/,/g, "");
    propertyValue = parseInt(propertyValue);


    var loadPeriod = $('#periodRangeText').text();
    loadPeriod = loadPeriod.replace(/,/g, "");
    loadPeriod = parseInt(loadPeriod);

    var downPayment = $('#downRangeText').text();
    downPayment = downPayment.replace(/,/g, "");
    downPayment = parseInt(downPayment);

    var interestRate = $('#interestRangeText').text();
    interestRate = interestRate.replace(/,/g, "");
    interestRate = parseFloat(interestRate);


    var loanAmount = propertyValue - downPayment;

    //loanAmount = loanAmount + ()

    var landDepartmentFee = parseInt((propertyValue * .04) + 540);

    var registrationFee = parseInt((propertyValue < 500000) ? 2000 : 4000);

    var mortgageRegistration = parseInt((loanAmount * 0.0025) + 10);

    var brokerCommison = parseInt(propertyValue * .02);


    var totalPurchseCost = landDepartmentFee + registrationFee + mortgageRegistration + brokerCommison;

    var loanAmountTotal = parseInt(loanAmount + (totalPurchseCost * 0.75));

    var totalRequiredUpfront = parseInt(downPayment + (totalPurchseCost * 0.25));


    var PR = loanAmountTotal;

    var IN = (interestRate / 100) / 12;

    var PE = loadPeriod * 12;

    var payment = (PR * IN) / (1 - Math.pow(1 + IN, -PE));


    var principalPerMonth = parseInt(PR / (loadPeriod * 12));

    //var interestPerMonth = parseInt((PR * IN));

    var totalPaymentPerMonth = round_decimals(payment, 2);

    totalPaymentPerMonth = parseInt(totalPaymentPerMonth);

    var interestPerMonth = parseInt(totalPaymentPerMonth - principalPerMonth);


    //alert(payment);
    $('#emi_total_amount_pay').html(numberWithCommas(principalPerMonth) + ' AED <br><b>Principal</b>');
    $('#emi_total_intrest_pay').html(numberWithCommas(interestPerMonth) + ' AED <br><b>Interest</b>');
    $('#emi_total_pay').text(numberWithCommas(totalPaymentPerMonth) + ' AED / M');
    $('#emi_loan_amount').text(numberWithCommas(loanAmountTotal));
    $('#emi_land_department_fee').text(numberWithCommas(landDepartmentFee));
    $('#emi_registration_fee').text(numberWithCommas(registrationFee));
    $('#emi_mortgage_registration').text(numberWithCommas(mortgageRegistration));
    $('#emi_broker_commision').text(numberWithCommas(brokerCommison));
    //$('#emi_valutation').text(numberWithCommas(valuation));
    $('#emi_purchace').text(numberWithCommas(totalPurchseCost));
    $('#emi_downpayment').text(numberWithCommas(downPayment));
    $('#emi_required_upfront').text(numberWithCommas(totalRequiredUpfront));

    var differentPecent = (principalPerMonth / totalPaymentPerMonth) * 100

    // differentPecent = differentPecent + interestPerMonth * 100;

    differentPecent = parseInt(differentPecent);

    var rightside = 100 - differentPecent;

    $('.toPay').css('width', differentPecent + '%');
    $('.remPay').css('width', rightside + '%');

}

function round_decimals(original_number, decimals) {
    var result1 = original_number * Math.pow(10, decimals)
    var result2 = Math.round(result1)
    var result3 = result2 / Math.pow(10, decimals)
    return (result3);
}

function removeWishList(propertyId) {

    var existIds = getCookie('wishListSotheby');

    var wishList = existIds;

    wishList = wishList.replace(propertyId + ",", "");
    wishList = wishList.replace("," + propertyId, "");
    wishList = wishList.replace(propertyId, "");


    document.cookie = "wishListSotheby=" + wishList + ";path=/";

    $('.wishlist_property_' + propertyId).removeClass('act');
    $('.wishlist_item_' + propertyId).slideUp("normal", function () {
        $(this).remove();
    });
    if (wishList == '') {

        $('#btnWish').parent('li').removeClass('act');
    }
}

function changePropertyType(typename) {
//alert('hi');
    $('.search_property_type .dd li').each(function () {
        var t = $(this);
        if (window.location.href.indexOf("properties-for-rent-in-dubai") > -1 || window.location.href.indexOf("is_rental=1") > -1) {
            typename = 'rent';
        } else if (window.location.href.indexOf("properties-for-sale-in-dubai") > -1 || window.location.href.indexOf("is_rental=0") > -1) {
            typename = 'buy';
        }
        var catCount = t.find('a').attr('data-' + typename + '-count');
        t.find('span').html(' (' + catCount + ')');
        t.show();
        if (catCount == 0) {
            t.hide();
        }
    });

}




//slider.js
/* development Page Gallery  */

$(document).ready(function () {
    $('#gallery-big').royalSlider({
        fullscreen: {
            enabled: false,
            nativeFS: false
        },
        arrowsNav: false,
        arrowsNavAutoHide: false,
        fadeinLoadedSlide: false,
        controlNavigationSpacing: 0,
        controlNavigation: 'bullets',
        loop: true,
        imageScaleMode: 'fill',
        numImagesToPreload: 2,
        keyboardNavEnabled: true,
        globalCaption: false
    });

});

/* development Page Gallery  */


/* Popup Emaar */
jQuery(document).ready(function ($) {
    //open popup
    $('.cd-popup-trigger').on('click', function (event) {
        event.preventDefault();
        $('.cd-popup').addClass('is-visible');
    });

    //close popup
    $('.cd-popup').on('click', function (event) {
        if ($(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup')) {
            event.preventDefault();
            $(this).removeClass('is-visible');
        }
    });
    //close popup when clicking the esc keyboard button
    $(document).keyup(function (event) {
        if (event.which == '27') {
            $('.cd-popup').removeClass('is-visible');
        }
    });
});
/* Popup Emaar */

/* Open */

/*$(function () {
 var parent = $("#mslider .carousel-inner");
 var divs = parent.children();
 while (divs.length) {
 parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
 }
 $("#mslider .carousel-inner div:first-child").addClass("active");
 });*/

$('.mlink').on('click', function (e) {

    if ($(this).parent('li').hasClass('actMenu')) {
        window.location.href = $(this).attr('href');
    } else {
        e.preventDefault();
        $(".mlink").parent('li').removeClass('actMenu');
        $(this).parent('li').addClass('actMenu');
        $(".overlay1").addClass('menuHeight');
        $('.mainOverlay').css({visibility: 'visible', opacity: 1});

    }
    $('.cls').click(function () {
        $(".overlay1").removeClass('menuHeight');
        $('.mainOverlay').css({visibility: 'hidden', opacity: 0});
        $('.actMenu').removeClass('actMenu');
    });
});

(function ($) {
    // console.log(navigator.userAgent);
    /* Adjustments for Safari on Mac */
    if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Mac') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
        // console.log('Safari on Mac detected, applying class...');
        $('html').addClass('searchSafari'); // provide a class for the safari-mac specific css to filter with
    }
})(jQuery);

$('.selectionLink a.searchType').on('click', function () {
//ehsan: shifted to search.js
    $('a.activesearchbtn').removeClass('activesearchbtn');
    $(this).addClass('activesearchbtn');
    if ($('.activesearchbtn').attr('id') == 'sale') {
        $('input[name=is_rental]').val(0);
    } else {
        $('input[name=is_rental]').val(1);
    }

});
$(document).ready(function () {
    //ehsan: deleted val=0 set as default in input is_rental
    if ($('.activesearchbtn').attr('id') == 'sale') {
        $('input[name=is_rental]').val(0);
    } else {
        $('input[name=is_rental]').val(1);
    }

    $(window).resize(function () {
        var h = $(window).height();
        $(".get_height").css('max-height', h);
    });
});

$(window).scroll(function () {
    var scroll = $(window).scrollTop();
    //console.log(scroll);
    if (scroll >= 50) {
        //console.log('a');
        $(".replica #header").addClass("change");
        $("#menuTopL ul.dropdown").addClass("change");
        $(".breadcrumb").addClass("graybread");
    } else {
        //console.log('a');
        $("#header").removeClass("change");
        $("#menuTopL ul.dropdown").removeClass("change");
        $(".breadcrumb").removeClass("graybread");
    }
});

function scrolldown() {
    var abx = $('#content').offset().top;
    abx = abx - 70;
    $('html,body').animate({
            scrollTop: abx
        },
        'slow');
};

$(document).ready(function () {
    var cards = $("devpics");
    for (var i = 0; i < cards.length; i++) {
        var target = Math.floor(Math.random() * cards.length - 1) + 1;
        var target2 = Math.floor(Math.random() * cards.length - 1) + 1;
        cards.eq(target).before(cards.eq(target2));
    }
});
