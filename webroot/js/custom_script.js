$(document).ready(function () {

	$('.currency_container a').click(function(){
		var currencyCode = $(this).attr('data-code');
		if(currencyCode){
			document.cookie = "currencyLetfloat="+currencyCode;
			location.reload();
		}
	});

	if($('.datepicket').length){
		$('.datepicket').each(function(){
			$(this).datepicker({dateFormat: "yy-mm-dd"});
		});
	}

	$('.checkbox_none').change(function(){
		var $t = $(this).parent().parent().parent();
		if($t.find('.checkbox_none:checked').length){
			$t.find('.select .chks').each(function(){
				$(this).find('.chk').prop('checked', false);
				$(this).find('label').removeClass('act');
			}); 
		}
	});

	$('.addWish').click(function(){
		var $t = $(this);
		var boatId = $(this).attr('data-id');

		

		if($t.hasClass('act')){
			$('.addedWishlist span').text('REMOVED FROM WISHLIST');
			$('.wishlist_property_'+boatId).removeClass('act');
			removeWishList(boatId);

			if($t.hasClass('btnAddWishlist')){
				$(this).find('span').text('Remove from wishlist')
				$('.addedWishlist').fadeIn(400, function(){
					setTimeout(function() {
						$('.addedWishlist').fadeOut(400)
					}, 1000);
				});
			}
		}else{
			$('.addedWishlist span').text('ADDED TO WISHLIST');
			var existIds = getCookie('wishListLetfloat');

    		if(existIds){
    			boatId = boatId+','+existIds;
    		}
    		//alert(boatId);
    		
			document.cookie = "wishListLetfloat="+boatId+";path=/";

			$('.wishlist_property_'+boatId).addClass('act');

			if($t.hasClass('btnAddWishlist')){
				$(this).find('span').text('Add to wishlist');
			}
		}


		

		return false;
	});

	

	$('.custom_select').each(function(){

		var $this = $(this);

		var valueText = $this.find("option:selected").text();

		if(!valueText){
			var valueText = $this.find("option:first").text();
		}

		

		$this.hide();

		var optionsValue = '';

		$this.find('option').each(function(){
			optionsValue = optionsValue + '<li><a href="#" data-value="'+$(this).val()+'">'+$(this).text()+'</a></li>';
		});

		optionsValue = '<div class="dd"><ul>'+optionsValue+'</ul></div>';

		$this.wrap("<div class='inpt slct slct_dd custom_select_slct_dd'></div>");
		$this.parent().append('<b>'+valueText+'</b>');
		$this.parent().append(optionsValue);

		$this.parent().find('.dd').wrapInner( "<div class='scroll'></div>" );

		$this.parent().find('.scroll').mCustomScrollbar({
			theme:"dark-thick"
		});
	});

	$('.custom_select_slct_dd').click(function(){
		//e.preventDefault();
		
		$('.slct_dd.act').not(this).removeClass('act');
		$(this).toggleClass('act');
	});

	$('.custom_select_slct_dd .dd li a').click(function(){
		var $p = $(this).parents('.custom_select_slct_dd');
		var valueTag = $(this).attr('data-value');
		var textTag = $(this).text();
		$p.find('b').text(textTag);
		$p.find('.custom_select').val(valueTag);

		$p.removeClass('act');

		return false;
	});

	if ( $("#priceRange").length > 0 ) {
		
		var priceRange = document.getElementById('priceRange');

		noUiSlider.create(priceRange, {
			start: 3000,
			connect: 'lower',
			step: 100,
			range: {
			  'min': 0,
			  'max': 10000
			}
		});

		var priceRangeText = document.getElementById('priceRangeText');

		priceRange.noUiSlider.on('update', function( values, handle ){
			$('#priceRange').parent().find('input').val(parseInt(values[handle]));
			priceRangeText.innerHTML = numberWithCommas( parseInt(values[handle]) ) + ' <span dir="rtl">د.إ</span> Per Hr';
		});

	}


	if ( $("#lengthRange").length > 0 ) {

		var lengthRange = document.getElementById('lengthRange');

		noUiSlider.create(lengthRange, {
			start: 150,
			connect: 'lower',
			step: 1,
			range: {
			  'min': 5,
			  'max': 400
			}
		});

		var lengthRangeText = document.getElementById('lengthRangeText');

		lengthRange.noUiSlider.on('update', function( values, handle ){
			$('#lengthRange').parent().find('input').val(parseInt(values[handle]));
			lengthRangeText.innerHTML = numberWithCommas( parseInt(values[handle]) ) + ' Ft.';
		});

	}

	$('.submitFilter').click(function(){

		$('#filterSidebar .chk:checked').each(function(){
			var $t = $(this);
			var $p = $(this).parents('.custom_checkbox');
			var $existValue = $p.find("input[type='hidden']").val();
			if($existValue){
				$existValue += ',';
			}
			$existValue += $t.val();
			$t.removeAttr('name');
			$p.find("input[type='hidden']").val($existValue);

		});
		//return false;

		$('#filterSidebar').submit();

		return false;
	});

	$('.showMore').click(function(){
		$(this).parent().find('.filter_select_content').toggleClass('show_all');
	});

	$('#btnWish').click(function(){
    	$.get( $(this).attr('href'), function( data ) {
		  	$('#wishlistContent').html(data);
		});
    	return false;
    });

	



});

function removeWishList(boatId){
	
	var existIds = getCookie('wishListLetfloat');

	var wishList = existIds;

	wishList = wishList.replace(boatId+",", "");
	wishList = wishList.replace(","+boatId, "");
	wishList = wishList.replace(boatId, "");


	document.cookie = "wishListLetfloat="+wishList+";path=/";
	
	$('.wishlist_property_'+boatId).removeClass('act');
	$('.wishlist_item_'+boatId).slideUp("normal", function() { $(this).remove(); } );
	if(wishList == ''){
		
		$('#btnWish').parent('li').removeClass('act');
	}
}

function getCookie(cname) {
    var name = cname + "=";
    console.log(document.cookie);
    var ca = document.cookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length,c.length);
        }
    }
    return "";
}


/*===================================================================================*/
/*	GOOGLE MAPS
/*===================================================================================*/

$(document).ready(function () {
	
	function initialize() {
		var mapOptions = {
			zoom: 13,
			center: new google.maps.LatLng(40.7902778, -73.9597222),
			disableDefaultUI: true
		}
		var map = new google.maps.Map(document.getElementById('map'), mapOptions);
	}
	
	//google.maps.event.addDomListener(window, 'load', initialize);
	
});


