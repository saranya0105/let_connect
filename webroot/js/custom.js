/*--------------------------------------list-grid-map view toggle------------------------------------*/
$(document).ready(function(){

	$('.switch_view').click(function(){
		var $view = $(this).attr('data-view');
		$('.main_content_area').attr('class','main_content_area '+$view+'_view');
		return false;
	});
});
/*-----------------------------------------nav bar hover effect-------------------------------------*/
$(document).ready(function () {
    $("#property_li").hover(
  function () {
     $('ul.property_ul').finish().slideDown('medium');
  }, 
  function () {
     $('ul.property_ul').finish().slideUp('medium');
  }
);
    $("#about_li").hover(
  function () {
     $('ul.about_ul').finish().slideDown('medium');
  }, 
  function () {
     $('ul.about_ul').finish().slideUp('medium');
  }
);

});
/*---------------------------------------------mob view-----------------------------------------------*/
$(document).ready(function(){
    $('#more-listing').click(function(){
        $('.hide_toggle').slideToggle('slow');

         if ($(this).text() == "view all our listing") { 
        $(this).text("view less listing"); 
	    } else { 
	        $(this).text("view all our listing"); 
	    };
    });
});
$(document).ready(function(){
    $('#mob_drop_toggle_button').click(function(){
        $('#mob_drop_menu_slide').slideToggle('slow');
    });
});
$(document).ready(function(){
    $('#property_btn').click(function(){
        $('#property_drop').slideToggle('slow');
    });
});
$(document).ready(function(){
    $('#about_btn').click(function(){
        $('#about_drop').slideToggle('slow');
    });
});

$(document).ready(function(){

$('#mob_filter_toggle:eq(0)').click(function(){
  $('#sidebar').toggle();
  $('.mob_list_view').toggle();
  $('#more-listing').toggle();    
  $('#toolbar').toggle(); 
});

});

/*---------------------------about page--------------------------*/

$(document).ready(function(){

$('.r1:eq(0)').click(function(){
  $('.e1').slideToggle('slow');
  $('.f1').slideToggle('slow');

  if ($(this).text() == "Read More") { 
        $(this).text("Read Less"); 
      } else { 
          $(this).text("Read More"); 
      };
});
$('.r2:eq(0)').click(function(){
  $('.e2').slideToggle('slow');
  $('.f2').slideToggle('slow');

  if ($(this).text() == "Read More") { 
        $(this).text("Read Less"); 
      } else { 
          $(this).text("Read More"); 
      };
});
$('.r3:eq(0)').click(function(){
  $('.e3').slideToggle('slow');
  $('.f3').slideToggle('slow');

  if ($(this).text() == "Read More") { 
        $(this).text("Read Less"); 
      } else { 
          $(this).text("Read More"); 
      };
});
$('.r4:eq(0)').click(function(){
  $('.e4').slideToggle('slow');
  $('.f4').slideToggle('slow');

  if ($(this).text() == "Read More") { 
        $(this).text("Read Less"); 
      } else { 
          $(this).text("Read More"); 
      };
});
$('.r5:eq(0)').click(function(){
  $('.e5').slideToggle('slow');
  $('.f5').slideToggle('slow');

  if ($(this).text() == "Read More") { 
        $(this).text("Read Less"); 
      } else { 
          $(this).text("Read More"); 
      };
});
});
/*-----------------------project page---------------------*/

/*---------------------end project page-------------------*/
/*-----------------------gallery page---------------------*/
$(document).ready(function(){
    $('.open-dis').click(function(){
        $('.overlay').slideToggle('slow');
        
            if ($(this).text() == "OPEN DESCRIPTION") { 
            $(this).text("CLOSE DESCRIPTION"); 
          } else { 
              $(this).text("OPEN DESCRIPTION"); 
          };
        });
        $('.open-dis').click(function() {
            $(this).toggleClass('active');
            return false;
    });
});
/*-----------------------gallery page---------------------*/