	<?php
return 
[
 	//'checkboxWrapper' => '<div class="checkbox">{{label}}</div>',
  	'checkbox' => '<input class="chk" type="checkbox" name="{{name}}" value="{{value}}"{{attrs}}>',
  	'nestingLabel' => '<div class="chks">{{hidden}}{{input}}<label{{attrs}}><span></span>{{text}}</label></div>',
  	'radio' => '<input class="rdo" type="radio" name="{{name}}" value="{{value}}"{{attrs}}>',
    'radioWrapper' => '{{label}}',
]
?>