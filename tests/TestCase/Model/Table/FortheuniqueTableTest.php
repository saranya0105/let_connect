<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FortheuniqueTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FortheuniqueTable Test Case
 */
class FortheuniqueTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FortheuniqueTable
     */
    public $Fortheunique;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fortheunique'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Fortheunique') ? [] : ['className' => FortheuniqueTable::class];
        $this->Fortheunique = TableRegistry::get('Fortheunique', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Fortheunique);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
