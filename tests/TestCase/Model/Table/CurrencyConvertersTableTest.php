<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CurrencyConvertersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CurrencyConvertersTable Test Case
 */
class CurrencyConvertersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CurrencyConvertersTable
     */
    public $CurrencyConverters;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.currency_converters'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CurrencyConverters') ? [] : ['className' => CurrencyConvertersTable::class];
        $this->CurrencyConverters = TableRegistry::get('CurrencyConverters', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CurrencyConverters);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
