<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ListyourpropertiesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ListyourpropertiesTable Test Case
 */
class ListyourpropertiesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ListyourpropertiesTable
     */
    public $Listyourproperties;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.listyourproperties'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Listyourproperties') ? [] : ['className' => ListyourpropertiesTable::class];
        $this->Listyourproperties = TableRegistry::get('Listyourproperties', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Listyourproperties);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
