<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EmiratesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EmiratesTable Test Case
 */
class EmiratesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\EmiratesTable
     */
    public $Emirates;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.emirates',
        'app.locations',
        'app.properties'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Emirates') ? [] : ['className' => EmiratesTable::class];
        $this->Emirates = TableRegistry::get('Emirates', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Emirates);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
