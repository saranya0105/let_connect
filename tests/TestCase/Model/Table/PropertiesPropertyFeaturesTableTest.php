<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PropertiesPropertyFeaturesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PropertiesPropertyFeaturesTable Test Case
 */
class PropertiesPropertyFeaturesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PropertiesPropertyFeaturesTable
     */
    public $PropertiesPropertyFeatures;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.properties_property_features',
        'app.properties',
        'app.categories',
        'app.emirates',
        'app.locations',
        'app.sub_locations',
        'app.agents',
        'app.photos',
        'app.property_features'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PropertiesPropertyFeatures') ? [] : ['className' => PropertiesPropertyFeaturesTable::class];
        $this->PropertiesPropertyFeatures = TableRegistry::get('PropertiesPropertyFeatures', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PropertiesPropertyFeatures);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
