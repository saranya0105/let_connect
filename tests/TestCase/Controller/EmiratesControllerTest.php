<?php
namespace App\Test\TestCase\Controller;

use App\Controller\EmiratesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\EmiratesController Test Case
 */
class EmiratesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.emirates',
        'app.locations',
        'app.properties',
        'app.categories',
        'app.sub_locations',
        'app.agents',
        'app.photos',
        'app.property_features',
        'app.properties_property_features'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
