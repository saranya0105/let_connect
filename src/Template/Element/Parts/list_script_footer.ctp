
<?php echo $this->start( 'script' ); ?>
<script type="text/javascript">
	$(document).ready(function () {

		function pinSymbol(color) {
		  return {
		    path: 'M1014.9,2522.1h-53.5c-7.6,0-13.7-6.3-13.7-13.7v-2.1c0-7.6,6.3-13.7,13.7-13.7h53.5 c7.6,0,13.7,6.3,13.7,13.7v2.1C1028.7,2515.8,1022.5,2522.1,1014.9,2522.1z',
		    fillColor: '#CCCCCC',
		    fillOpacity: 0.4,
		    strokeColor: '#CCCCCC',
		    strokeWeight: 2,
		    scale: 2
		  };
		}

		function setMarkersLocation(map,location){
			var marker;

			latlngset = new google.maps.LatLng(location.latitude, location.longitude);

			var marker = new google.maps.Marker({  
				map: map, 
				title: location.name,
				position: latlngset ,
			});
			


			var content = location.name;

			var infowindow = new google.maps.InfoWindow()

			google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){ 
				return function() {
					infowindow.setContent(content);
					infowindow.open(map,marker);
				};
			})(marker,content,infowindow));  

		}

		function setMarkers(map,location){
			var marker;



			var heading = location.price + '  '+ location.sub_location + ' ' + location.location + ' ' + location.Emirate

			latlngset = new google.maps.LatLng(location.latitude, location.longitude);

			// var marker = new google.maps.Marker({  
			// 	map: map, title: heading , 
			// 	position: latlngset ,
			// 	//icon: 'https://maps.google.com/mapfiles/kml/shapes/parking_lot_maps.png', 
			// 	labelClass: 'itemMarker',
			// 	label:'20k'
			// });
			var marker = new MarkerWithLabel({
			    position: latlngset,
			    map: map,
			    draggable: false,
			    raiseOnDrag: true,
			    labelContent: location.price_show,
			    //labelAnchor: new google.maps.Point('-35', '-25'),
			    labelClass: "labels_map itemMarker", // the CSS class for the label
			    labelInBackground: false,
			   	 //icon: pinSymbol('red')
			   	icon: '<?php //echo $this->html->url('/img/map_marker_transparent.png',true); ?>', 
			  });
			var content = '<div class="itemInfo clear"> <div class="img"><a target="_blank" href="'+location.property_link+'"><img alt="google map location image" src="'+location.image+'"></a></div> <div class="txt"> <p class="price">'+location.price+'</p> <h3><a target="_blank" href="'+location.property_link+'">'+location.sub_location+', <span>'+location.location+'</span>, '+location.Emirate+'.</a></h3> <div class="tbl"> <div class="cell"><b>Approx size:<br>Ref No.:</b></div> <div class="cell">'+location.bua+' Sq.ft<br>'+location.reference_no+'</div> </div> </div> </div>';

			var infowindow = new google.maps.InfoWindow()

			google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){ 
				return function()	 {
					infowindow.setContent(content);
					infowindow.open(map,marker);
				};
			})(marker,content,infowindow));  

		}
		
		var map;
		$('.icon_map').click(function(){

			//$('.listings #middle .loading').fadeIn(400);
			

			var myOptions = {
		        center: new google.maps.LatLng(25.001188, 55.679419),
		        zoom: 10,
		        mapTypeId: google.maps.MapTypeId.ROADMAP

		    };
		    
		    map = new google.maps.Map(document.getElementById("googleMap"), myOptions);

		    //setMarkers(map,locations);
		    $.get( '<?php echo $this->Html->url('/properties/get_location_gmap/',true) ?>', function( data ) {
					for (var i = 0, len = data.length; i < len; i++) {
						setMarkersLocation(map,data[i]);
					//console.log(data[i].longitude);
					}
				}, "json" );

		    map.addListener('idle', function() {
		    	//$('.listings #middle .loading').fadeOut(400)
				//$('.listings .mapView').fadeIn(600);
				
				
				window.setTimeout(function() {
					var bounds = map.getBounds();
					var zoomPoint = map.getZoom();
					console.log(zoomPoint);
					if(zoomPoint > 11){
						$.get( '<?php echo $this->Html->url('/properties/properties_map/',true) ?>'+bounds.b.b+'/'+bounds.b.f+'/'+bounds.f.f+'/'+bounds.f.b+'/', function( data ) {
							for (var i = 0, len = data.length; i < len; i++) {
								setMarkers(map,data[i]);
							//console.log(data[i].longitude);
							}
						}, "json" );
					}
					
				}, 0);
		        //console.log(map.getBounds());
		    });
		});

	});
</script>
<?php echo $this->end(); ?>