<footer>
			<div class="footer-section wow fadeInUp">
				<div class="row">
					<div class="footer_content_mob footer_content_tab col-lg-2 col-md-2 col-sm-2 col-xs-12">
						<h5>ABOUT LIV</h5>
						<p>We approach the design, development and management of each Omniyat property as if it were a unique work of art. Just as there is no mistaking a Picasso or a Van Gogh, there is no mistaking that you are anywhere but in an Omniyat property. We nurture <a  href="<?= $this->Url->build("/pages/about/",true); ?>" class="read_more">Read More</a></p>
					</div>
					<div class="footer_content_mob col-lg-2 col-md-2 col-sm-2 col-xs-12">
						<h5>ASSET MANAGEMENT</h5>
						<p>Lorem ipsum dolor sit amet, et porro nulla deserunt sea, eu habemus tibique pro, rationibus vituperatoribus vis ut. sea ex exerci praesent interpretaris. Id usu possit audire reformidans. Eu eam vidit porro oratio,</p>
					</div>
					<div class="footer_content_mob col-lg-2 col-md-2 col-sm-2 col-xs-12">
						<h5>OUR DEVELOPMENTS</h5>
						<div class="location_para">
							<ul class="list-unstyled">
								<li>Palm Jumeirah</li>
								<li>Jumeirah Golf Estate</li>
								<li>Dubai Marina</li>
								<li>Arabian Ranches</li>
								<li>Al Barari</li>
							</ul>
						</div>	
					</div>
					<div class="footer_content_mob col-lg-2 col-md-2 col-sm-2 col-xs-12">
						<h5>UPCOMING PROJECTS</h5>
						<div class="project-list">
							<ul class="list-unstyled">
								<li>Lorem ipsum possim</li>
								<li>Lorem ipsum possim</li>
								<li>Lorem ipsum possim</li>
								<li>Lorem ipsum possim</li>
								<li>Lorem ipsum possim</li>
							</ul>
						</div>
					</div>
					<div class="footer_content_mob footer_content_tab col-lg-2 col-md-2 col-sm-2 col-xs-12">
						<h5>CONTACT US</h5>
						<p id="contact-details">Brand Real Estate LLC</p>
						<p>Building Marina Promenade street
							Dubai Marina Area 17196<br>
							Tel.No: +971 4 223 333<br>
							Fax.No: +971 4 223 333<br>
							Email: info@brands.com</p>
					</div>
				</div>
			</div>

			<div class="footer-end wow fadeInUp">
				<div class="social-icons">
					<a><i class="fa fa-facebook  fa-lg" ></i></a>
					<a><i class="fa fa-instagram  fa-lg" ></i></a>
					<a><i class="fa fa-twitter  fa-lg" ></i></a>
					<a><i class="fa fa-linkedin  fa-lg" ></i></a>
					<a><i class="fa fa-youtube-play  fa-lg" ></i></a>
					<a><i class="fa fa-google-plus  fa-lg" ></i></a>
					<a><i class="fa fa-pinterest-p  fa-lg" ></i></a>
				</div>

				<div class="footer-final">
					<p>
						<a href="#">COMPANY</a><span>|</span>
						<a href="#">ADVERTISING</a><span>|</span>
						<a href="#">CARERRS</a><span>|</span>
						<a href="#">TERMS OF USAGE</a><span>|</span>
						<a href="#">HELP</a><span>|</span>
						<a href="#">PRIVACY POLICY</a>
					</p>    
				</div>
				<div class="footer-final-content container">
					<p style="margin-bottom: 0px;">Lorem ipsum dolor sit amet, et porro nulla deserunt sea, eu habemus tibique pro, rationibus vituperatoribus vis ut. sea ex exerci praesent interpretaris. Id usu possit audire reformidans. Eu eam vidit porro oratio, possim omnesque volutpat id Lorem ipsum dolor sit amet, et porro nulla deserunt sea, eu habemus tibique pro, rationibus vituperatoribus vis ut. sea ex exerci praesent interpretaris. Id usu possit audire reformidans. Eu eam vidit porro oratio, possim omnesque volutpat id</p>
				</div>
				<div class="footer-final-para container">
					<p style="margin-bottom: 0px;">Magna reque tritani nec id. Duo velit accumsan efficiendi ne, diam sale conceptam et mea. Cu mei scripta maluisset, eos at dicam partem torquatos.</p>
				</div>
				<div class="footer-copy-rights">
					<p>&#169; 2017 Company Name, All right reserved</p>
				</div>
			</div>
		</footer>