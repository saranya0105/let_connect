<div id="sidebar-main" class="col-md-3 col-lg-3 col-sm-4 col-xs-12 wow fadeInLeft">
							<div id="sidebar" class="filterBy">
							    <div class="ins">
							         <form action="<?= $this->Url->build('/properties/',true) ?>" id="filterSidebar">
							            <h2>FILTER PROPERTY</h2>
							            <label>Property-Type</label>
							            <div class="filter_select_content">
                    						<?= $this->Omniyat->input('category_id', ['options' => $categories,'empty'=>'Select option','class'=>'inpt custom_select']); ?>
                						</div>
							            <div>
							            	<?php
							            		$priceListMax = $this->Omniyat->getPriceList(true,'max');
  												$priceListMin = $this->Omniyat->getPriceList(true,'min')
							            	?>
							                <label>Price</label>
							                <div class="clear">
							                	<?= $this->Omniyat->input('price_min', ['options' => $priceListMin,'empty'=>'Min','class'=>'inpt custom_select']); ?>
							                   	<?= $this->Omniyat->input('price_max', ['options' => $priceListMax,'empty'=>'Max','class'=>'inpt custom_select']); ?>
							                   
							                </div>
							            </div>
							            <label>Rooms</label>
							            <div class="inpt slct slct_dd marg_b2">
							                <b class="default">Bedrooms</b>
							                <?= $this->Omniyat->input('bedroom',['type'=>'hidden']); ?>
							                <input class="filter_selected" name="beds" value="" type="hidden">
							                <div class="dd">
							                    <ul>
							                        <li><a class="" href="#" data-id="1">1</a></li>
							                        <li><a class="" href="#" data-id="2">2</a></li>
							                        <li><a class="" href="#" data-id="3">3</a></li>
							                        <li><a class="" href="#" data-id="4">4</a></li>
							                        <li><a class="" href="#" data-id="5">5</a></li>
							                        <li><a class="" href="#" data-id="6">6</a></li>
							                        <li><a class="" href="#" data-id="7">7</a></li>
							                        <li><a class="" href="#" data-id="8">8</a></li>
							                        <li><a class="" href="#" data-id="9">9</a></li>
							                        <li><a class="" href="#" data-id="10">10+</a></li>
							                    </ul>
							                </div>
							            </div>
							            <div class="inpt slct slct_dd">
							                <b class="default">Bathrooms</b>
							                <?= $this->Omniyat->input('bathroom',['type'=>'hidden']); ?>
							                <input class="filter_selected" name="baths" value="" type="hidden">
							                <div class="dd">
							                    <ul>
							                        <li><a class="" href="#" data-id="1">1</a></li>
							                        <li><a class="" href="#" data-id="2">2</a></li>
							                        <li><a class="" href="#" data-id="3">3</a></li>
							                        <li><a class="" href="#" data-id="4">4</a></li>
							                        <li><a class="" href="#" data-id="5">5</a></li>
							                        <li><a class="" href="#" data-id="6">6</a></li>
							                        <li><a class="" href="#" data-id="7">7</a></li>
							                        <li><a class="" href="#" data-id="8">8</a></li>
							                        <li><a class="" href="#" data-id="9">9</a></li>
							                        <li><a class="" href="#" data-id="10">10+</a></li>
							                    </ul>
							                </div>
							            </div>
							            <label>Parking</label>
							            <div class="inpt slct slct_dd">
							                <b class="default">Car parking</b>
							                <?= $this->Omniyat->input('carparking',['type'=>'hidden']); ?>
							                <input class="filter_selected" name="parking" value="" type="hidden">
							                <div class="dd">
							                    <ul>
							                        <li><a class="" href="#" data-id="1">1</a></li>
							                        <li><a class="" href="#" data-id="2">2</a></li>
							                        <li><a class="" href="#" data-id="3">3</a></li>
							                        <li><a class="" href="#" data-id="4">4</a></li>
							                        <li><a class="" href="#" data-id="5">5</a></li>
							                        <li><a class="" href="#" data-id="6">6</a></li>
							                        <li><a class="" href="#" data-id="7">7</a></li>
							                        <li><a class="" href="#" data-id="8">8</a></li>
							                        <li><a class="" href="#" data-id="9">9</a></li>
							                        <li><a class="" href="#" data-id="10">10+</a></li>
							                    </ul>
							                </div>
							            </div>
							            
							            <?php if($featurecount != 0) { ?>
							            <div class="custom_checkbox group">
							                <label>Fixture &amp; Fittings</label>
							                <fieldset>
							                    <div class="chks">
							                    <?= $this->Omniyat->input('feature_id', ['options' => $propertyFeatures,'multiple'=>'checkbox']); ?>
							                     </div>
							                    <a href="#" class="showMore">View more</a>
							                </fieldset>
							            </div>
							            <?php } ?>
							            <div class="btns clear">
							                <a href="#" title="Cancel your filter choices" class="btn btnLight" id="Filter_Cancel">Cancel</a>
							                <a href="#" title="Filter your property listings" class="btn btnBlue submitFilter" id="properyFilter">Filter</a>
							            </div>
							        </form>
							    </div>
							</div>
						</div>