<div class="inpt slct slct_dd custom_click_event phone_prefix">
	<b>United Arab Emirates <span class="code">+971</span></b>
	<input type="hidden" name="ph_ext" value="+971">
	
	<div class="dd"  data-height="200">
		<ul>
			<li><a href="#">Afghanistan <span class="code">+93</span></a></li>
			<li><a href="#">Albania <span class="code">+355</span></a></li>
			<li><a href="#">Algeria <span class="code">+213</span></a></li>
			<li><a href="#">American Samoa <span class="code">+1-684</span></a></li>
			<li><a href="#">Andorra <span class="code">+376</span></a></li>
			<li><a href="#">Angola <span class="code">+244</span></a></li>
			<li><a href="#">Anguilla <span class="code">+1-264</span></a></li>
			<li><a href="#">Antarctica <span class="code">+672</span></a></li>
			<li><a href="#">Antigua and Barbuda <span class="code">+1-268</span></a></li>
			<li><a href="#">Argentina <span class="code">+54</span></a></li>
			<li><a href="#">Armenia <span class="code">+374</span></a></li>
			<li><a href="#">Aruba <span class="code">+297</span></a></li>
			<li><a href="#">Australia <span class="code">+61</span></a></li>
			<li><a href="#">Austria <span class="code">+43</span></a></li>
			<li><a href="#">Azerbaijan <span class="code">+994</span></a></li>
			<li><a href="#">Bahamas <span class="code">+1-242</span></a></li>
			<li><a href="#">Bahrain <span class="code">+973</span></a></li>
			<li><a href="#">Bangladesh <span class="code">+880</span></a></li>
			<li><a href="#">Barbados <span class="code">+1-246</span></a></li>
			<li><a href="#">Belarus <span class="code">+375</span></a></li>
			<li><a href="#">Belgium <span class="code">+32</span></a></li>
			<li><a href="#">Belize <span class="code">+501</span></a></li>
			<li><a href="#">Benin <span class="code">+229</span></a></li>
			<li><a href="#">Bermuda <span class="code">+1-441</span></a></li>
			<li><a href="#">Bhutan <span class="code">+975</span></a></li>
			<li><a href="#">Bolivia <span class="code">+591</span></a></li>
			<li><a href="#">Bosnia and Herzegovina<span class="code">+387</span></a></li>
			<li><a href="#">Botswana <span class="code">+267</span></a></li>
			<li><a href="#">Brazil <span class="code">+55</span></a></li>
			<li><a href="#">British Indian Ocean Territory<span class="code">+246</span></a></li>
			<li><a href="#">British Virgin Islands <span class="code">+1-284</span></a></li>
			<li><a href="#">Brunei <span class="code">+673</span></a></li>
			<li><a href="#">Bulgaria <span class="code">+359</span></a></li>
			<li><a href="#">Burkina Faso <span class="code">+226</span></a></li>
			<li><a href="#">Myanmar <span class="code">+95</span></a></li>
			<li><a href="#">Burundi <span class="code">+257</span></a></li>
			<li><a href="#">Cambodia <span class="code">+855</span></a></li>
			<li><a href="#">Cameroon <span class="code">+237</span></a></li>
			<li><a href="#">Canada <span class="code">+1</span></a></li>
			<li><a href="#">Cape Verde <span class="code">+238</span></a></li>
			<li><a href="#">Cayman Islands <span class="code">+1-345</span></a></li>
			<li><a href="#">Central African Republic <span class="code">+236</span></a></li>
			<li><a href="#">Chad <span class="code">+235</span></a></li>
			<li><a href="#">Chile <span class="code">+56</span></a></li>
			<li><a href="#">China <span class="code">+86</span></a></li>
			<li><a href="#">Christmas Island <span class="code">+61</span></a></li>
			<li><a href="#">Cocos Islands <span class="code">+61</span></a></li>
			<li><a href="#">Colombia <span class="code">+57</span></a></li>
			<li><a href="#">Comoros <span class="code">+269</span></a></li>
			<li><a href="#">Republic of the Congo <span class="code">+242</span></a></li>
			<li><a href="#">Democratic Republic of the Congo <span class="code">+243</span></a></li>
			<li><a href="#">Cook Islands <span class="code">+682</span></a></li>
			<li><a href="#">Costa Rica <span class="code">+506</span></a></li>
			<li><a href="#">Croatia <span class="code">+385</span></a></li>
			<li><a href="#">Cuba <span class="code">+53</span></a></li>
			<li><a href="#">Curacao <span class="code">+599</span></a></li>
			<li><a href="#">Cyprus <span class="code">+357</span></a></li>
			<li><a href="#">Czech Republic <span class="code">+420</span></a></li>
			<li><a href="#">Denmark <span class="code">+45</span></a></li>
			<li><a href="#">Djibouti <span class="code">+253</span></a></li>
			<li><a href="#">Dominica <span class="code">+1-767</span></a></li>
			<li><a href="#">Dominican Republic <span class="code">+1-809, 1-829, 1-849</span></a></li>
			<li><a href="#">East Timor <span class="code">+670</span></a></li>
			<li><a href="#">Ecuador <span class="code">+593</span></a></li>
			<li><a href="#">Egypt <span class="code">+20</span></a></li>
			<li><a href="#">El Salvador <span class="code">+503</span></a></li>
			<li><a href="#">Equatorial Guinea <span class="code">+240</span></a></li>
			<li><a href="#">Eritrea <span class="code">+291</span></a></li>
			<li><a href="#">Estonia <span class="code">+372</span></a></li>
			<li><a href="#">Ethiopia <span class="code">+251</span></a></li>
			<li><a href="#">Falkland Islands <span class="code">+500</span></a></li>
			<li><a href="#">Faroe Islands <span class="code">+298</span></a></li>
			<li><a href="#">Fiji <span class="code">+679</span></a></li>
			<li><a href="#">Finland <span class="code">+358</span></a></li>
			<li><a href="#">France <span class="code">+33</span></a></li>
			<li><a href="#">French Polynesia <span class="code">+689</span></a></li>
			<li><a href="#">Gabon <span class="code">+241</span></a></li>
			<li><a href="#">Gambia <span class="code">+220</span></a></li>
			<li><a href="#">Georgia <span class="code">+995</span></a></li>
			<li><a href="#">Germany <span class="code">+49</span></a></li>
			<li><a href="#">Ghana <span class="code">+233</span></a></li>
			<li><a href="#">Gibraltar <span class="code">+350</span></a></li>
			<li><a href="#">Greece <span class="code">+30</span></a></li>
			<li><a href="#">Greenland <span class="code">+299</span></a></li>
			<li><a href="#">Grenada <span class="code">+1-473</span></a></li>
			<li><a href="#">Guam <span class="code">+1-671</span></a></li>
			<li><a href="#">Guatemala <span class="code">+502</span></a></li>
			<li><a href="#">Guernsey <span class="code">+44-1481</span></a></li>
			<li><a href="#">Guinea <span class="code">+224</span></a></li>
			<li><a href="#">Guinea-Bissau <span class="code">+245</span></a></li>
			<li><a href="#">Guyana <span class="code">+592</span></a></li>
			<li><a href="#">Haiti <span class="code">+509</span></a></li>
			<li><a href="#">Honduras <span class="code">+504</span></a></li>
			<li><a href="#">Hong Kong <span class="code">+852</span></a></li>
			<li><a href="#">Hungary <span class="code">+36</span></a></li>
			<li><a href="#">Iceland <span class="code">+354</span></a></li>
			<li><a href="#">India <span class="code">+91</span></a></li>
			<li><a href="#">Indonesia <span class="code">+62</span></a></li>
			<li><a href="#">Iran <span class="code">+98</span></a></li>
			<li><a href="#">Iraq <span class="code">+964</span></a></li>
			<li><a href="#">Ireland <span class="code">+353</span></a></li>
			<li><a href="#">Isle of Man <span class="code">+44-1624</span></a></li>
			<li><a href="#">Israel <span class="code">+972</span></a></li>
			<li><a href="#">Italy <span class="code">+39</span></a></li>
			<li><a href="#">Ivory Coast <span class="code">+225</span></a></li>
			<li><a href="#">Jamaica <span class="code">+1-876</span></a></li>
			<li><a href="#">Japan <span class="code">+81</span></a></li>
			<li><a href="#">Jersey <span class="code">+44-1534</span></a></li>
			<li><a href="#">Jordan <span class="code">+962</span></a></li>
			<li><a href="#">Kazakhstan <span class="code">+7</span></a></li>
			<li><a href="#">Kenya <span class="code">+254</span></a></li>
			<li><a href="#">Kiribati <span class="code">+686</span></a></li>
			<li><a href="#">Kosovo <span class="code">+383</span></a></li>
			<li><a href="#">Kuwait <span class="code">+965</span></a></li>
			<li><a href="#">Kyrgyzstan <span class="code">+996</span></a></li>
			<li><a href="#">Laos <span class="code">+856</span></a></li>
			<li><a href="#">Latvia <span class="code">+371</span></a></li>
			<li><a href="#">Lebanon <span class="code">+961</span></a></li>
			<li><a href="#">Lesotho <span class="code">+266</span></a></li>
			<li><a href="#">Liberia <span class="code">+231</span></a></li>
			<li><a href="#">Libya <span class="code">+218</span></a></li>
			<li><a href="#">Liechtenstein <span class="code">+423</span></a></li>
			<li><a href="#">Lithuania <span class="code">+370</span></a></li>
			<li><a href="#">Luxembourg <span class="code">+352</span></a></li>
			<li><a href="#">Macau <span class="code">+853</span></a></li>
			<li><a href="#">Macedonia <span class="code">+389</span></a></li>
			<li><a href="#">Madagascar <span class="code">+261</span></a></li>
			<li><a href="#">Malawi <span class="code">+265</span></a></li>
			<li><a href="#">Malaysia <span class="code">+60</span></a></li>
			<li><a href="#">Maldives <span class="code">+960</span></a></li>
			<li><a href="#">Mali <span class="code">+223</span></a></li>
			<li><a href="#">Malta <span class="code">+356</span></a></li>
			<li><a href="#">Marshall Islands <span class="code">+692</span></a></li>
			<li><a href="#">Mauritania <span class="code">+222</span></a></li>
			<li><a href="#">Mauritius <span class="code">+230</span></a></li>
			<li><a href="#">Mayotte <span class="code">+262</span></a></li>
			<li><a href="#">Mexico <span class="code">+52</span></a></li>
			<li><a href="#">Micronesia <span class="code">+691</span></a></li>
			<li><a href="#">Moldova <span class="code">+373</span></a></li>
			<li><a href="#">Monaco <span class="code">+377</span></a></li>
			<li><a href="#">Mongolia <span class="code">+976</span></a></li>
			<li><a href="#">Montenegro <span class="code">+382</span></a></li>
			<li><a href="#">Montserrat <span class="code">+1-664</span></a></li>
			<li><a href="#">Morocco <span class="code">+212</span></a></li>
			<li><a href="#">Mozambique <span class="code">+258</span></a></li>
			<li><a href="#">Namibia <span class="code">+264</span></a></li>
			<li><a href="#">Nauru <span class="code">+674</span></a></li>
			<li><a href="#">Nepal <span class="code">+977</span></a></li>
			<li><a href="#">Netherlands <span class="code">+31</span></a></li>
			<li><a href="#">Netherlands Antilles <span class="code">+599</span></a></li>
			<li><a href="#">New Caledonia <span class="code">+687</span></a></li>
			<li><a href="#">New Zealand <span class="code">+64</span></a></li>
			<li><a href="#">Nicaragua <span class="code">+505</span></a></li>
			<li><a href="#">Niger <span class="code">+227</span></a></li>
			<li><a href="#">Nigeria <span class="code">+234</span></a></li>
			<li><a href="#">Niue <span class="code">+683</span></a></li>
			<li><a href="#">Northern Mariana Islands <span class="code">+1-670</span></a></li>
			<li><a href="#">North Korea <span class="code">+850</span></a></li>
			<li><a href="#">Norway <span class="code">+47</span></a></li>
			<li><a href="#">Oman <span class="code">+968</span></a></li>
			<li><a href="#">Pakistan <span class="code">+92</span></a></li>
			<li><a href="#">Palau <span class="code">+680</span></a></li>
			<li><a href="#">Palestine <span class="code">+970</span></a></li>
			<li><a href="#">Panama <span class="code">+507</span></a></li>
			<li><a href="#">Papua New Guinea <span class="code">+675</span></a></li>
			<li><a href="#">Paraguay <span class="code">+595</span></a></li>
			<li><a href="#">Peru <span class="code">+51</span></a></li>
			<li><a href="#">Philippines <span class="code">+63</span></a></li>
			<li><a href="#">Pitcairn <span class="code">+64</span></a></li>
			<li><a href="#">Poland <span class="code">+48</span></a></li>
			<li><a href="#">Portugal <span class="code">+351</span></a></li>
			<li><a href="#">Puerto Rico <span class="code">+1-787, 1-939</span></a></li>
			<li><a href="#">Qatar <span class="code">+974</span></a></li>
			<li><a href="#">Reunion <span class="code">+262</span></a></li>
			<li><a href="#">Romania <span class="code">+40</span></a></li>
			<li><a href="#">Russia <span class="code">+7</span></a></li>
			<li><a href="#">Rwanda <span class="code">+250</span></a></li>
			<li><a href="#">Saint Barthelemy <span class="code">+590</span></a></li>
			<li><a href="#">Samoa <span class="code">+685</span></a></li>
			<li><a href="#">San Marino <span class="code">+378</span></a></li>
			<li><a href="#">Sao Tome and Principe <span class="code">+239</span></a></li>
			<li><a href="#">Saudi Arabia <span class="code">+966</span></a></li>
			<li><a href="#">Senegal <span class="code">+221</span></a></li>
			<li><a href="#">Serbia <span class="code">+381</span></a></li>
			<li><a href="#">Seychelles <span class="code">+248</span></a></li>
			<li><a href="#">Sierra Leone <span class="code">+232</span></a></li>
			<li><a href="#">Singapore <span class="code">+65</span></a></li>
			<li><a href="#">Sint Maarten <span class="code">+1-721</span></a></li>
			<li><a href="#">Slovakia <span class="code">+421</span></a></li>
			<li><a href="#">Slovenia <span class="code">+386</span></a></li>
			<li><a href="#">Solomon Islands <span class="code">+677</span></a></li>
			<li><a href="#">Somalia <span class="code">+252</span></a></li>
			<li><a href="#">South Africa <span class="code">+27</span></a></li>
			<li><a href="#">South Korea <span class="code">+82</span></a></li>
			<li><a href="#">South Sudan <span class="code">+211</span></a></li>
			<li><a href="#">Spain <span class="code">+34</span></a></li>
			<li><a href="#">Sri Lanka <span class="code">+94</span></a></li>
			<li><a href="#">Saint Helena <span class="code">+290</span></a></li>
			<li><a href="#">Saint Kitts and Nevis <span class="code">+1-869</span></a></li>
			<li><a href="#">Saint Lucia <span class="code">+1-758</span></a></li>
			<li><a href="#">Saint Martin <span class="code">+590</span></a></li>
			<li><a href="#">Saint Pierre and Miquelon <span class="code">+508</span></a></li>
			<li><a href="#">Saint Vincent and the Grenadines <span class="code">+1-784</span></a></li>
			<li><a href="#">Sudan <span class="code">+249</span></a></li>
			<li><a href="#">Suriname <span class="code">+597</span></a></li>
			<li><a href="#">Svalbard and Jan Mayen <span class="code">+47</span></a></li>
			<li><a href="#">Swaziland <span class="code">+268</span></a></li>
			<li><a href="#">Sweden <span class="code">+46</span></a></li>
			<li><a href="#">Switzerland <span class="code">+41</span></a></li>
			<li><a href="#">Syria <span class="code">+963</span></a></li>
			<li><a href="#">Taiwan <span class="code">+886</span></a></li>
			<li><a href="#">Tajikistan <span class="code">+992</span></a></li>
			<li><a href="#">Tanzania <span class="code">+255</span></a></li>
			<li><a href="#">Thailand <span class="code">+66</span></a></li>
			<li><a href="#">Togo <span class="code">+228</span></a></li>
			<li><a href="#">Tokelau <span class="code">+690</span></a></li>
			<li><a href="#">Tonga <span class="code">+676</span></a></li>
			<li><a href="#">Trinidad and Tobago <span class="code">+1-868</span></a></li>
			<li><a href="#">Tunisia <span class="code">+216</span></a></li>
			<li><a href="#">Turkey <span class="code">+90</span></a></li>
			<li><a href="#">Turkmenistan <span class="code">+993</span></a></li>
			<li><a href="#">Turks and Caicos Islands <span class="code">+1-649</span></a></li>
			<li><a href="#">Tuvalu <span class="code">+688</span></a></li>
			<li><a href="#">United Arab Emirates <span class="code">+971</span></a></li>
			<li><a href="#">Uganda <span class="code">+256</span></a></li>
			<li><a href="#">United Kingdom <span class="code">+44</span></a></li>
			<li><a href="#">Ukraine <span class="code">+380</span></a></li>
			<li><a href="#">Uruguay <span class="code">+598</span></a></li>
			<li><a href="#">United States <span class="code">+1</span></a></li>
			<li><a href="#">Uzbekistan <span class="code">+998</span></a></li>
			<li><a href="#">Vanuatu <span class="code">+678</span></a></li>
			<li><a href="#">Vatican <span class="code">+379</span></a></li>
			<li><a href="#">Venezuela <span class="code">+58</span></a></li>
			<li><a href="#">Vietnam <span class="code">+84</span></a></li>
			<li><a href="#">U.S. Virgin Islands <span class="code">+1-340</span></a></li>
			<li><a href="#">Wallis and Futuna <span class="code">+681</span></a></li>
			<li><a href="#">Western Sahara <span class="code">+212</span></a></li>
			<li><a href="#">Yemen <span class="code">+967</span></a></li>
			<li><a href="#">Zambia <span class="code">+260</span></a></li>
			<li><a href="#">Zimbabwe <span class="code">+263</span></a></li>
			<li><a href="#">Other</a></li>
		</ul>
	</div>
	
</div>