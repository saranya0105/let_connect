<nav class="navbar navbar-top">
			<div class="navbar-logo col-lg-3 col-sm-3">
				<a class="navbar-brand" href="<?= $this->Url->build("/",true); ?>"><img class="img-responsive" src="<?= $this->Url->build("/img/Omniyat-Exc-by-LIV-EN-T.png",true); ?>"></a>
			</div>
			<div class="mob_drop_menu">
			<button id="mob_drop_toggle_button" class="btn dropdown-toggle mob_drop_toggle_btn" data-toggle="dropdown"><i class="fa fa-bars" aria-hidden="true"></i></button>
				<div id="mob_drop_menu_slide" class="mob_drop_menu1 dropdown-menu">
				 	<ul>
						<li class="nav-menu-mob">
							<a><button id="property_btn" class="dropbtn_mob">Property</button></a>
							<div id="property_drop" class="inner-dropdown-menu">
								<ul>
							    	<li><a href="<?= $this->Url->build("/properties/index/Rent",true); ?>" onclick="myFunction();">Property for rent</a></li>
							    	<li><a href="<?= $this->Url->build("/properties/index/Sale",true); ?>">Property for sale</a></li>
						    	</ul>
							</div>
						</li>
						<li class="nav-menu-mob">
							<a   href="<?= $this->Url->build("/pages/project/",true); ?>">Project</a>
						</li>
						<li class="nav-menu-mob">
							<a  href="<?= $this->Url->build("/pages/gallery/",true); ?>">Gallery</a>
						</li>
						<li class="nav-menu-mob">
							<a  href="http://careers.omniyat.com">Careers</a>
						</li>
						<li class="nav-menu-mob">
							<a><button id="about_btn" class="dropbtn_mob">About</button></a>
							<div id="about_drop" class="inner-dropdown-menu">
								<ul>
						    	<li><a href="<?= $this->Url->build("/pages/about/",true); ?>">About Us</a></li>
						    	<li><a href="<?= $this->Url->build("/pages/about/#team",true); ?>">The Team</a></li>
						    	<li><a href="<?= $this->Url->build("/pages/about/#omniyart",true); ?>">OMNIYAT</a></li>
						    	</ul>
							</div>
						</li>
						<li class="nav-menu-mob">
							<a  href="<?= $this->Url->build("/pages/contactus/",true); ?>">Contact Us</a>
						</li>
					</ul>
				</div>
			</div>
			<div id="navbar" class="navbar-menu col-lg-6 col-lg-offset-3">
				<ul class="nav navbar-nav mr-auto">
					<li id="property_li" class="nav-item">
						<div class="dropdown">
							<a href="<?= $this->Url->build("/properties/index/",true); ?>"><button class="dropbtn">Property</button></a>
							<div class="dropdown-content">
								<ul class="property_ul">
									<div class="list_drop container">
								    	<li><a href="<?= $this->Url->build("/properties/index/Rent",true); ?>" onclick="myFunction();">Property for rent</a></li>
								    	<li><a href="<?= $this->Url->build("/properties/index/Sale",true); ?>">Property for sale</a></li>
									</div>
								</ul>
							</div>
						</div>
					</li>
					<li class="nav-item">
						<a class="nav-menu"  href="<?= $this->Url->build("/pages/project/",true); ?>">Project</a>
					</li>
					<li class="nav-item">
						<a class="nav-menu" href="<?= $this->Url->build("/pages/gallery/",true); ?>">Gallery</a>
					</li>
					<li class="nav-item">
						<a class="nav-menu" href="http://careers.omniyat.com">Careers</a>
					</li>
					<li id="about_li" class="nav-item">
						<div class="dropdown">
							<button class="dropbtn">About</button>
							<div class="dropdown-content">
								<ul class="about_ul">
									<div class="list_drop container">
						    	<li><a href="<?= $this->Url->build("/pages/about/",true); ?>">About Us</a></li>
						    	<li><a href="<?= $this->Url->build("/pages/about/#team",true); ?>">The Team</a></li>
						    	<li><a href="<?= $this->Url->build("/pages/about/#omniyart",true); ?>">OMNIYAT</a></li>
						    		</div>
						    	</ul>
							</div>
						</div>
					</li>
					<li class="nav-item">
						<a class="nav-menu" href="<?= $this->Url->build("/pages/contactus/",true); ?>">Contact Us</a>
					</li>
				</ul>
			</div>
		</nav>