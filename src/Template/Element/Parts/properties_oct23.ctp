<?php $colSize = isset($colSize) ? $colSize : 4; ?>
	<div class="col-lg-<?= $colSize ?> wow zoomIn">
		<div class="product_item">

			<div class="product_item_image">
			<img src="<?= $this->Url->build("/photos/".$property->location_id."/".$property->unit_no."/1.JPG",true); ?> ">
			</div>
	
					<div class="product_item_content">
							<div class="txt">
								<p>
								<?php 
								    if($property->price == 0) { 
									    $price  = "Add price on application" ; 
								    }else {
									    $price = number_format($property->price).' <strong style="font-size: 20px;"></strong>';
									} 
								?>	

									<?= $price; ?><br> <?= $property->location->name; ?>, <?= $property->emirate->name; ?>
								</p>
								<p class="hide_list"><?= $property->beds; ?> Bed<span>/</span><?= $property->baths; ?> Bath<span>/</span><?= $property->parking; ?> Park</p>
								<div class="tbl">
									<div class="cell">
										<b>Type:</b> <?= $property->category->name; ?>
										<br>
										<b>Size:</b> <?= $property->bua; ?> Sq. Ft.
										<br>
										<b>Ref No.:</b> <?= $property->reference_no; ?>
									</div>
								</div>
								<div class="show_list product_item_desc">
									<?= $property->description; ?> 
								</div>
							</div>
							<div class="main-content-btns">
							<a href="<?= $this->Url->build("/properties/view/$property->id"); ?>"><button  class="btn btn-default content_view_btn" >view details</button></a>
								<button class="btn btn-default content_contact_btn">contact us</button>
							</div>
					</div>
		</div>
	</div>	