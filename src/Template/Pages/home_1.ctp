<div id="carousel_home" class="carousel slide wow fadeIn" data-ride="carousel">
			<ol class="carousel-indicators">
			    <li data-target="#carousel_home" data-slide-to="0" class="active"></li>
			    <li data-target="#carousel_home" data-slide-to="1"></li>
			    <li data-target="#carousel_home" data-slide-to="2"></li>
			    <li data-target="#carousel_home" data-slide-to="3"></li>
		    </ol>
		 <!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<img src="<?= $this->Url->build("/img/One by Omniyat - 1.jpg",true); ?>" alt="...">
				</div>
				<div class="item">
					<img src="<?= $this->Url->build("/img/Bayswater-2.jpg",true); ?>" alt="...">
				</div>
				<div class="item">
					<img src="<?= $this->Url->build("/img/The Binary-3.jpg",true); ?>" alt="...">
				</div>
				<div class="item">
					<img src="<?= $this->Url->build("/img/The Square-4.jpg",true); ?>" alt="...">
				</div>
			</div>
			<div class="carousel-caption" style="z-index:0;">
				<div class="main-content">
					<p id="heading-main">LUXURY PROPERTY FOR RENT</p>
					<p id="heading-sub">Luxury residential specialists</p>
					<button id="explore" type="button" class="btn btn-default btn-lg">Discover more</button>
				</div>
			</div>
			<a class="left carousel-control" href="#carousel_home" data-slide="prev">
		    	<span class="glyphicon glyphicon-chevron-left"></span>
		    	<span class="sr-only">Previous</span>
		    </a>
		    <a class="right carousel-control" href="#carousel_home" data-slide="next">
		    	<span class="glyphicon glyphicon-chevron-right"></span>
		    	<span class="sr-only">Next</span>
		    </a>
		</div>

	<div  id="search-wrap" >
		        <div class="search_container">
						<div class="inpt slct slct_dd banner_dropdown">
						    <b class="default">Property Type</b>
						    <input class="filter_selected" name="category_id" value="" type="hidden">
						    <div class="dd">
						        <ul>
						        	<li><a data-id="" href="#">Residential for rent</a></li>
						            <li><a class="" href="#" data-id="1">Residential for Buy</a></li>
						            <li><a class="" href="#" data-id="2">Commercial for rent</a></li>
						            <li><a class="" href="#" data-id="3">Commercial for Buy</a></li>						            
						        </ul>
						    </div>
						</div>
						<div class="inpt slct slct_dd banner_dropdown">
						    <b class="default">Search by Project</b>
						    <input class="filter_selected" name="category_id" value="" type="hidden">
						    <div class="dd">
						        <ul>
						        	
						            <li><a class="" href="#" data-id="1">ONE PALM</a></li>
						            <li><a class="" href="#" data-id="2">Langham Place</a></li>
						            <li><a class="" href="#" data-id="3">THE STERLING</a></li>
						            <li><a class="" href="#" data-id="4">ANWA</a></li>
						            <li><a class="" href="#" data-id="5">THE OPUS</a></li>
						            <li><a class="" href="#" data-id="6">THE PAD</a></li>
						            <li><a class="" href="#" data-id="7">The Binary</a></li>
						            <li><a class="" href="#" data-id="8">BAYSWATER</a></li>
						            <li><a class="" href="#" data-id="9">ONE by Omniyat</a></li>
						            <li><a class="" href="#" data-id="10">THE SQUARE</a></li>						            
						        </ul>
						    </div>
						</div>

					<a id="search-btn" href="listing.html" class="btn btn-default" type="search" style="color: rgb(255, 255, 255);">Search</a>
					</div>
				</div>
	<div class="container-second">
		
		<p>explore our featured properties</p>
	</div>	
	<div class="row" id="featured-property">
		<div class="col-lg-4 col-md-4 col-sm-12 wow fadeInLeft">
			<div class="featured_image_wrapper"><img src="<?= $this->Url->build("/img/featured_one.png",true); ?>"></div>
			<div class="featured_inner_text">langham place<br>downtown dubai<br>residence & the<br>hotel</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-12 wow fadeInUp">
			<div class="featured_image_wrapper"><img src="<?= $this->Url->build("/img/featured_second.png",true); ?>"></div>
			<div class="featured_inner_text">the sterling <br>a paradise for the<br>exceptional</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-12 wow fadeInRight">
			<div class="featured_image_wrapper"><img src="<?= $this->Url->build("/img/featured_third.png",true); ?>"></div>
			<div class="featured_inner_text">one palm<br>palm jumeirah,<br>dubai</div>
		</div>						
	</div>
	<div class="container-third">
		<p>view our latest hot listings</p>
	</div>	
	<section>
		<div id="hot-listing" class="container-fluid">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-12 hot_listing_wrapper wow fadeIn" >
					<div class="hot_listing_inner">
						<img src="<?= $this->Url->build("/img/hot_listing_one.png",true); ?>">
						<div class="hot_link_inner">THE GARDENS, PALM JUMEIRAH<br>AED 17,500,000</div>
						<div class="btn_wrap">
							<button class="btn btn-primary">for sale</button>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 hot_listing_wrapper wow fadeIn">
					<div class="hot_listing_inner">
						<img src="<?= $this->Url->build("/img/hot_listing_two.png",true); ?>">
						<div class="hot_link_inner">THE GARDENS, PALM JUMEIRAH<br>AED 17,500,000</div>
						<div class="btn_wrap">
							<button class="btn btn-primary">for sale</button>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 hot_listing_wrapper wow fadeIn">
					<div class="hot_listing_inner">
						<img src="<?= $this->Url->build("/img/hot_listing_three.png",true); ?>">
						<div class="hot_link_inner">THE GARDENS, PALM JUMEIRAH<br>AED 17,500,000</div>
						<div class="btn_wrap">
							<button class="btn btn-primary">for sale</button>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 hot_listing_wrapper wow fadeIn">
					<div class="hot_listing_inner">
						<img src="<?= $this->Url->build("/img/hot_listing_four.png",true); ?>">
						<div class="hot_link_inner">THE GARDENS, PALM JUMEIRAH<br>AED 17,500,000</div>
						<div class="btn_wrap">
							<button class="btn btn-primary">for sale</button>
						</div>
					</div>
				</div>
			</div>									
		</div>
	</section>
	<div class="container-fourth">
		<a href="<?= $this->Url->build("/properties/index/",true); ?>" target="_blank"><button id="more-listing" class="btn btn-primary" type="search" style="color: rgb(255, 255, 255);">view all our listing</button></a>
	</div>
	<section class="subscribing">
		<div class="row margin-none">
			<div id="subcrib_txt_one" class="col-lg-6 col-md-6 col-sm-12 ">
				<div class="local-knowledge-head">LOCAL KNOWLEDGE</div>
				<div class="local-knowledge">Our team are all certified by the Real Estate<br>Regulatory Agency giving you peace if mind</div>
			</div>
			<div id="subcrib_img_one" class="col-lg-6 col-md-6 col-sm-12 wow slideInRight">
				<img src="<?= $this->Url->build("/img/subcribe_one.png",true); ?>">
			</div>
		</div>	
		<div class="row margin-none">
			<div id="subcrib_img_two" class="col-lg-6 col-md-6 col-sm-12 wow slideInLeft">
				<img src="<?= $this->Url->build("/img/subcribe_two.png",true); ?>">
				<div class="subcribe_img_txt">Get the latest news and happenings<br>on luxury real estate directly in your inbox</div>
			</div>
			<div id="subcrib_submition" class="col-lg-6 col-md-6 col-sm-12 ">
				<div class="email_input_area">
					<input class="btn btn-lg" name="email" id="email" type="email" placeholder="Enter your email" required>
				</div>
				<div class="checkbox_main">
					<input id="c1" class="chk" type="checkbox">
					<label for="c1">
						<span></span>
						I am a Broker/Agent
					</label>
				</div>
				<div id="subcrib_btn" ><button id="subcrib_btn_inner" class="btn btn-info btn-lg" type="submit">Submit</button></div>
			</div>
			<div class="promise">By subscribing, you are agreeing to our Privacy Policy. You can unsubscribe anytime<br>from our emails by clicking the "unsubscribe" link.</div>
		</div>
	</div>
	</div>
	</section>	