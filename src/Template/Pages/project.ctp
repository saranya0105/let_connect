<div class="path_bar wow flipInX">
		<div class="row">
			<div class="col-lg-11 col-lg-offset-1">
				
					<div> <a href="<?= $this->Url->build("/properties/index/",true); ?>"> Home </a> > Project </div>
				
			</div>
		</div>
	</div>
	<div class="list-projects">
		<a href="http://www.omniyat.com/project/thebinary/" data-name="The Binary" class="project-item" id="binary">
			<img alt="The Binary" src="http://www.omniyat.com/wp-content/uploads/2015/08/the_binary.jpg" width="2800" height="600">
		</a>
		<a href="http://www.omniyat.com/project/onebyomniyat/" data-name="ONE by Omniyat" class="project-item" id="onebyomniyat">
			<img alt="ONE by Omniyat" src="http://www.omniyat.com/wp-content/uploads/2015/08/OBBBanner.jpg" width="2800" height="600">
		</a>
		<a href="http://www.omniyat.com/project/bayswater/" data-name="BAYSWATER" class="project-item" >
			<img alt="BAYSWATER" src="http://www.omniyat.com/wp-content/uploads/2015/08/the_bayswaterbanner.jpg" width="2800" height="600">
		</a>
		<a href="http://www.omniyat.com/project/thesquare/" data-name="THE SQUARE" class="project-item" id="square">
			<img alt="THE SQUARE" src="http://www.omniyat.com/wp-content/uploads/2015/08/the_squarebanner.jpg" width="2800" height="600">
		</a>
    </div>