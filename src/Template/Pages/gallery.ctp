<div class="path_bar">
				<div class="row">
					<div class="col-md-9 col-md-offset-3">
						
							<div> <a href="<?= $this->Url->build("/properties/index/",true); ?>"> Home </a> > Gallery </div>
						
					</div>
				</div>
			</div>
			

   <section class="section-gallery clearfix" style="clear:both">
            <div id="carousel_home" class="carousel slide wow fadeIn" data-ride="carousel">
             <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active square_slide">
                        <img src="http://www.omniyat.com/wp-content/uploads/2015/09/square-nursery.jpg" alt="...">
                        
                                <div style="display:none" class="overlay">
                                    <div class="text">
                                        <h2>The Square</h2>
                                        <p>A vibrant and colourful nursery offers children the utmost care alongside a warm and stimulating environment. </p>
                                    </div>
                                </div>
                    </div>
                    <div class="item square_slide">
                        <img src="http://www.omniyat.com/wp-content/uploads/2015/09/square-4.jpg" alt="...">
                        
                                <div style="display:none" class="overlay">
                                    <div class="text">
                                        <h2>The Square</h2>
                                        <p>A stunning outdoor patio features a communal barbecue area that provides residents and guests with the perfect place to relax and entertain.</p>
                                    </div>
                                </div>
                    </div>
                    <div class="item square_slide">
                        <img src="http://www.omniyat.com/wp-content/uploads/2015/09/041.jpg" alt="...">
                        
                                <div style="display:none" class="overlay">
                                    <div class="text">
                                        <h2>The Square</h2>
                                        <p>The Square is in strategic proximity to shopping centers, beaches and parks.</p>
                                    </div>
                                </div>
                    </div>
                    <div class="item square_slide">
                        <img src="http://www.omniyat.com/wp-content/uploads/2015/09/11.jpg" alt="...">
                        
                                <div style="display:none" class="overlay">
                                    <div class="text">
                                        <h2>The Square</h2>
                                        <p>A vibrant place providing homes to a beautiful community of 524 residential apartments.</p>
                                    </div>
                                </div>
                    </div>
                    <div class="item square_slide">
                        <img src="http://www.omniyat.com/wp-content/uploads/2015/09/09.jpg" alt="...">
                        
                                <div style="display:none" class="overlay">
                                    <div class="text">
                                        <h2>The Square</h2>
                                        <p>Amenities at the Square include a gorgeous swimming pool, a state-of-the-art gym, lush gardens, children’s play area and much more.</p>
                                    </div>
                                </div>
                    </div>
                    <div class="item square_slide">
                        <img src="http://www.omniyat.com/wp-content/uploads/2015/09/05.jpg" alt="...">
                        
                                <div style="display:none" class="overlay">
                                    <div class="text">
                                        <h2>The Square</h2>
                                        <p>Boasting a diverse variety of delicious restaurants, The Square offers cuisines to suit every taste.</p>
                                    </div>
                                </div>
                    </div>
                    <div class="item square_slide">
                        <img src="http://www.omniyat.com/wp-content/uploads/2015/09/02.jpg" alt="...">
                        
                                <div style="display:none" class="overlay">
                                    <div class="text">
                                        <h2>The Square</h2>
                                        <p>The Square is a stunning landmark by Omniyat and one of the premium developments in the area.</p>
                                    </div>
                                </div>
                    </div>
                    <div class="item one_by_omniyat_slide">
                        <img src="http://www.omniyat.com/wp-content/uploads/2016/01/Image1.jpg" alt="...">
                        
                        <div style="display:none" class="overlay">
                            <div class="text">
                                <h2>One by Omniyat</h2>
                                <p>One Business Bay is Omniyat’s signature commercial tower.</p>
                            </div>
                        </div>
                    </div>
                    <div class="item one_by_omniyat_slide">
                        <img src="http://www.omniyat.com/wp-content/uploads/2016/01/Image2.jpg" alt="...">
                        
                        <div style="display:none" class="overlay">
                            <div class="text">
                                <h2>One by Omniyat</h2>
                                <p>The tower’s distinctive form gives it an appearance of a skillfully cut diamond.</p>
                            </div>
                        </div>
                    </div>
                    <div class="item bayswater_slide">
                        <img src="http://www.omniyat.com/wp-content/uploads/2016/01/Image4.jpg" alt="...">
                        
                        <div style="display:none" class="overlay">
                            <div class="text">
                                <h2>Bayswater</h2>
                                <p>Bayswater is designed to give an impression of two towers inclined in opposite directions.</p>
                            </div>
                        </div>
                    </div>
                    <div class="item bayswater_slide">
                        <img src="http://www.omniyat.com/wp-content/uploads/2016/01/Image3.jpg" alt="...">
                        
                        <div style="display:none" class="overlay">
                            <div class="text">
                                <h2>Bayswater</h2>
                                <p>Work and enjoy simultaneously this beautiful view of the lake. </p>
                            </div>
                        </div>
                    </div>
                    <div class="item the_binary_slide">
                        <img src="http://www.omniyat.com/wp-content/uploads/2016/01/Image5.jpg" alt="...">
                        
                        <div style="display:none" class="overlay">
                            <div class="text">
                                <h2>The Binary</h2>
                                <p>Facing the lake view while getting your job done is all what you could ask for. </p>
                            </div>
                        </div>
                    </div>
                    <div class="item the_binary_slide">
                        <img src="http://www.omniyat.com/wp-content/uploads/2016/01/Entrance.jpg" alt="...">
                        
                        <div style="display:none" class="overlay">
                            <div class="text">
                                <h2>The Binary</h2>
                                <p>Our pyramid looking entrance is how high we aim to go. </p>
                            </div>
                        </div>
                    </div>
                    <div class="item the_binary_slide">
                        <img src="http://www.omniyat.com/wp-content/uploads/2016/01/Image6.jpg" alt="...">
                        
                        <div style="display:none" class="overlay">
                            <div class="text">
                                <h2>The Binary</h2>
                                <p>Top of the top. At The Binary, this is how we make you feel. </p>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="open-dis wow slideInRight">OPEN DESCRIPTION</a>
                <a class="left carousel-control" href="#carousel_home" data-slide="prev">
                    <span><i class="fa fa-angle-left" style="font-size:80px; top: 40%; position: absolute;"></i></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel_home" data-slide="next">
                    <span><i class="fa fa-angle-right" style="font-size:80px; top: 40%; position: absolute;"></i></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <div class="row filters wow fadeIn">
                <div class="container">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="gallery-title">GALLERY</div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="filter-wrapper">
                            <div class="filter-inner button-group filter-button-group">
                                <a class="btn filter_trigger active" data-target="all" id="no_filter" data-filter="*">All</a>
                                <a class="btn filter_trigger" data-target="square_slide" id="the_square" data-filter=".the-square">The Square</a>
                                <a class="btn filter_trigger" data-target="one_by_omniyat_slide" id="one_by_omniyat" data-filter=".one-by-omniyat">One by Omniyat</a>
                                <a class="btn filter_trigger" data-target="bayswater_slide" id="bayswater_only" data-filter=".bayswater">Bayswater</a>
                                <a class="btn filter_trigger" data-target="the_binary_slide" id="the_binary" data-filter=".the-binary">The Binary</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="thumbs grid">
                    <div class="img-wrapper wow rollIn the-square" data-id="main-square-nursery">
                        <div class="img-inner">
                            <div class="cap" style="display:none;">A vibrant and colourful nursery offers children the utmost care alongside a warm and stimulating environment. </div>
                            <a href="http://www.omniyat.com/wp-content/uploads/2015/09/square-nursery.jpg"><img src="http://www.omniyat.com/wp-content/uploads/2015/09/square-nursery-260x150.jpg" class="attachment-gallery-thumb size-gallery-thumb" alt="A vibrant and colourful nursery offers children the utmost care alongside a warm and stimulating environment." width="260" height="150"></a>
                            <div class="img-title">The Square</div>
                        </div>
                    </div>
                    <div class="img-wrapper wow rollIn the-square" data-id="main-square-4">
                        <div class="img-inner">
                            <div class="cap" style="display:none;">A stunning outdoor patio features a communal barbecue area that provides residents and guests with the perfect place to relax and entertain.</div>
                            <a href="http://www.omniyat.com/wp-content/uploads/2015/09/square-4.jpg"><img src="http://www.omniyat.com/wp-content/uploads/2015/09/square-4-260x150.jpg" class="attachment-gallery-thumb size-gallery-thumb" alt="A stunning outdoor patio features a communal barbecue area that provides residents and guests with the perfect place to relax and entertain." width="260" height="150"></a>
                            <div class="img-title">The Square</div>
                        </div>
                    </div>
                    <div class="img-wrapper wow rollIn the-square" data-id="main-041">
                        <div class="img-inner">
                            <div class="cap" style="display:none;">The Square is in strategic proximity to shopping centers, beaches and parks.</div>
                            <a href="http://www.omniyat.com/wp-content/uploads/2015/09/041.jpg"><img src="http://www.omniyat.com/wp-content/uploads/2015/09/041-260x150.jpg" class="attachment-gallery-thumb size-gallery-thumb" alt="The Square is in strategic proximity to shopping centers, beaches and parks." width="260" height="150"></a>
                            <div class="img-title">The Square</div>
                        </div>
                    </div>
                    <div class="img-wrapper wow rollIn the-square" data-id="main-11">
                        <div class="img-inner">
                            <div class="cap" style="display:none;">A vibrant place providing homes to a beautiful community of 524 residential apartments.</div>
                            <a href="http://www.omniyat.com/wp-content/uploads/2015/09/11.jpg"><img src="http://www.omniyat.com/wp-content/uploads/2015/09/11-260x150.jpg" class="attachment-gallery-thumb size-gallery-thumb" alt="A vibrant place providing homes to a beautiful community of 524 residential apartments." width="260" height="150"></a>
                            <div class="img-title">The Square</div>
                        </div>
                    </div>
                    <div class="img-wrapper wow rollIn the-square" data-id="main-09">
                        <div class="img-inner">
                            <div class="cap" style="display:none;">Amenities at the Square include a gorgeous swimming pool, a state-of-the-art gym, lush gardens, children’s play area and much more.</div>
                            <a href="http://www.omniyat.com/wp-content/uploads/2015/09/09.jpg"><img src="http://www.omniyat.com/wp-content/uploads/2015/09/09-260x150.jpg" class="attachment-gallery-thumb size-gallery-thumb" alt="Amenities at the Square include a gorgeous swimming pool, a state-of-the-art gym, lush gardens, children’s play area and much more." width="260" height="150"></a>
                            <div class="img-title">The Square</div>
                        </div>
                    </div>
                    <div class="img-wrapper wow rollIn the-square" data-id="main-05-7">
                        <div class="img-inner">
                            <div class="cap" style="display:none;">Boasting a diverse variety of delicious restaurants, The Square offers cuisines to suit every taste.</div>
                            <a href="http://www.omniyat.com/wp-content/uploads/2015/09/05.jpg"><img src="http://www.omniyat.com/wp-content/uploads/2015/09/05-260x150.jpg" class="attachment-gallery-thumb size-gallery-thumb" alt="Boasting a diverse variety of delicious restaurants, The Square offers cuisines to suit every taste." width="260" height="150"></a>
                            <div class="img-title">The Square</div>
                        </div>
                    </div>
                    <div class="img-wrapper wow rollIn the-square" data-id="main-02-29">
                        <div class="img-inner">
                            <div class="cap" style="display:none;">The Square is a stunning landmark by Omniyat and one of the premium developments in the area.</div>
                            <a href="http://www.omniyat.com/wp-content/uploads/2015/09/02.jpg"><img src="http://www.omniyat.com/wp-content/uploads/2015/09/02-260x150.jpg" class="attachment-gallery-thumb size-gallery-thumb" alt="The Square is a stunning landmark by Omniyat and one of the premium developments in the area." width="260" height="150"></a>
                            <div class="img-title">The Square</div>
                        </div>
                    </div>
                    <div class="img-wrapper wow rollIn one-by-omniyat" data-id="main-image1">
                        <div class="img-inner">
                            <div class="cap" style="display:none;">One Business Bay is Omniyat’s signature commercial tower.</div>
                            <a href="http://www.omniyat.com/wp-content/uploads/2016/01/Image1.jpg"><img src="http://www.omniyat.com/wp-content/uploads/2016/01/Image1-260x150.jpg" class="attachment-gallery-thumb size-gallery-thumb" alt="One Business Bay is Omniyat's signature commercial tower." width="260" height="150"></a>
                            <div class="img-title">One by Omniyat</div>
                        </div>
                    </div>
                    <div class="img-wrapper wow rollIn one-by-omniyat" data-id="main-image2">
                        <div class="img-inner">
                            <div class="cap" style="display:none;">The tower’s distinctive form gives it an appearance of a skillfully cut diamond.</div>
                            <a href="http://www.omniyat.com/wp-content/uploads/2016/01/Image2.jpg"><img src="http://www.omniyat.com/wp-content/uploads/2016/01/Image2-260x150.jpg" class="attachment-gallery-thumb size-gallery-thumb" alt="The tower's distinctive form gives it an appearance of a skillfully cut diamond." width="260" height="150"></a>
                            <div class="img-title">One by Omniyat</div>
                        </div>
                    </div>
                    <div class="img-wrapper wow rollIn bayswater" data-id="main-image4">
                        <div class="img-inner">
                            <div class="cap" style="display:none;">Bayswater is designed to give an impression of two towers inclined in opposite directions.</div>
                            <a href="http://www.omniyat.com/wp-content/uploads/2016/01/Image4.jpg"><img src="http://www.omniyat.com/wp-content/uploads/2016/01/Image4-260x150.jpg" class="attachment-gallery-thumb size-gallery-thumb" alt="Bayswater is designed to give an impression of two towers inclined in opposite directions." width="260" height="150"></a>
                            <div class="img-title">Bayswater</div>
                        </div>
                    </div>
                    <div class="img-wrapper wow rollIn bayswater" data-id="main-image3">
                        <div class="img-inner">
                            <div class="cap" style="display:none;">Work and enjoy simultaneously this beautiful view of the lake. </div>
                            <a href="http://www.omniyat.com/wp-content/uploads/2016/01/Image3.jpg"><img src="http://www.omniyat.com/wp-content/uploads/2016/01/Image3-260x150.jpg" class="attachment-gallery-thumb size-gallery-thumb" alt="Work and enjoy simultaneously this beautiful view of the lake." width="260" height="150"></a>
                            <div class="img-title">Bayswater</div>
                        </div>
                    </div>
                    <div class="img-wrapper wow rollIn the-binary" data-id="main-image5">
                        <div class="img-inner">
                            <div class="cap" style="display:none;">Facing the lake view while getting your job done is all what you could ask for. </div>
                            <a href="http://www.omniyat.com/wp-content/uploads/2016/01/Image5.jpg"><img src="http://www.omniyat.com/wp-content/uploads/2016/01/Image5-260x150.jpg" class="attachment-gallery-thumb size-gallery-thumb" alt="Facing the lake view while getting your job done is all what you could ask for." width="260" height="150"></a>
                            <div class="img-title">The Binary</div>
                        </div>
                    </div>
                    <div class="img-wrapper wow rollIn the-binary" data-id="main-entrance">
                        <div class="img-inner">
                            <div class="cap" style="display:none;">Our pyramid looking entrance is how high we aim to go. </div>
                            <a href="http://www.omniyat.com/wp-content/uploads/2016/01/Entrance.jpg"><img src="http://www.omniyat.com/wp-content/uploads/2016/01/Entrance-260x150.jpg" class="attachment-gallery-thumb size-gallery-thumb" alt="Our pyramid looking entrance is how high we aim to go." width="260" height="150"></a>
                            <div class="img-title">The Binary</div>
                        </div>
                    </div>
                    <div class="img-wrapper wow rollIn the-binary" data-id="main-image6">
                        <div class="img-inner">
                            <div class="cap" style="display:none;">Top of the top. At The Binary, this is how we make you feel. </div>
                            <a href="http://www.omniyat.com/wp-content/uploads/2016/01/Image6.jpg"><img src="http://www.omniyat.com/wp-content/uploads/2016/01/Image6-260x150.jpg" class="attachment-gallery-thumb size-gallery-thumb" alt="Top of the top. At The Binary, this is how we make you feel." width="260" height="150"></a>
                            <div class="img-title">The Binary</div>
                        </div>
                    </div>
            </div>
        </section>
			<?php $this->start('script'); ?>
              <script>
            // init Isotope
            var $grid = $('.grid').isotope({
              // options
            });
            // filter items on button click
            $('.filter-button-group').on( 'click', 'a', function() {
              var filterValue = $(this).attr('data-filter');
              var slideType = $(this).attr('data-target');
               $('#carousel_home .carousel-inner > div').addClass('item');
               $('#carousel_home .carousel-inner > div').removeClass('hidden');
               if(slideType != 'all'){
              //  alert($('#carousel_home .carousel-inner > div:not(.'+slideType+')').length);
                    $('#carousel_home .carousel-inner > div:not(.'+slideType+')').removeClass('item');
                    $('#carousel_home .carousel-inner > div:not(.'+slideType+')').addClass('hidden');
                    $('#carousel_home .carousel-inner > .item:first').addClass('active');
               }else{
                   $('#carousel_home .carousel-inner > .item').removeClass('active');
                   $('#carousel_home .carousel-inner > .item:first').addClass('active');
               }
              $grid.isotope({ filter: filterValue });
            });
        </script>
        <?php $this->end('script'); ?>