<div class="path_bar">
				<div class="row">
					<div class="col-md-9 col-md-offset-3">
						
							<div> <a href="<?= $this->Url->build("/properties/index/",true); ?>"> Home </a> > About Us </div>
						
					</div>
				</div>
			</div>
			
			<section class="section-about" style="height: 3033px;">
				<div class="col-inner">
					<div class="col-sm-5 about_banner">
						<div class="row">
							<img src="<?= $this->Url->build("/img/section1.jpg",true); ?>" class="post-image wp-post-image" alt="section1" width="2000" height="669">
						</div>
					</div>
					<div class="col-sm-7 caption-wrapper wow fadeInRight" style="height: 3033px;">
						<div class="banner-caption" style="max-width:500px">
							<h2>About</h2>
							<p>We approach the design, development and management of each Omniyat property as if it were a unique work of art. Just as there is no mistaking a Picasso or a Van Gogh, there is no mistaking that you are anywhere but in an Omniyat property. We nurture and maintain close partnerships with the world’s leading architects, engineers, interior designers and artists. It’s how we create beautiful and inspiring built environments &ndash; living canvases of residential, commercial, hospitality and retail spaces &ndash; that offer an inimitable premium experience.</p>
						</div>
					</div>
				</div>
				<div class="pos-absolute width-auto" style="width: 1349px;">
					<img src="/wp-content/uploads/2015/08/section1.jpg" class="post-image wp-post-image" alt="section1" width="2000" height="669">
				</div>
			</section>


			<section class="stars-archive clearfix" id="team">
			    <header>
			        <div class="container">
			            <div class="row">
			                <div class="col-sm-4 col-md-6">
			                    <h2 class="pull-left">The Team</h2>
			                </div>
			                <div class="col-sm-8 col-md-6">
			                </div>
			            </div>
			        </div>
			    </header>
			    <section class="clearfix stars-archive-row">
			        <div class="container">
			            <div class="row">
			                <div class="col-md-5 col-sm-5 pull-left wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
			                    <div class="stars-archive-thumb pull-right">
			                        <img src="http://www.omniyat.com/wp-content/uploads/2015/08/mahdi-amjad1-363x297.jpg" class="post-image wp-post-image" alt="MAHDI K M J AMJAD">
			                    </div>
			                </div>
			                <div class="col-md-7 col-sm-7 wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;">
			                    <div class="stars-archive-col pull-left">
			                        <div class="pull-left stars-archive-text">
			                            <h3>MAHDI K M J AMJAD</h3>
			                            <div class="excerpt-content">
			                                <p><strong>Mahdi K M J Amjad</strong>, Mahdi Amjad founded Omniyat in 2005 with a vision to contribute to Dubai’s skyline through the development of modern commercial and residential buildings. He is the Executive Chairman and Chief Executive Officer of what has become one of Dubai’s fastest growing and most significant players in the real estate industry....</p>
			                            </div>
			                            <div class="full-content" style="display:none;">
			                                <p>Executive Chairman and Chief Executive Officer</p>
			                                <p><strong>&nbsp;</strong><strong>&nbsp;</strong>Mahdi Amjad founded Omniyat in 2005 with a vision to contribute to Dubai’s skyline through the development of modern commercial and residential buildings. He is the Executive Chairman and Chief Executive Officer of what has become one of Dubai’s fastest growing and most significant players in the real estate industry.</p>
			                                <p>Over the last decade, Amjad has lead Omniyat to become a leading privately-held property developer that&nbsp; has dynamically enhanced the city’s skyline with landmark projects. He has led Omniyat to put art and design at the heart of every one of its developments and has been instrumental in large-scale collaborations with award-winning and globally respected designers, including the late Dame Zaha Hadid, Foster &amp; Partners and Super Potato.</p>
			                                <p>At the start of 2017, Amjad led the unveiling of ultra-luxury development One Palm, the newest and most exclusive residential space on the globally-recognized Palm Jumeirah. Also in 2017, Amjad will unveil The Opus, the first Dame Zaha Hadid-designed building in Dubai and the home to the first ME by Melia hotel in the region.</p>
			                                <p>Amjad’s astute management capability, visionary outlook and proactive decisions have helped Omniyat&nbsp; to orchestrate development of unique projects at vantage locations within master communities. The company’s consistent and bespoke approach to all projects, from their design through to their delivery, generates a superior value proposition in a variety of investment environments and is widely recognized as an industry benchmark in the Arab region.</p>
			                                <p>In 2005, Amjad looked at Dubai’s skyline as his canvas and still today he is making his mark with brushstrokes of unique and landmark projects, and with many more projects in the pipeline continues to innovate and delight.</p>
			                            </div>
			                            <a class="read-more readMoreBtn btn pull-left" href="javascript:void(0)">Read More</a> 									
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </section>
			    <section class="clearfix stars-archive-row">
			        <div class="container">
			            <div class="row">
			                <div class="col-md-5 col-sm-5 pull-left wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
			                    <div class="stars-archive-thumb pull-right">
			                        <img src="http://www.omniyat.com/wp-content/uploads/2015/08/mark-phoenix2-363x297.jpg" class="post-image wp-post-image" alt="MARK PHOENIX">
			                    </div>
			                </div>
			                <div class="col-md-7 col-sm-7 wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;">
			                    <div class="stars-archive-col pull-left">
			                        <div class="pull-left stars-archive-text">
			                            <h3>MARK PHOENIX</h3>
			                            <div class="excerpt-content">
			                                <p><strong>Mark Phoenix</strong>, a creative thinker and inspirational leader, is the Managing Director of Omniyat and leads the Group’s operations in the UAE. Mark has driven the organisation’s strategic approach of collaborating with high-profi...</p>
			                            </div>
			                            <div class="full-content" style="display:none;">
			                                <p><strong>Mark Phoenix</strong>, a creative thinker and inspirational leader, is the Managing Director of Omniyat and leads the Group’s operations in the UAE.</p>
			                                <p>Mark has driven the organisation’s strategic approach of collaborating with high-profile designers and top-notch sales consultants. He directs these successful and profitable partnerships on a day-to-day basis, protecting Omniyat’s distinctive reputation for providing valuable and high quality functional design &ndash; fuelling the developer’s long-term advancement in the premium real-estate market.</p>
			                                <p>Driven by artistic sensibilities, Mark ensures the Group’s steady growth with prudent diligence and conscientious supervision. He leads Omniyat’s development strategy, which has made the brand one of the leading privately-held property developers, committed to Dubai’s vision of leading by design.</p>
			                            </div>
			                            <a class="read-more readMoreBtn btn pull-left" href="javascript:void(0)">Read More</a> 									
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </section>
			    <section class="clearfix stars-archive-row">
			        <div class="container">
			            <div class="row">
			                <div class="col-md-5 col-sm-5 pull-left wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
			                    <div class="stars-archive-thumb pull-right">
			                        <img src="http://www.omniyat.com/wp-content/uploads/2015/08/tony-manning1-363x297.jpg" class="post-image wp-post-image" alt="TONY MANNING">
			                    </div>
			                </div>
			                <div class="col-md-7 col-sm-7 wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;">
			                    <div class="stars-archive-col pull-left">
			                        <div class="pull-left stars-archive-text">
			                            <h3>TONY MANNING</h3>
			                            <div class="excerpt-content">
			                                <p><strong>Tony Manning</strong> brings over 30 years of professional and commercial experience ranging from SME’s as the founding partner of a chartered accountancy practice to the largest international companies with KPMG and BP. He moved from Londo...</p>
			                            </div>
			                            <div class="full-content" style="display:none;">
			                                <p><strong>Tony Manning</strong> brings over 30 years of professional and commercial experience ranging from SME’s as the founding partner of a chartered accountancy practice to the largest international companies with KPMG and BP. He moved from London to Dubai in 1996 to take up the position of Chief Financial Officer with EPPCO.</p>
			                                <p>As CFO, he has contributed to the management and control of Omniyat’s sustained rapid growth together with transformation of the Group following the dynamic start up and establishment of Omniyat in 2005.</p>
			                            </div>
			                            <a class="read-more readMoreBtn btn pull-left" href="javascript:void(0)">Read More</a> 									
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </section>
			    <section class="clearfix stars-archive-row">
			        <div class="container">
			            <div class="row">
			                <div class="col-md-5 col-sm-5 pull-left wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
			                    <div class="stars-archive-thumb pull-right">
			                        <img src="http://www.omniyat.com/wp-content/uploads/2015/08/ishan-garga-363x297.jpg" class="post-image wp-post-image" alt="ISHAN GARGA">
			                    </div>
			                </div>
			                <div class="col-md-7 col-sm-7 wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;">
			                    <div class="stars-archive-col pull-left">
			                        <div class="pull-left stars-archive-text">
			                            <h3>ISHAN GARGA</h3>
			                            <div class="excerpt-content">
			                                <p><strong>Ishan Garga</strong>, the Executive Director of Corporate Finance and Investments, has over twelve years of experience in investment banking, corporate finance, strategy and risk consulting. He was previously working at Investate, an investme........</p>
			                            </div>
			                            <div class="full-content" style="display:none;">
			                                <p><strong>Ishan Garga</strong>, the Executive Director of Corporate Finance and Investments, has over twelve years of experience in investment banking, corporate finance, strategy and risk consulting. He was previously working at Investate, an investment firm, where he was responsible for sourcing new deals, fund raising and structuring investment products. Ishan has completed a Post Graduate Programme in Management from the Management Development Institute in India, and a BSc from Delhi University. He has worked for leading consulting firms such as KPMG and PricewaterhouseCoopers, India.</p>
			                            </div>
			                            <a class="read-more readMoreBtn btn pull-left" href="javascript:void(0)">Read More</a> 									
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </section>
			    <section class="clearfix stars-archive-row">
			        <div class="container">
			            <div class="row">
			                <div class="col-md-5 col-sm-5 pull-left wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
			                    <div class="stars-archive-thumb pull-right">
			                        <img src="http://www.omniyat.com/wp-content/uploads/2015/08/mohammed-hmeid-363x297.jpg" class="post-image wp-post-image" alt="MOHAMMED HMEID">
			                    </div>
			                </div>
			                <div class="col-md-7 col-sm-7 wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;">
			                    <div class="stars-archive-col pull-left">
			                        <div class="pull-left stars-archive-text">
			                            <h3>MOHAMMED HMEID</h3>
			                            <div class="excerpt-content">
			                                <p><strong>Mohammed Hmeid</strong>, the Sales and Marketing Director at Omniyat, brings an innovative and distinctive approach to the brand experience and drives the creative vision of the organization. Mohammed has brought the concept - creating livabl........</p>
			                            </div>
			                            <div class="full-content" style="display:none;">
			                                <p><strong>Mohammed Hmeid</strong>, the Sales and Marketing Director at Omniyat, brings an innovative and distinctive approach to the brand experience and drives the creative vision of the organization. Mohammed has brought the concept &ndash; creating livable art, to life by initiating the thought process behind each development and creating a distinct personality for them.</p>
			                                <p>He has ensured this concept is backed by a strong marketing infrastructure and has developed bespoke sales methodology that allows the brand to tell its story and sell its promise.</p>
			                                <p>With over 18 years of experience in sales and marketing in the MENA region, Mohammed successfully cultivates relationships built on trust and mutual satisfaction. The company’s belief that value is first created in the minds of the customers is ably communicated by him and his team to stakeholders during their first experience at Omniyat’s sales boutique.</p>
			                                <p>Mohammed’s reputation for designing and delivering outstanding sales experiences are unmatched. He drives a top notch and consistent approach, keeping Omniyat’s promise of conforming to individuality and detail orientation at all times.</p>
			                                <p>His work for Omniyat won over 20 global awards and accolades while his result driven approach has resulted in breaking sales records while working with  General Motors, Jaguar Land Rover.</p>
			                            </div>
			                            <a class="read-more readMoreBtn btn pull-left" href="javascript:void(0)">Read More</a> 									
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
			</section>
			    <!--/.omniyat omoniart -->
			<section class="section-omniart" id="omniyart" style="overflow:hidden; background:#060c19 url(<?= $this->Url->build("/img/bg-omniart.jpg",true); ?>) no-repeat center top;">
			        <div class="container">
			            <h2>OMNIYAT: WHERE ART INSPIRES LIVING</h2>
			            <div class="row">
			                <div class="col-sm-6 col-md-6">
			                    <p>At Omniyat, we believe in ‘Creating distinct liveable art in Dubai’ and OMNIYAT is the essence of the experience we create.</p>
			                    <p>All of our projects are driven by exclusive artistic sensibilities where design &amp; art are at the centre piece of each of our developments and are committed to contributing towards the city’s vision of leading by design.</p>
			                    <p>We collaborate with high profile designers such as Zaha Hadid, Steve Leung and Super Potato. Each designer, world-renowned for their award-winning designs, brings a distinct artistic look and feel to every individual property and this is the life-force behind OMNIYAT.</p>
			                </div>
			                <div class="col-sm-6 col-md-6">
			                    <p>Every project carries its unique design signature which speaks to the aesthetic sensibilities of its audience &ndash; from the contemporary oriental look and feel at One at Palm Jumeirah, to the artistic touch at The Opus. Additionally, a Miami-chic feel at The Pad to a functional and modern experience at the Square and the art deco at The ANWA, all properties reflect OMNIYAT sensibilities.</p>
			                    <p>Each of our signature project design offers serious bragging rights and every project is planned after studying the lifestyle aspirations of potential residents. We believe in consistent delivery on the design promise, to create a superior return on investment and give every home owner the fulfilling experience of living in a unique space that reflects their personality.</p>
			                </div>
			            </div>
			        </div>
			</section>
