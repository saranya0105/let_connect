<div class="path_bar wow flipInX">
		<div class="row">
			<div class="col-lg-11 col-lg-offset-1">
				
					<div> <a href="<?= $this->Url->build("/properties/index/",true); ?>"> Home </a> > Contact Us </div>
				
			</div>
		</div>
	</div>
	<section class="contact clearfix" style="clear:both">
		<div class="custom contact_col col-lg-7 col-md-7 col-sm-12 col-xs-12 pull-left">
			<div class="row">
				<div class="map-container-left map_g">
					<img src="<?= $this->Url->build("/img/map.png",true); ?>">
				</div>
			</div>
		</div>
		<div class="custom contact_col col0lg-5 col-md-5 col-sm-12 col-xs-12">
			<div class="row">
				<div class="contact-image">
					<img src="http://www.omniyat.com/wp-content/uploads/2015/08/contact_us.jpg" class="post-image wp-post-image" alt="contact_us" width="671" height="568">
				</div>
				<div class="contact-right-col pull-left contact_pg">
				    <?= $this->Flash->render() ?>
					<h2>Contact Us</h2>
					<div class="wow fadeInLeft animated contact_content_para" style="visibility: visible; animation-name: fadeInLeft;">
						<h3><span style="color: #ffffff;">Omniyat Sales Suite – Ground Level</span></h3>
						<p>One by Omniyat<br>
						Business Bay<br>
						Toll-free (within the UAE):<strong> 800 666</strong><br>
						Outside the UAE: +971 4 511 5004</p>
						<h3><span style="color: #ffffff;">Omniyat Head Office – 26th floor</span></h3>
						<p>One by Omniyat<br>
						Business Bay<br>
						04 511 5000</p>
						<h3><span style="color: #ffffff;">For Resales &amp; Leasing Enquiries</span></h3>
						<p><a href="mailto:assetmanagement@omniyat.com">assetmanagement@omniyat.com</a>&nbsp;OR call +971 (0)4 511 5449</p>
						<h3><span style="color: #ffffff;">Connect with us</span></h3>
						<p><a href="mailto:sales@omniyat.com">sales@omniyat.com</a></p>
						<p><a href="http://omniyat.com/privacy.pdf"><b>PRIVACY POLICY</b></a></p>
						<p><a class="btn btn-white" data-toggle="modal" data-target="#email_friend" title="Enquire Now">Enquire Now</a></p>
					</div>
				</div>
			</div>
		</div>
		<div id="email_friend" class="modal fade" role="dialog">
							  <div class="modal-dialog">

							    <!-- Modal content-->
							    <div class="modal-content" id="mail_popup">
							    	<div class="modal-header" id="mail_popup_head"> 
							    		<button type="button" class="close btn-danger" data-dismiss="modal" aria-label="Close" style="background-color: #d9534f;border-color: #d43f3a;"><span aria-hidden="true">&times;</span></button>
									</div>
							     	<div class="modal-body" id="mail_popup_body">
								      	
								      
								
								   <?= $this->Form->create(null,['class'=>'friends_mail_form','style'=>'height:560px;','validate'=>true]); ?>		
										<div class="mail_form_input1">
											<div><?= $this->Omniyat->input('first_name',['placeholder'=>'First Name','required'=>true]); ?></div>
											<div><?= $this->Omniyat->input('last_name',['placeholder'=>'Last Name','required'=>true]); ?></div>
											<div><?= $this->Omniyat->input('sender_email',['placeholder'=>'Email','required'=>true]); ?></div>
											<div>
											    <?= $this->element('Parts/isd_code'); ?>
											    <?= $this->Omniyat->input('phone',['placeholder'=>'Your Phone','required'=>true]); ?>
											    

                                             </div>   
											<div><?= $this->Omniyat->input('nation',['placeholder'=>'Nationality','required'=>true]); ?></div>
											<div><?= $this->Omniyat->input('interested',['type'=>'textarea','required'=>true,'value'=>'I am interested in the development that i saw on your website. Can you please contact me so we can discuss more details regarding this opportunity?']); ?></div>
											<button class="btn submit" type="submit">SEND</button>
											<div class="detail_terms1">
												<small>
														By submiting this form, you accept<br>
														our <span class="detail_promise">Terms of use</span> and <span class="detail_promise">Privacy policy</span>
												</small>
											</div>
										</div>
									<?= $this->Form->end(); ?>
							      </div>
							    </div>
							  </div>
							</div>
	</section>