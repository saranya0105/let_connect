<div id="carousel_home" class="carousel slide wow fadeIn" data-ride="carousel">
			<ol class="carousel-indicators">
			    <li data-target="#carousel_home" data-slide-to="0" class="active"></li>
			    <li data-target="#carousel_home" data-slide-to="1"></li>
			    <li data-target="#carousel_home" data-slide-to="2"></li>
			    <li data-target="#carousel_home" data-slide-to="3"></li>
		    </ol>
		 <!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<img src="<?= $this->Url->build("/img/One by Omniyat - 1.jpg",true); ?>" alt="...">
				</div>
				<div class="item">
					<img src="<?= $this->Url->build("/img/Bayswater-2.jpg",true); ?>" alt="...">
				</div>
				<div class="item">
					<img src="<?= $this->Url->build("/img/The Binary-3.jpg",true); ?>" alt="...">
				</div>
				<div class="item">
					<img src="<?= $this->Url->build("/img/The Square-4.jpg",true); ?>" alt="...">
				</div>
			</div>
			<div class="carousel-caption" style="z-index:0;">
				<div class="main-content">
					<p id="heading-main">LUXURY PROPERTY FOR RENT</p>
					<p id="heading-sub">Luxury residential specialists</p>
					<button id="explore" type="button" class="btn btn-default btn-lg">Discover more</button>
				</div>
			</div>
			<a class="left carousel-control" href="#carousel_home" data-slide="prev">
		    	<span class="glyphicon glyphicon-chevron-left"></span>
		    	<span class="sr-only">Previous</span>
		    </a>
		    <a class="right carousel-control" href="#carousel_home" data-slide="next">
		    	<span class="glyphicon glyphicon-chevron-right"></span>
		    	<span class="sr-only">Next</span>
		    </a>
		</div>

	<div  id="search-wrap" >
		        <div class="search_container">
						<div class="inpt slct slct_dd banner_dropdown">
						    <b class="default" id="resident_dropdown">Property Type</b>
						     <?= $this->Omniyat->input('resident_type',['type'=>'hidden']); ?>
						    <input class="filter_selected" name="category_id" value="" type="hidden">
						    <div class="dd">
						        <ul>
						        	<li><a data-id="" href="#">Residential for rent</a></li>
						            <li><a class="" href="#" data-id="1">Residential for Buy</a></li>
						            <li><a class="" href="#" data-id="2">Commercial for rent</a></li>
						            <li><a class="" href="#" data-id="3">Commercial for Buy</a></li>						            
						        </ul>
						    </div>
						</div>
						<div class="inpt slct slct_dd banner_dropdown">
						    <b class="default" id="location">Search by Location</b>
						    <input class="filter_selected" name="category_id" value="" type="hidden">
						    <div class="dd">
						      <ul>

						      <?php foreach($locations as $location_id => $location){ ?>
						      <li><a data-id="" href="#" id="<?php echo $location_id;?>"><?php echo $location; ?></a></li>
						      <?php } ?>
						      </ul>
						        
						    
						    </div>
						</div>

					<a id="search-btn" onclick="myFunction()" href="#" class="btn btn-default" type="search" style="color: rgb(255, 255, 255);">Search</a>
					</div>
				</div>
	<div class="container-second">
		
		<p>explore our featured properties</p>
	</div>	
	<div class="row" id="featured-property">
		<div class="col-lg-4 col-md-4 col-sm-12 wow fadeInLeft">
			<div class="featured_image_wrapper"><img src="<?= $this->Url->build("/img/featured_one.png",true); ?>"></div>
			<div class="featured_inner_text">langham place<br>downtown dubai<br>residence & the<br>hotel</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-12 wow fadeInUp">
			<div class="featured_image_wrapper"><img src="<?= $this->Url->build("/img/featured_second.png",true); ?>"></div>
			<div class="featured_inner_text">the sterling <br>a paradise for the<br>exceptional</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-12 wow fadeInRight">
			<div class="featured_image_wrapper"><img src="<?= $this->Url->build("/img/featured_third.png",true); ?>"></div>
			<div class="featured_inner_text">one palm<br>palm jumeirah,<br>dubai</div>
		</div>						
	</div>
	<div class="container-third">
		<p>view our latest hot listings</p>
	</div>	
	<section>
		<div id="hot-listing" class="container-fluid">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-12 hot_listing_wrapper wow fadeIn" >
					<div class="hot_listing_inner">
						<img src="<?= $this->Url->build("/img/hot_listing_one.png",true); ?>">
						<div class="hot_link_inner">THE GARDENS, PALM JUMEIRAH<br>AED 17,500,000</div>
						<div class="btn_wrap">
							<button class="btn btn-primary">for sale</button>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 hot_listing_wrapper wow fadeIn">
					<div class="hot_listing_inner">
						<img src="<?= $this->Url->build("/img/hot_listing_two.png",true); ?>">
						<div class="hot_link_inner">THE GARDENS, PALM JUMEIRAH<br>AED 17,500,000</div>
						<div class="btn_wrap">
							<button class="btn btn-primary">for sale</button>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 hot_listing_wrapper wow fadeIn">
					<div class="hot_listing_inner">
						<img src="<?= $this->Url->build("/img/hot_listing_three.png",true); ?>">
						<div class="hot_link_inner">THE GARDENS, PALM JUMEIRAH<br>AED 17,500,000</div>
						<div class="btn_wrap">
							<button class="btn btn-primary">for sale</button>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 hot_listing_wrapper wow fadeIn">
					<div class="hot_listing_inner">
						<img src="<?= $this->Url->build("/img/hot_listing_four.png",true); ?>">
						<div class="hot_link_inner">THE GARDENS, PALM JUMEIRAH<br>AED 17,500,000</div>
						<div class="btn_wrap">
							<button class="btn btn-primary">for sale</button>
						</div>
					</div>
				</div>
			</div>									
		</div>
	</section>
	<div class="container-fourth">
		<a href="<?= $this->Url->build("/properties/index/",true); ?>" target="_blank"><button id="more-listing" class="btn btn-primary" type="search" style="color: rgb(255, 255, 255);">view all our listing</button></a>
	</div>
	<section class="subscribing">
		<div class="row margin-none">
			<div id="subcrib_txt_one" class="col-lg-6 col-md-6 col-sm-12 ">
				<div class="local-knowledge-head">LOCAL KNOWLEDGE</div>
				<div class="local-knowledge">Our team are all certified by the Real Estate<br>Regulatory Agency giving you peace if mind</div>
			</div>
			<div id="subcrib_img_one" class="col-lg-6 col-md-6 col-sm-12 wow slideInRight">
				<img src="<?= $this->Url->build("/img/subcribe_one.png",true); ?>">
			</div>
		</div>	
		<div class="row margin-none">
			<div id="subcrib_img_two" class="col-lg-6 col-md-6 col-sm-12 wow slideInLeft">
				<img src="<?= $this->Url->build("/img/subcribe_two.png",true); ?>">
				<div class="subcribe_img_txt">Get the latest news and happenings<br>on luxury real estate directly in your inbox</div>
			</div>
			<div id="subcrib_submition" class="col-lg-6 col-md-6 col-sm-12 ">
				<div class="email_input_area">
					<input class="btn btn-lg" name="email" id="email" type="email" placeholder="Enter your email" required>
				</div>
				<div class="checkbox_main">
					<input id="c1" class="chk" type="checkbox">
					<label for="c1">
						<span></span>
						I am a Broker/Agent
					</label>
				</div>
				<div id="subcrib_btn" ><button id="subcrib_btn_inner" class="btn btn-info btn-lg" type="submit">Submit</button></div>
			</div>
			<div class="promise">By subscribing, you are agreeing to our Privacy Policy. You can unsubscribe anytime<br>from our emails by clicking the "unsubscribe" link.</div>
		</div>
	</div>
	</div>
	</section>	
	 <script type="text/javascript">
	 function myFunction(){

	 	var resident = document.getElementById("resident_dropdown").innerHTML;
	 	var location = document.getElementById("location").innerHTML;
	 	var location_id ="";
	 	if(location =='The Square')
	 		location_id = 93;
	 	else if(location =='One By Omniyat')
	 		location_id = 94;
	 	else if(location =='Binary')
	 		location_id = 95;
	 	else
	 		location_id = '';
	 	if(resident == 'Property Type' && location_id  == '') {
	 		//alert("Please select Property Type and Location");
	 		//exit;
	 	}
	 	if(resident == 'Property Type'){
	 		//alert("Please select Property Type");
	 		//exit;
	 	}
	 	if(location_id  == ''){
	 		//alert("Please select Location");
	 		//exit;
	 	}
	 	if(resident != 'Property Type' && location_id  != '') {

		 	if(resident == 'Residential for rent'){
		 		resident = 'Resident';
		 		window.location.href = "/liv/properties/index/Rent?type=" + resident+ "&location_id="+location_id; 
		 	}
		 	if(resident == 'Residential for Buy'){
		 		resident = 'Resident';
		 		window.location.href = "/liv/properties/index/Sale?type=" + resident+ "&location_id="+location_id; 
		 	}
		 	if(resident == 'Commercial for rent'){
		 		resident = 'Commercial';
		 		window.location.href = "/liv/properties/index/Rent?type=" + resident+ "&location_id="+location_id; 

		 	}
		 	if(resident == 'Commercial for Buy'){
		 		resident = 'Commercial';
		 		window.location.href = "/liv/properties/index/Sale?type=" + resident+ "&location_id="+location_id; 
		 	}
	 	}

	 }
	 </script>