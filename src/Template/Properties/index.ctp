<div class="row" id="toolbar">
	<div id="properties-count" class="col-lg-3 col-sm-4">
		<?= $this->Paginator->counter('{{count}}') ?> Properties <?php if($type!=''){echo 'For '.$type;}?> In Dubai
	</div>
	<div id="toolbar_icons" class="col-lg-5 col-md-offset-4 col-sm-4 col-sm-offset-1">
	<div class="tools">
		<div class="tools_icon">sort by</div>
		<div class="inpt slct slct_dd sort_dropdown">
			    <b class="default">Select option</b>
			    <input class="filter_selected" name="category_id" value="" type="hidden">
			    <div class="dd">                    
			        <ul class="filter_sort">
			            <li><?php echo $this->Paginator->sort('price', 'Highest Price', ['direction' => 'desc']);?></li>
			            <li><?php echo $this->Paginator->sort('price', 'Lowest Price', ['direction' => 'asc']);?></li>
			            
			            <li><?php echo $this->Paginator->sort('created', 'Recently added', ['direction' => 'asc']);?></li>
			        </ul>
			    </div>
			</div>
		<span></span>
		<div class="tools_icon">view by</div>
		<div class="tools_icon"><a class="switch_view" data-view="grid" href=""><img id="grid_view_icon" src="<?= $this->Url->build("/img/grid-view.png",true); ?>" ></a></div>
		<div class="tools_icon"><img src="<?= $this->Url->build("/img/line.png",true); ?>"></div>
		<div class="tools_icon"><a class="switch_view" data-view="list" href=""><img id="list_view_icon" src="<?= $this->Url->build("/img/list-view.png",true); ?>"></a></div>
		<div class="tools_icon"><img src="<?= $this->Url->build("/img/line.png",true); ?>"></div>
		<div class="tools_icon"><a class="switch_view icon_map" data-view="map" href=""><img id="location_icon" src="<?= $this->Url->build("/img/location.png",true); ?>"></a></div>

		</div>
	</div>
</div>
<div class="path_bar">
	<div class="row">
		<div class="col-md-9 col-md-offset-3">
			
				<div> <a href="<?= $this->Url->build("/",true); ?>"> Home </a> > Properties  <?php if($type!=''){echo '> For '.$type;}?> </div>
			 
		</div>
	</div>
</div>
<div class="content_area" id="wrapper">
	<div id="content">
		<div class="row">
			<?= $this->element('Parts/filter_sidebar'); ?>
			<div class="col-lg-9 col-sm-8 col-xs-12">
				<div class="main_content_area">
					<div class="row view_gird_list">
					<?php foreach($properties as $property): ?>
						<?= $this->element('Parts/properties',['property'=>$property]); ?>
						<?php endforeach; ?>
					</div>
					<div class="map_container" id="map">
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="paginator" style="text-align: center;">
		<ul class="pagination">
			<?= $this->Paginator->first('<< ' . __('first')) ?>
			<?= $this->Paginator->prev('< ' . __('previous')) ?>
			<?= $this->Paginator->numbers() ?>
			<?= $this->Paginator->next(__('next') . ' >') ?>
			<?= $this->Paginator->last(__('last') . ' >>') ?>
		</ul>
		
	</div>
</div>
	
<?php //echo $this->element( 'Parts/list_script_footer' ); ?> 

<?php $this->start('script'); ?>
<script>
        $('.filter_sort a').click(function(){
           window.location.href = $(this).attr('href'); 
        });
      // The following example creates complex markers to indicate beaches near
      // Sydney, NSW, Australia. Note that the anchor is set to (0,32) to correspond
      // to the base of the flagpole.
        $('.icon_map').click(function(){
            var map = new google.maps.Map(document.getElementById('map'), {
              zoom: 10,
              center: {lat: 25.1472177, lng: 55.1193884}
            });
    
            setMarkers(map);
            //alert('hi');
        });
      

      // Data for the markers consisting of a name, a LatLng and a zIndex for the
      // order in which these markers should display on top of each other.
      var lat='25.188331';
		var lang='55.261828';
var lat='25.187079';
		var lang='55.266913';
var lat='25.184157';
		var lang='55.264202';

      var beaches = [
        ['Bondi Beach', 25.290394, 55.353754, 4],
        ['Coogee Beach', 25.188331, 55.261828, 5],
        ['The Binary', 25.187079, 55.266913, 3],
        ['BAYSWATER', 25.184157, 55.264202, 2]
      ];

      function setMarkers(map) {
        // Adds markers to the map.

        // Marker sizes are expressed as a Size of X,Y where the origin of the image
        // (0,0) is located in the top left of the image.

        // Origins, anchor positions and coordinates of the marker increase in the X
        // direction to the right and in the Y direction down.
        var image = {
          url: 'http://www.omniyat.com/wp-content/themes/omniyat-theme/img/marker-red.png',
          // This marker is 20 pixels wide by 32 pixels high.
          //size: new google.maps.Size(20, 32),
          // The origin for this image is (0, 0).
          //origin: new google.maps.Point(0, 0),
          // The anchor for this image is the base of the flagpole at (0, 32).
          //anchor: new google.maps.Point(0, 32)
        };
        // Shapes define the clickable region of the icon. The type defines an HTML
        // <area> element 'poly' which traces out a polygon as a series of X,Y points.
        // The final coordinate closes the poly by connecting to the first coordinate.
        var shape = {
          coords: [1, 1, 1, 20, 18, 20, 18, 1],
          type: 'poly'
        };
        for (var i = 0; i < beaches.length; i++) {
          var beach = beaches[i];
          var marker = new google.maps.Marker({
            position: {lat: beach[1], lng: beach[2]},
            map: map,
            icon: image,
            shape: shape,
            title: beach[0],
            zIndex: beach[3]
          });
        }
      }
    </script>

<?php $this->end('script'); ?>