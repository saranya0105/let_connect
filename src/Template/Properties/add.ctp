<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="properties form large-9 medium-8 columns content">
    <?= $this->Form->create($property,array('enctype' => 'multipart/form-data') )?>
    <fieldset>
        <legend><?= __('Add Property') ?></legend>
        <?php
            echo $this->Form->control('is_featured');
            echo $this->Form->control('reference_no');
            echo $this->Form->control('unit_no');
            echo $this->Form->control('type');
            echo $this->Form->control('street_no');
            echo $this->Form->control('floor');
            echo $this->Form->control('category_id', ['options' => $categories, 'empty' => true]);
            echo $this->Form->control('emirate_id', ['options' => $emirates, 'empty' => true]);
            echo $this->Form->control('location_id', ['options' => $locations, 'empty' => true]);
            echo $this->Form->control('sub_location_id', ['options' => $subLocations, 'empty' => true]);
            echo $this->Form->control('beds');
            echo $this->Form->control('baths');
            echo $this->Form->control('bua');
            echo $this->Form->control('plot');
            echo $this->Form->control('price');
            echo $this->Form->control('is_rental');
            echo $this->Form->control('rental_frequency');
            echo $this->Form->control('cheques');
            echo $this->Form->control('price_per_sq_ft');
            echo $this->Form->control('parking');
            echo $this->Form->control('listing_title');
            echo $this->Form->control('description');
            echo $this->Form->control('furnished');
            echo $this->Form->control('view');
            echo $this->Form->control('latitude');
            echo $this->Form->control('longitude');
            echo $this->Form->control('date_listed');
            echo $this->Form->control('last_updated');
            echo $this->Form->control('agent_id', ['options' => $agents, 'empty' => true]);
            echo $this->Form->control('crm_ref');
            echo $this->Form->control('link_ref');
            echo $this->Form->control('property_features._ids', ['options' => $propertyFeatures]);
            echo $this->Form->control('image', array('type' => 'file','help'=>'use .JPG .PNG','label'=>'Property Image'));
            
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
