	<div class="path_bar">
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					
						<div> Home > <a href="listing.html">Properties > For Sale > <?= $property->reference_no; ?> </a></div>
					
				</div>
			</div>
		</div>
		<div id="carousel_home" class="carousel slide wow fadeIn" data-ride="carousel">
			<ol class="carousel-indicators">
			    <li data-target="#carousel_home" data-slide-to="0" class="active"></li>
			    <li data-target="#carousel_home" data-slide-to="1"></li>
			    <li data-target="#carousel_home" data-slide-to="2"></li>
			    <li data-target="#carousel_home" data-slide-to="3"></li>
		    </ol>
		<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<img src="<?= $this->Url->build("/img/One by Omniyat - 1.jpg",true); ?>" alt="...">
				</div>
				<div class="item">
					<img src="<?= $this->Url->build("/img/Bayswater-2.jpg",true); ?>" alt="...">
				</div>
				<div class="item">
					<img src="<?= $this->Url->build("/img/The Binary-3.jpg",true); ?>" alt="...">
				</div>
				<div class="item">
					<img src="<?= $this->Url->build("/img/The Square-4.jpg",true); ?>" alt="...">
				</div>
			</div>
			<a class="left carousel-control" href="#carousel_home" data-slide="prev">
		    	<span class="glyphicon glyphicon-chevron-left"></span>
		    	<span class="sr-only">Previous</span>
		    </a>
		    <a class="right carousel-control" href="#carousel_home" data-slide="next">
		    	<span class="glyphicon glyphicon-chevron-right"></span>
		    	<span class="sr-only">Next</span>
		    </a>
		</div>

				<div class="actionsBottom wow slideInLeft">
		    <div class="wrap clear">
				<ul>
				    <li style="width: 20%;">
				        <a href="#" class="icon_photos act click_slider" data-link="http://www.gulfsothebysrealty.com/buy/11-bedroom-villa-for-sale-in-palm-jumeirah-dubai-ER-S-17909" data-id="4446" data-image="/images/return_image_data/11-bedroom-villa-for-sale-in-palm-jumeirah-dubai-ER-S-17909-332747.jpg" data-sublocation="Signature Villas Frond D" data-location="Palm Jumeirah" data-emirate="Dubai" data-price="200,000,000 AED" data-size="40,000 Sq.Ft" data-reference="ER-S-17909">
				        <button class="btn" onclick="toggleFullScreen()">
				        <span>Photos</span>
				        </button>
				        </a>
				    </li>
				    <li style="width: 20%;">
				        <a  href="#" class="icon_videos" data-video="7ilQ80kShoc" data-link="http://www.gulfsothebysrealty.com/buy/11-bedroom-villa-for-sale-in-palm-jumeirah-dubai-ER-S-17909" data-id="4446" data-image="http://watermark.propspace.com/watermark5?c_id=1004&amp;l_id=1576257624833140&amp;aid=1452375&amp;id=14961283059737934&amp;wmt=1499605269&amp;image=30_05_2017-14_25_17-1004-62bf1edb36141f114521ec4bb4175579.jpg&amp;resize=true&amp;watermarkdis=true" data-sublocation="Signature Villas Frond D" data-location="Palm Jumeirah" data-emirate="Dubai" data-price="200,000,000 AED" data-size="40,000 Sq.Ft" data-reference="ER-S-17909">
				        <button class="btn" id="video_popup">
				        <span>Video</span></button>
				        </a>
				    </li>
				    <li style="width: 20%;"><a href="#" class="icon_share" onclick="shareProduct()">
				    	<button class="btn"><span>Share this</span></button></a>
				    </li>
				    <li style="width: 20%;">
				        <a href="#" class="icon_email" title="Email a friend" data-textarea="I found this amazing property on Gulf Sotheby’s International Realty website. Take a look, and let me know what you think. http://www.gulfsothebysrealty.com/buy/11-bedroom-villa-for-sale-in-palm-jumeirah-dubai-ER-S-17909">
				        <button class="btn" data-toggle="modal" data-target="#email_friend" ><span>Email to a friend</span></button></a>

				        <div id="email_friend" class="modal fade" role="dialog">
						  <div class="modal-dialog">

						    <!-- Modal content-->
						    <div class="modal-content" id="mail_popup">
						    	<div class="modal-header" id="mail_popup_head"> 
						    		<button type="button" class="close btn-danger" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								</div>
						     	<div class="modal-body" id="mail_popup_body">
							      	<form class="friends_mail_form">
							      		
										<div class="mail_form_input">
											<div><input type="Email" name="Mail" placeholder="Your email"></div>
											<div><input type="Email" name="Mail" placeholder="Your email"></div>
											<div><input type="text" name="Phone" placeholder="Friend's email"></div>
											<div><textarea cols="20">I found this amazing property on Gulf Sotheby’s International Realty website. Take a look, and let me know what you think. http://www.gulfsothebysrealty.com/buy/11-bedroom-villa-for-sale-in-palm-jumeirah-dubai-ER-S-17909
											</textarea></div>
											<div class="btn">SEND</div>
										</div>
									</form>
						      	</div>
						    </div>
						  </div>
						</div>
				    </li>
				</ul>
		    </div>
		</div>
		<div class="row description_area">
			<div class="property_description col-lg-8">
				<div class="description_content">
						<div class="description_heading"><?= $property->listing_title; ?> <span><b>AED <?= $property->price; ?></b></span></div>
						<div class="description_sub_heading"><?= $property->location->name; ?>, <?= $property->emirate->name; ?></div>
						<div class="tbl tbl_description">
						    <div class="cell">
						        Ref No. <b>:</b> <?= $property->reference_no; ?><br>
						        Property Type <b>:</b> <?= $property->category->name; ?><br>
						        BUA <b>:</b> <?= $property->bua; ?> Sq.Ft.
						    </div>
						</div>
						<div class="description_para">
							<?= $property->description; ?> 
						</div>
				
						<div class="description_para">
							Contact us today for your private viewing of this extraordinary property.
						</div>
				</div>	
			</div>
			<div class="submit_form col-lg-4 wow slideInRight">
				<form class="detail_form">
					<div class="form_heading">
						<h4>contact us</h4>
					</div>
					<div class="form_input">
						<div><input type="text" name="Name" placeholder="Full name"></div>
						<div><input type="Email" name="Mail" placeholder="Email"></div>
						<div><input type="text" name="Phone" placeholder="Your phone"></div>
						<div><input type="text" name="Nation" placeholder="Nationality"></div>
						<div><textarea cols="20">I am interested in the development that i saw on your website. Can you please contact me so we can discuss more details regarding this opportunity?
						</textarea></div>
						<div class="btn">SEND</div>
						<div class="detail_terms">
							<small>
								By submiting this form, you accept<br>
								our <span class="detail_promise">Terms of use</span> and <span class="detail_promise">Privacy policy</span>
							</small>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="property_location_container">
			property location
		</div>
		<div class="detail_map_container wow slideInUp">
			<img src="<?= $this->Url->build("/img/Detail_map.png",true); ?>">
		</div>
		<div class="detail_map_container wow slideInUp" id="googleMap">
			
		</div>
		<div class="similar_properties_container">
			similar properties
		</div>
		<div class="content_area" id="wrapper">
			<div class="row view_gird_list">
				<?php foreach($relatedProperties as $property): ?>
					<?= $this->element('Parts/properties',['property'=>$property,'colSize' => '3']); ?>
				<?php endforeach; ?>
							
			</div>								
		</div>
		<section class="subscribing">
			<div class="row margin-none">
				<div id="subcrib_txt_one" class="col-lg-6 col-md-6 col-sm-12 ">
					<div class="local-knowledge-head">LOCAL KNOWLEDGE</div>
					<div class="local-knowledge">Our team are all certified by the Real Estate<br>Regulatory Agency giving you peace if mind</div>
				</div>
				<div id="subcrib_img_one" class="col-lg-6 col-md-6 col-sm-12 wow slideInRight">
					<img src="<?= $this->Url->build("/img/subcribe_one.png",true); ?>">
				</div>
			</div>	
			<div class="row margin-none">
				<div id="subcrib_img_two" class="col-lg-6 col-md-6 col-sm-12 wow slideInLeft">
					<img src="<?= $this->Url->build("/img/subcribe_two.png",true); ?>">
					<div class="subcribe_img_txt">Get the latest news and happenings<br>on luxury real estate directly in your inbox</div>
				</div>
				<div id="subcrib_submition" class="col-lg-6 col-md-6 col-sm-12 ">
					<div class="email_input_area">
						<input class="btn btn-lg" name="email" id="email" type="email" placeholder="Enter your email" required>
					</div>
					<div class="checkbox_main">
						<input id="c1" class="chk" type="checkbox">
						<label for="c1">
							<span></span>
							I am a Broker/Agent
						</label>
					</div>
					<div id="subcrib_btn" ><button id="subcrib_btn_inner" class="btn btn-info btn-lg" type="submit">Subscribe</button></div>
				</div>
				<div class="promise">By subscribing, you are agreeing to our Privacy Policy. You can unsubscribe anytime<br>from our emails by clicking the "unsubscribe" link.</div>
			</div>
	

		</section>	
		<?php $this->start('script'); ?>
		<script type="text/javascript">

  		var map;
		$(window).load(function () {

		//$('.listings #middle .loading').fadeIn(400);

		latlngset = new google.maps.LatLng(<?php echo $property['latitude'] ?>, <?php echo $property['longitude'] ?>);


		var myOptions = {
			center: latlngset,
			zoom: 15,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			styles: [
				{
					"featureType": "water",
					"elementType": "geometry.fill",
					"stylers": [
						{
							"color": "#d6d5d6"
						},
						{
							"weight": 4
						}
					]
				},
				{
					"featureType": "water",
					"elementType": "labels.icon",
					"stylers": [
						{
							"color": "#110b3d"
						}
					]
				}
			]


		};

		map = new google.maps.Map(document.getElementById("googleMap"), myOptions);
		//map.disableScrollWheelZoom();

		//setMarkers(map,locations);


		var marker = new google.maps.Marker({
			map: map, title: 'Map', position: latlngset
		});
		map.setCenter(marker.getPosition());


		//var content = '<a href="http://hello.com">Loan Number</a>: ' + loan +  '<h3>' + "Address: " + add + '</h3>';

		var infowindow = new google.maps.InfoWindow()

		google.maps.event.addListener(marker, 'click', (function (marker, content, infowindow) {
			return function () {
				// infowindow.setContent(content);
				infowindow.open(map, marker);
			};
		})(marker, content, infowindow));


	});

</script>
<?php $this->end('script'); ?>

