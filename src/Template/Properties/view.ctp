
	<div class="path_bar">
			<div class="row">
				<div class="col-md-9 col-md-offset-3">
					<?php if($property->is_rental == 1) {
						$type = 'Rent';
						} else {
						$type = 'Sale';
							} 
							?>
						<div> <a href="<?= $this->Url->build("/",true); ?>"> Home </a> > <a href="<?= $this->Url->build("/properties/index/",true); ?>">Properties </a> > For <?php echo $type;?> > <?= $property->reference_no; ?> </div>
					
				</div>
			</div>
		</div>
		<?php if($property->location_id == 93) {?>
		<div id="carousel_home" class="carousel slide wow fadeIn detail_banner" data-ride="carousel">
				<div class="item active">
					<img src="<?= $this->Url->build("/img/The Square-4.jpg",true); ?>" alt="..." class="Square100">
				</div>
		</div>
		<?php } ?>
		<?php if($property->location_id == 94) {?>
		<div id="carousel_home" class="carousel slide wow fadeIn" data-ride="carousel">
				<div class="item active">
					<img src="<?= $this->Url->build("/img/One by Omniyat - 1.jpg",true); ?>" alt="...">
				</div>
		</div>		
				<?php } ?>
		<?php if($property->location_id == 95) {?>
		<div id="carousel_home" class="carousel slide wow fadeIn" data-ride="carousel">
			<div class="item active">
				<img src="<?= $this->Url->build("/img/The Binary-3.jpg",true); ?>" alt="...">
			</div>
		</div>
		<?php } ?>
		

				<div class="actionsBottom wow slideInLeft">
		    <div class="wrap clear">
				<ul>

				  <li style="width: 20%;">
				        <a href="#f1" class="icon_photos act click_slider popup" data-link="http://www.gulfsothebysrealty.com/buy/11-bedroom-villa-for-sale-in-palm-jumeirah-dubai-ER-S-17909" data-id="4446" data-image="/images/return_image_data/11-bedroom-villa-for-sale-in-palm-jumeirah-dubai-ER-S-17909-332747.jpg" data-sublocation="Signature Villas Frond D" data-location="Palm Jumeirah" data-emirate="Dubai" data-price="200,000,000 AED" data-size="40,000 Sq.Ft" data-reference="ER-S-17909">
				        <button class="btn" data-toggle="modal" data-target="#email_friend1">
				        <span>Photos</span>
				        </button>
				        </a>
					        <div id="inner_img" style="display: none;">
					        	<a id="f1" data-fancybox="images" class="fancybox" rel="group" href="<?= $this->Url->build("/photos/".$property->location_id."/".$property->unit_no."/1.JPG",true); ?>"></a>
					        	
							</div>

				    </li>
				    <li style="width: 20%;">
				        <a  href="#" class="icon_videos" data-video="7ilQ
				        80kShoc" data-link="http://www.gulfsothebysrealty.com/buy/11-bedroom-villa-for-sale-in-palm-jumeirah-dubai-ER-S-17909" data-id="4446" data-image="http://watermark.propspace.com/watermark5?c_id=1004&amp;l_id=1576257624833140&amp;aid=1452375&amp;id=14961283059737934&amp;wmt=1499605269&amp;image=30_05_2017-14_25_17-1004-62bf1edb36141f114521ec4bb4175579.jpg&amp;resize=true&amp;watermarkdis=true" data-sublocation="Signature Villas Frond D" data-location="Palm Jumeirah" data-emirate="Dubai" data-price="200,000,000 AED" data-size="40,000 Sq.Ft" data-reference="ER-S-17909">
				        <button class="btn" id="video_popup">
				        <span>Video</span></button>
				        </a>
				    </li>
				    <li style="width: 20%;"><a href="#" class="icon_share" onclick="shareProduct()">
				    	<button class="btn"><span>Share this</span></button></a>
				    </li>
				    <li style="width: 20%;">
				        <a class="icon_email" title="Email a friend" data-textarea="I found this amazing property on Gulf Sotheby’s International Realty website. Take a look, and let me know what you think. http://www.gulfsothebysrealty.com/buy/11-bedroom-villa-for-sale-in-palm-jumeirah-dubai-ER-S-17909">
				        <button class="btn" data-toggle="modal" data-target="#email_friend" ><span>Email to a friend</span></button></a>

				        <div id="email_friend" class="modal fade" role="dialog">
						  <div class="modal-dialog">

						    <!-- Modal content-->
						    <div class="modal-content" id="mail_popup">
						    	<div class="modal-header" id="mail_popup_head"> 
						    		<button type="button" class="close btn-danger" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								</div>
						     	<div class="modal-body" id="mail_popup_body">
							      	<form class="friends_mail_form">
							      		
										<div class="mail_form_input">
											<div><input type="text" name="Your Name" placeholder="Your name"></div>
											<div><input type="Email" name="Mail" placeholder="Your email"></div>
											<div><input type="text" name="Phone" placeholder="Friend's email"></div>
											<div><textarea cols="20">I found this amazing property on LIV’s International Realty website. Take a look, and let me know what you think. http://www.LIV.com/buy/11-bedroom-villa-for-sale-in-palm-jumeirah-dubai-ER-S-17909
											</textarea></div>
											<div class="btn">SEND</div>
										</div>
									</form>
						      	</div>
						    </div>
						  </div>
						</div>
				    </li>
				</ul>
		    </div>
		</div>
         <?php
            if($property->price == 0) { 
			    $price  = 'Price on Application' ; 
		    }else {
			    $price = 'AED '.number_format($property->price);
			    if($property->is_rental){
			         $price .= '<span>(Per annum)</span>';
			    }
			} 
        ?>
		<img id ="image_id" src="<?= $this->Url->build("/photos/".$property->location_id."/".$property->unit_no."/1.JPG",true); ?> " alt="..." style="display:none">
        <div class="mob_view_detail">
			<div class="mob_view_inner_detail">
				<div class="description_heading_mob"><?= $price; ?></div>
					<div class="description_sub_heading_mob">Signature villa, Palm Jumeirah</div>
					<div class="tbl tbl_description_mob">
					    <div class="cell">
					    	Palm Jumeirah, Dubai<br>
					        Ref No. <b>:</b> TT-S-2014<br>
					        Property Type <b>:</b> Villa<br>
					        BUA <b>:</b> 15,000 Sq.Ft.
					    </div>
					</div>
					<div class="mob_detail_btn1"><button class="btn make_an_offer_btn">make an offer</button></div>
					<div class="mob_detail_btn2"><button class="btn arrange_viewing_btn">arrange viewing</button></div>
			</div>
			<div class="mob_view_inner_features">
				<?= $property->description; ?>
			</div>
		</div>
		<div class="row description_area">
			<div class="property_description col-lg-8">
				<div class="description_content">
				       
						<div class="description_heading"><?= $property->listing_title; ?> <span><b><?= $price; ?></b></span></div>
						<div class="description_sub_heading"><?= $property->location->name; ?>, <?= $property->emirate->name; ?></div>
						<div class="tbl tbl_description">
						    <div class="cell">
						        Ref No. <b>:</b> <?= $property->reference_no; ?><br>
						        Property Type <b>:</b> <?= $property->category->name; ?><br>
						        BUA <b>:</b> <?= $property->bua; ?> Sq.Ft.
						    </div>
						</div>
						<div class="description_para">
							<?= $property->description; ?> 
						</div>
				
						<div class="description_para">
							Contact us today for your private viewing of this extraordinary property.
						</div>
				</div>	
			</div>
			<div class="submit_form col-lg-4 wow slideInRight">
				<?= $this->Form->create(null,['class'=>'detail_form','validate'=>true]); ?>
				
					<div class="form_heading">
						<h4>contact us</h4>
					</div>
					<?= $this->Flash->render() ?>
					<div class="form_input">
						<div><?= $this->Omniyat->input('first_name',['placeholder'=>'First Name','required'=>true]); ?></div>
						<div><?= $this->Omniyat->input('last_name',['placeholder'=>'Last Name','required'=>true]); ?></div>
						<div><?= $this->Omniyat->input('sender_email',['placeholder'=>'Email','required'=>true]); ?></div>
						
						<div class="phone_wrap"><?= $this->element('Parts/isd_code'); ?><?= $this->Omniyat->input('phone',['placeholder'=>'Your Phone','required'=>true]); ?></div>
						<div><?= $this->Omniyat->input('nation',['placeholder'=>'Nationality','required'=>true]); ?></div>
						<div><?= $this->Omniyat->input('interested',['type'=>'textarea','required'=>true,'value'=>'I am interested in the development that i saw on your website. Can you please contact me so we can discuss more details regarding this opportunity?']); ?></div>
						 <button class="btn submit" type="submit">SEND</button>
						<div class="detail_terms">
							<small>
								By submiting this form, you accept<br>
								our <span class="detail_promise">Terms of use</span> and <span class="detail_promise">Privacy policy</span>
							</small>
						</div>
					</div>
				<?= $this->Form->end(); ?>
			</div>
		</div>
		<div class="property_location_container">
			property location
		</div>
		<div class="detail_map_container wow slideInUp">
		<?php if($property->location_id == 93) {?>
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3607.4119454856927!2d55.35125076454103!3d25.290360084121847!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f5c8604757fad%3A0x15feb2de703b1db9!2sOmniyat+The+Square+Apartment+-+44th+St+-+Dubai+-+United+Arab+Emirates!5e0!3m2!1sen!2s!4v1508246958625" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
			<?php } ?>
			<?php if($property->location_id == 94) {?>
			<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7220.915863336419!2d55.261698!3d25.187775!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f69d72d8193ab%3A0xcf59cc34ff9b036f!2sOne+by+Omniyat+-+Dubai+-+United+Arab+Emirates!5e0!3m2!1sen!2s!4v1508246737500" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
			<?php } ?>
			<?php if($property->location_id == 95) {?>
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3610.474049334188!2d55.26438471453894!3d25.187231138286652!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f69d3bb4198df%3A0xa2db82f588c4c7fb!2sThe+Binary+by+Omniyat!5e0!3m2!1sen!2s!4v1508246863228" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
			<?php } ?>
		</div>
		<div class="detail_map_container wow slideInUp" id="googleMap">
			
		</div>
		<div class="similar_properties_container">
			similar properties
		</div>
		<div class="content_area" id="wrapper">
			<div class="row view_gird_list">
				<?php foreach($relatedProperties as $property): ?>
					<?= $this->element('Parts/properties',['property'=>$property,'colSize' => '3']); ?>
				<?php endforeach; ?>
							
			</div>								
		</div>
		<?php $this->start('script'); ?>
		<script type="text/javascript">
			var db, isfullscreen = false;
			function toggleFullScreen(){
				$("#image_id").css("display","block")
				db = document.getElementById("image_id");
				if(isfullscreen == false){
					if(db.requestFullScreen){
					    db.requestFullScreen();
					} else if(db.webkitRequestFullscreen){
					    db.webkitRequestFullscreen();
					} else if(db.mozRequestFullScreen){
					    db.mozRequestFullScreen();
					} else if(db.msRequestFullscreen){
					    db.msRequestFullscreen();
					} 
					isfullscreen = true;
				} 
			}
			$(document).keyup(function(e) {
  				$("#image_id").css("display","none");  // esc
			});
		</script>
		<?php $this->end('script'); ?>

