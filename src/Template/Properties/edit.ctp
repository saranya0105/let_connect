<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $property->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $property->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Properties'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Category'), ['controller' => 'Categories', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Emirates'), ['controller' => 'Emirates', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Emirate'), ['controller' => 'Emirates', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Locations'), ['controller' => 'Locations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Location'), ['controller' => 'Locations', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sub Locations'), ['controller' => 'SubLocations', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sub Location'), ['controller' => 'SubLocations', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Agents'), ['controller' => 'Agents', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Agent'), ['controller' => 'Agents', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Photos'), ['controller' => 'Photos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Photo'), ['controller' => 'Photos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Property Features'), ['controller' => 'PropertyFeatures', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Property Feature'), ['controller' => 'PropertyFeatures', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="properties form large-9 medium-8 columns content">
    <?= $this->Form->create($property) ?>
    <fieldset>
        <legend><?= __('Edit Property') ?></legend>
        <?php
            echo $this->Form->control('is_featured');
            echo $this->Form->control('reference_no');
            echo $this->Form->control('unit_no');
            echo $this->Form->control('type');
            echo $this->Form->control('street_no');
            echo $this->Form->control('floor');
            echo $this->Form->control('category_id', ['options' => $categories, 'empty' => true]);
            echo $this->Form->control('emirate_id', ['options' => $emirates, 'empty' => true]);
            echo $this->Form->control('location_id', ['options' => $locations, 'empty' => true]);
            echo $this->Form->control('sub_location_id', ['options' => $subLocations, 'empty' => true]);
            echo $this->Form->control('beds');
            echo $this->Form->control('baths');
            echo $this->Form->control('bua');
            echo $this->Form->control('plot');
            echo $this->Form->control('price');
            echo $this->Form->control('is_rental');
            echo $this->Form->control('rental_frequency');
            echo $this->Form->control('cheques');
            echo $this->Form->control('price_per_sq_ft');
            echo $this->Form->control('parking');
            echo $this->Form->control('listing_title');
            echo $this->Form->control('description');
            echo $this->Form->control('furnished');
            echo $this->Form->control('view');
            echo $this->Form->control('latitude');
            echo $this->Form->control('longitude');
            echo $this->Form->control('date_listed');
            echo $this->Form->control('last_updated');
            echo $this->Form->control('agent_id', ['options' => $agents, 'empty' => true]);
            echo $this->Form->control('crm_ref');
            echo $this->Form->control('link_ref');
            echo $this->Form->control('property_features._ids', ['options' => $propertyFeatures]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
