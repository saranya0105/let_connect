<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            Liv :
            <?= $this->fetch('title') ?>
        </title>
       <?= $this->Html->meta('icon') ?> 
        <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
        <?php  
            $css = ['bootstrap.min.css','animate.css','style.css','style_mob.css','jquery.fancybox.css','jquery.fancybox.min.css'];
        ?>
        <?= $this->Html->css($css) ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
</head>
<body>
   <?= $this->element('Parts/header') ?>
   

   <?= $this->fetch('content') ?>
  
    
	   <?= $this->element('Parts/footer') ?>
  
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

	 <?= $this->Html->script(array('bootstrap.min.js','plugin.js','script.js','wow.js','custom_script.js','custom.js','jquery.fancybox.min.js')) ?>
	 <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCxtqPs7a0KJANV74TnqevifxQcbqVSCCQ">
    </script>
    <script>
	      new WOW().init();
	</script>

     <?= $this->fetch('script') ?>
</body>
</html>
