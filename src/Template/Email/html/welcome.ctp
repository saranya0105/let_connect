<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>


<div align="center">
    <table border="0" cellspacing="0" cellpadding="0" width="680" style="width:510.0pt">
        <tbody>
            <tr>
                <td style="padding:0in 0in 0in 0in">
                    <div>
                        <div align="center">
                            <table border="0" cellspacing="0" cellpadding="0" width="680" style="width:510.0pt;background:#e3e3e3">
                                <tbody>
                                    <tr>
                                        <td style="padding:0in 0in 0in 0in">
                                            <div align="center">
                                                <table border="0" cellspacing="0" cellpadding="0" width="644" style="width:483.0pt;background:#e3e3e3">
                                                    <tbody>
                                                        <tr style="height:1.5pt">
                                                            <td width="2" style="width:1.5pt;background:#d8d8d8;padding:0in 0in 0in 0in;height:1.5pt"></td>
                                                            <td style="background:#d8d8d8;padding:0in 0in 0in 0in;height:1.5pt"></td>
                                                            <td width="2" style="width:1.5pt;background:#d8d8d8;padding:0in 0in 0in 0in;height:1.5pt"></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="2" style="width:1.5pt;background:#d8d8d8;padding:0in 0in 0in 0in"></td>
                                                            <td style="padding:0in 0in 0in 0in">
                                                                <table border="0" cellspacing="0" cellpadding="0" align="left" width="640" style="width:480.0pt;background:white">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="padding:37.5pt 30.0pt 37.5pt 30.0pt">
                                                                                <table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td style="padding:0in 0in 0in 0in"></td>
                                                                                            <td style="padding:0in 0in 0in 0in">
                                                                                                <p class="MsoNormal" align="right" style="text-align:right;line-height:13.5pt">
                                                                                                    <img width="198"  src="<?= $this->Url->build("/img/Omniyat-Exc-by-LIV-EN-T.png",true); ?>" align="left" hspace="12" alt="" class="CToWUd">
                                                                                                        <span style="font-size:9.0pt;color:#868683">Call us now: </span>
                                                                                                         
                                                                                                            <span style="font-size:9.0pt;color:#868683"></span>
                                                                                                            <span style="font-size:10.5pt;color:#4a4944">800 666</span>
                                                                                                            <span style="font-size:9.0pt;color:#868683"></span>
                                                                                                        </p>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="padding:0in 30.0pt 0in 30.0pt">
                                                                                        <table border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100.0%">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td valign="top" style="padding:0in 0in 0in 0in">
                                                                                                        <p class="MsoNormal" style="line-height:13.5pt">
                                                                                                            <b>
                                                                                                                <span style="font-size:13.5pt;color:black">Hi LIV's,</span>
                                                                                                            </b>
                                                                                                            <span style="font-size:9.0pt;color:black"></span>
                                                                                                        </p>
                                                                                                        <p class="MsoNormal" style="line-height:13.5pt">
                                                                                                            <span style="font-size:11.0pt;;color:#1f497d">&nbsp;</span>
                                                                                                        </p>
                                                                                                        
                                                                                                        <p style="margin-top:0in;line-height:13.5pt">
                                                                                                            <span style="font-size:11.0pt;">I would like to contact you!</span>
                                                                                                        </p>
                                                                                                         <p style="margin-top:0in;line-height:13.5pt">
                                                                                                            <span style="font-size:11.0pt;">Please see my details below and get in touch with me ASAP</span>
                                                                                                        </p>
                                                                                                    </td>
                                                                                                    <td width="16" valign="top" style="width:12.0pt;padding:0in 0in 0in 0in"></td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="height:7.5pt">
                                                                                    <td style="padding:0in 0in 0in 0in;height:7.5pt"></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="background:#efeeea;padding:15.0pt 30.0pt 3.75pt 30.0pt">
                                                                                        <table border="0" cellspacing="0" cellpadding="0" width="95%" style="width:95.22%">
                                                                                            <tbody>
                                                                                                <tr style="height:10.1pt">
                                                                                                    <td width="30%" style="width:30.38%;padding:0in 0in 6.75pt 0in;height:10.1pt">
                                                                                                        <p class="MsoNormal" style="line-height:13.5pt">
                                                                                                            <b>
                                                                                                                <span style="font-size:9.0pt;">Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</span>
                                                                                                            </b>
                                                                                                        </p>
                                                                                                    </td>
                                                                                                    <td width="69%" valign="top" style="width:69.62%;padding:0in 0in 6.75pt 0in;height:10.1pt">
                                                                                                        <p class="MsoNormal">
                                                                                                           
                                                                                                                <span style="font-size:10.0pt;;color:blue">
                                                                                                             <?= $data['first_name'] ?>&nbsp;<?= $data['last_name'] ?>
                                                                                                                </span>
                                                                                                            
                                                                                                        </p>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr style="height:10.1pt">
                                                                                                    <td width="30%" style="width:30.38%;padding:0in 0in 6.75pt 0in;height:10.1pt">
                                                                                                        <p class="MsoNormal" style="line-height:13.5pt">
                                                                                                            <b>
                                                                                                                <span style="font-size:9.0pt;">Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</span>
                                                                                                            </b>
                                                                                                        </p>
                                                                                                    </td>
                                                                                                    <td width="69%" valign="top" style="width:69.62%;padding:0in 0in 6.75pt 0in;height:10.1pt">
                                                                                                        <table border="0" cellspacing="0" cellpadding="0" width="190" style="width:142.5pt">
                                                                                                            <tbody>
                                                                                                                <tr style="height:15.0pt">
                                                                                                                    <td width="191" style="width:143.0pt;padding:0in 0in 0in 0in;height:15.0pt"><?= $data['sender_email'] ?></td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr style="height:10.1pt">
                                                                                                    <td width="30%" style="width:30.38%;padding:0in 0in 6.75pt 0in;height:10.1pt">
                                                                                                        <p class="MsoNormal" style="line-height:13.5pt">
                                                                                                            <b>
                                                                                                                <span style="font-size:9.0pt;">Phone&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</span>
                                                                                                            </b>
                                                                                                        </p>
                                                                                                    </td>
                                                                                                    <td width="69%" valign="top" style="width:69.62%;padding:0in 0in 6.75pt 0in;height:10.1pt">
                                                                                                        <p class="MsoNormal">
                                                                                                            
                                                                                                                <span style="font-size:10.0pt;;color:blue">
                                                                                                                    <?= $data['phone'] ?>
                                                                                                                </span>
                                                                                                            
                                                                                                        </p>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                 
                                                                                               
                                                                                                <tr style="height:10.1pt">
                                                                                                    <td width="30%" style="width:30.38%;padding:0in 0in 6.75pt 0in;height:10.1pt">
                                                                                                        <p class="MsoNormal" style="line-height:13.5pt">
                                                                                                            <b>
                                                                                                                <span style="font-size:9.0pt;">Message &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :</span>
                                                                                                            </b>
                                                                                                        </p>
                                                                                                    </td>
                                                                                                    <td width="69%" valign="top" style="width:69.62%;padding:0in 0in 6.75pt 0in;height:10.1pt">
                                                                                                        <p class="MsoNormal">
                                                                                                           
                                                                                                                <span style="font-size:10.0pt;;color:blue">
                                                                                                                    <?= $data['interested'] ?>
                                                                                                                </span>
                                                                                                            
                                                                                                        </p>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="height:7.5pt">
                                                                                    <td style="padding:0in 0in 0in 0in;height:7.5pt"></td>
                                                                                </tr>
                                                                                 
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td width="2" style="width:1.5pt;background:#d8d8d8;padding:0in 0in 0in 0in"></td>
                                                                    </tr>
                                                                    <tr style="height:1.5pt">
                                                                        <td width="2" style="width:1.5pt;background:#d8d8d8;padding:0in 0in 0in 0in;height:1.5pt"></td>
                                                                        <td style="background:#d8d8d8;padding:0in 0in 0in 0in;height:1.5pt"></td>
                                                                        <td width="2" style="width:1.5pt;background:#d8d8d8;padding:0in 0in 0in 0in;height:1.5pt"></td>
                                                                    </tr>
                                                                    <tr style="height:105.0pt">
                                                                        <td style="padding:0in 0in 0in 0in;height:105.0pt"></td>
                                                                        <td style="padding:0in 0in 0in 0in;height:105.0pt">
                                                                   
                                                                                            <p align="center" style="margin-bottom:12.0pt;text-align:center;line-height:10.5pt">
                                                                                                <span style="font-size:8.5pt;color:#404040">© OMNIYAT EXCLUSIVELY BY LIV 2017. All Rights Reserved.</span>
                                                                                            </p>
                                                                                        </td>
                                                                                        <td style="padding:0in 0in 0in 0in;height:105.0pt"></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
