<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>

<table>
<tr><th>First Name : </th><th><?= $data['first_name'] ?></th></tr>
<tr><th>Last Name : </th><th><?= $data['last_name'] ?></th></tr>
<tr><th>Email: </th><th><?= $data['sender_email'] ?></th></tr>
<tr><th>Phone: </th><th><?= $data['phone'] ?></th></tr>
<tr><th>Nationality: </th><th><?= $data['nation'] ?></th></tr>
</br>
<tr><th><?= $data['interested'] ?></th></tr>
</table>
<?php 
$content = explode("\n", $content);

foreach ($content as $line) :
    echo '<p> ' . $line . "</p>\n";
endforeach;
