<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>



<div align="center">
<table border="0" cellspacing="0" cellpadding="0" width="650" align="center" style="margin:30px auto 30px auto; background:#FFFFFF;">
    <tr>
        <td style="color:#666; font-size:12px;">
        
            <table width="100%" border="0" cellspacing="0" cellpadding="18" style="background:#fff">
                <tr>
                    <td align="left"><a href="#" title="Visit Website"><img width="198" src="<?= $this->Url->build("/img/Omniyat-Exc-by-LIV-EN-T.png",true); ?>" alt="Omniyat Exclusively By LIV " border="0" /></a></td>
                    <td align="right"><b style="font-size:18px;color:#000;">800 666</b></td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#EFEFEF;">
                <tr>
                    <td align="left" style="padding:40px 40px;">
                        <h1 style="font-size:20px; color:#666666;margin:0; padding:0 0 15px 0; font-weight:normal;">Hi  <?= $data['first_name'] ?>&nbsp;<?= $data['last_name'] ?></h1>
                        <h1 style="font-size:20px; color:#666666;margin:0; padding:0 0 15px 0; font-weight:normal;">YOUR DETAILS HAVE BEEN DELIVERED</h1>
                        <p style="font-size:15px; color:#666;margin:0; padding:0; line-height:160%;">Thank you for registering your interest in LIV. Your details have been sent to our property consultant and they will contact you shortly to help you with your requirements.</p>
                    </td>
                </tr>
            </table>
         
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#fff">
                <tr>
                    <td align="center" style="padding:30px 25px; font-size:16px; color:#000; line-height:150%">
                        <p style="color:#999999; margin:0; padding:0 0 20px 0; text-align:center;"><a href="#" title="Visit Website"><img width="198" src="<?= $this->Url->build("/img/Omniyat-Exc-by-LIV-EN-T.png",true); ?>" alt="Omniyat Exclusively by LIV " border="0" width="190" /></a></p>
                        <p style="margin:0; padding:0;">FOR MORE INFORMATION ABOUT OUR LUXURY PROPERTY(Sales / Rentals), CONTACT:
                        <br> <a href="mailto:sales@omniyat.com" style="color:#999; text-decoration:none;"> sales@omniyat.com</a> OR UAE 800 666 / International +971 800 666<br><a href="http://www.numerique.ae/liv/" target="_blank" style="color:#999; text-decoration:none;">WWW.LIV.COM</a></p>
                    </td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background:#333333">
                <tr>
                    <td align="center" style="padding:25px 25px; font-size:11px; color:#666; line-height:150%">
                        <p style="color:#999999; margin:0; padding:0 0 12px 0;">© OMNIYAT EXCLUSIVELY BY LIV 2017. All Rights Reserved.</p>
                    
               

                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</div>

