<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Locations Model
 *
 * @property \App\Model\Table\EmiratesTable|\Cake\ORM\Association\BelongsTo $Emirates
 * @property \App\Model\Table\PropertiesTable|\Cake\ORM\Association\HasMany $Properties
 * @property \App\Model\Table\SubLocationsTable|\Cake\ORM\Association\HasMany $SubLocations
 *
 * @method \App\Model\Entity\Location get($primaryKey, $options = [])
 * @method \App\Model\Entity\Location newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Location[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Location|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Location patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Location[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Location findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class LocationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('locations');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Emirates', [
            'foreignKey' => 'emirate_id'
        ]);
        $this->hasMany('Properties', [
            'foreignKey' => 'location_id'
        ]);
        $this->hasMany('SubLocations', [
            'foreignKey' => 'location_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->allowEmpty('name');

        $validator
            ->scalar('alt_names')
            ->allowEmpty('alt_names');

        $validator
            ->scalar('slug')
            ->allowEmpty('slug');

        $validator
            ->integer('property_count')
            ->requirePresence('property_count', 'create')
            ->notEmpty('property_count');

        $validator
            ->scalar('latitude')
            ->allowEmpty('latitude');
            
        $validator
            ->scalar('longitude')
            ->allowEmpty('longitude');        

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['emirate_id'], 'Emirates'));

        return $rules;
    }
}
