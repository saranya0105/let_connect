<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PropertyFeatures Model
 *
 * @property \App\Model\Table\PropertiesTable|\Cake\ORM\Association\BelongsToMany $Properties
 *
 * @method \App\Model\Entity\PropertyFeature get($primaryKey, $options = [])
 * @method \App\Model\Entity\PropertyFeature newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PropertyFeature[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PropertyFeature|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PropertyFeature patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PropertyFeature[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PropertyFeature findOrCreate($search, callable $callback = null, $options = [])
 */
class PropertyFeaturesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('property_features');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsToMany('Properties', [
            'foreignKey' => 'property_feature_id',
            'targetForeignKey' => 'property_id',
            'joinTable' => 'properties_property_features'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->allowEmpty('name');

        $validator
            ->boolean('is_amenity')
            ->allowEmpty('is_amenity');

        return $validator;
    }
}
