<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Properties Model
 *
 * @property \App\Model\Table\CategoriesTable|\Cake\ORM\Association\BelongsTo $Categories
 * @property \App\Model\Table\EmiratesTable|\Cake\ORM\Association\BelongsTo $Emirates
 * @property \App\Model\Table\LocationsTable|\Cake\ORM\Association\BelongsTo $Locations
 * @property \App\Model\Table\SubLocationsTable|\Cake\ORM\Association\BelongsTo $SubLocations
 * @property \App\Model\Table\AgentsTable|\Cake\ORM\Association\BelongsTo $Agents
 * @property \App\Model\Table\PhotosTable|\Cake\ORM\Association\HasMany $Photos
 * @property \App\Model\Table\PropertyFeaturesTable|\Cake\ORM\Association\BelongsToMany $PropertyFeatures
 *
 * @method \App\Model\Entity\Property get($primaryKey, $options = [])
 * @method \App\Model\Entity\Property newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Property[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Property|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Property patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Property[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Property findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \Cake\ORM\Behavior\CounterCacheBehavior
 */
class PropertiesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('properties');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('CounterCache', ['Categories' => ['property_count'], 'Emirates' => ['property_count'], 'Locations' => ['property_count'], 'SubLocations' => ['property_count'], 'Agents' => ['property_count']]);

        $this->belongsTo('Categories', [
            'foreignKey' => 'category_id'
        ]);
        $this->belongsTo('Emirates', [
            'foreignKey' => 'emirate_id'
        ]);
        $this->belongsTo('Locations', [
            'foreignKey' => 'location_id'
        ]);
        $this->belongsTo('SubLocations', [
            'foreignKey' => 'sub_location_id'
        ]);
        $this->belongsTo('Agents', [
            'foreignKey' => 'agent_id'
        ]);
        $this->hasMany('Photos', [
            'foreignKey' => 'property_id'
        ]);
        $this->belongsToMany('PropertyFeatures', [
            'foreignKey' => 'property_id',
            'targetForeignKey' => 'property_feature_id',
            'joinTable' => 'properties_property_features'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->boolean('is_featured')
            ->requirePresence('is_featured', 'create')
            ->notEmpty('is_featured');

        $validator
            ->scalar('reference_no')
            ->allowEmpty('reference_no')
            ->add('reference_no', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('unit_no')
            ->allowEmpty('unit_no');

        $validator
            ->scalar('type')
            ->allowEmpty('type');

        $validator
            ->scalar('street_no')
            ->allowEmpty('street_no');

        $validator
            ->scalar('floor')
            ->allowEmpty('floor');

        $validator
            ->scalar('beds')
            ->allowEmpty('beds');

        $validator
            ->scalar('baths')
            ->allowEmpty('baths');

        $validator
            ->scalar('bua')
            ->allowEmpty('bua');

        $validator
            ->scalar('plot')
            ->allowEmpty('plot');

        $validator
            ->scalar('price')
            ->allowEmpty('price');

        $validator
            ->boolean('is_rental')
            ->allowEmpty('is_rental');

        $validator
            ->scalar('rental_frequency')
            ->allowEmpty('rental_frequency');

        $validator
            ->allowEmpty('cheques');

        $validator
            ->decimal('price_per_sq_ft')
            ->allowEmpty('price_per_sq_ft');

        $validator
            ->scalar('parking')
            ->allowEmpty('parking');

        $validator
            ->scalar('listing_title')
            ->allowEmpty('listing_title');

        $validator
            ->scalar('description')
            ->allowEmpty('description');

        $validator
            ->scalar('furnished')
            ->allowEmpty('furnished');

        $validator
            ->scalar('view')
            ->allowEmpty('view');

        $validator
            ->scalar('latitude')
            ->allowEmpty('latitude');

        $validator
            ->scalar('longitude')
            ->allowEmpty('longitude');

        $validator
            ->scalar('date_listed')
            ->allowEmpty('date_listed');

        $validator
            ->scalar('last_updated')
            ->allowEmpty('last_updated');

        $validator
            ->scalar('crm_ref')
            ->allowEmpty('crm_ref');

        $validator
            ->scalar('link_ref')
            ->allowEmpty('link_ref');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['reference_no']));
        $rules->add($rules->existsIn(['category_id'], 'Categories'));
        $rules->add($rules->existsIn(['emirate_id'], 'Emirates'));
        $rules->add($rules->existsIn(['location_id'], 'Locations'));
        $rules->add($rules->existsIn(['sub_location_id'], 'SubLocations'));
        $rules->add($rules->existsIn(['agent_id'], 'Agents'));

        return $rules;
    }
}
