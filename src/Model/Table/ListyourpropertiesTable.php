<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Listyourproperties Model
 *
 * @method \App\Model\Entity\Listyourproperty get($primaryKey, $options = [])
 * @method \App\Model\Entity\Listyourproperty newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Listyourproperty[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Listyourproperty|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Listyourproperty patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Listyourproperty[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Listyourproperty findOrCreate($search, callable $callback = null, $options = [])
 */
class ListyourpropertiesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('listyourproperties');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('service')
            ->requirePresence('service', 'create')
            ->notEmpty('service');

        $validator
            ->scalar('name')
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('number')
            ->requirePresence('number', 'create')
            ->notEmpty('number');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->scalar('country')
            ->requirePresence('country', 'create')
            ->notEmpty('country');

        $validator
            ->scalar('property_lcoatoon')
            ->requirePresence('property_lcoatoon', 'create')
            ->notEmpty('property_lcoatoon');

        $validator
            ->scalar('property_type')
            ->requirePresence('property_type', 'create')
            ->notEmpty('property_type');

        $validator
            ->scalar('property_name')
            ->requirePresence('property_name', 'create')
            ->notEmpty('property_name');

        $validator
            ->scalar('bed')
            ->requirePresence('bed', 'create')
            ->notEmpty('bed');

        $validator
            ->scalar('vacant')
            ->requirePresence('vacant', 'create')
            ->notEmpty('vacant');

        $validator
            ->scalar('vacant_date')
            ->requirePresence('vacant_date', 'create')
            ->notEmpty('vacant_date');

        $validator
            ->scalar('title_deed')
            ->allowEmpty('title_deed');

        $validator
            ->scalar('passport_copy')
            ->allowEmpty('passport_copy');

        $validator
            ->scalar('additional_iformation')
            ->requirePresence('additional_iformation', 'create')
            ->notEmpty('additional_iformation');

        $validator
            ->dateTime('date')
            ->requirePresence('date', 'create')
            ->notEmpty('date');

        $validator
            ->scalar('ip_address')
            ->allowEmpty('ip_address');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
}
