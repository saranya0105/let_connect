<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PropertiesPropertyFeatures Model
 *
 * @property \App\Model\Table\PropertiesTable|\Cake\ORM\Association\BelongsTo $Properties
 * @property \App\Model\Table\PropertyFeaturesTable|\Cake\ORM\Association\BelongsTo $PropertyFeatures
 *
 * @method \App\Model\Entity\PropertiesPropertyFeature get($primaryKey, $options = [])
 * @method \App\Model\Entity\PropertiesPropertyFeature newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PropertiesPropertyFeature[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PropertiesPropertyFeature|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PropertiesPropertyFeature patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PropertiesPropertyFeature[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PropertiesPropertyFeature findOrCreate($search, callable $callback = null, $options = [])
 */
class PropertiesPropertyFeaturesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('properties_property_features');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Properties', [
            'foreignKey' => 'property_id'
        ]);
        $this->belongsTo('PropertyFeatures', [
            'foreignKey' => 'property_feature_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['property_id'], 'Properties'));
        $rules->add($rules->existsIn(['property_feature_id'], 'PropertyFeatures'));

        return $rules;
    }
}
