<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CurrencyConverters Model
 *
 * @method \App\Model\Entity\CurrencyConverter get($primaryKey, $options = [])
 * @method \App\Model\Entity\CurrencyConverter newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CurrencyConverter[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CurrencyConverter|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CurrencyConverter patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CurrencyConverter[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CurrencyConverter findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CurrencyConvertersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('currency_converters');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('from')
            ->requirePresence('from', 'create')
            ->notEmpty('from');

        $validator
            ->scalar('to')
            ->requirePresence('to', 'create')
            ->notEmpty('to');

        $validator
            ->scalar('rates')
            ->requirePresence('rates', 'create')
            ->notEmpty('rates');

        return $validator;
    }
}
