<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Fortheunique Model
 *
 * @method \App\Model\Entity\Fortheunique get($primaryKey, $options = [])
 * @method \App\Model\Entity\Fortheunique newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Fortheunique[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Fortheunique|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Fortheunique patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Fortheunique[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Fortheunique findOrCreate($search, callable $callback = null, $options = [])
 */
class FortheuniqueTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('fortheunique');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('fullname')
            ->requirePresence('fullname', 'create')
            ->notEmpty('fullname');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->scalar('phone')
            ->allowEmpty('phone');

        $validator
            ->dateTime('DATE')
            ->requirePresence('DATE', 'create')
            ->notEmpty('DATE');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
}
