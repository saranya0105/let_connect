<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Emirates Model
 *
 * @property \App\Model\Table\LocationsTable|\Cake\ORM\Association\HasMany $Locations
 * @property \App\Model\Table\PropertiesTable|\Cake\ORM\Association\HasMany $Properties
 *
 * @method \App\Model\Entity\Emirate get($primaryKey, $options = [])
 * @method \App\Model\Entity\Emirate newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Emirate[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Emirate|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Emirate patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Emirate[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Emirate findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class EmiratesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('emirates');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Locations', [
            'foreignKey' => 'emirate_id'
        ]);
        $this->hasMany('Properties', [
            'foreignKey' => 'emirate_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->allowEmpty('name');

        $validator
            ->scalar('slug')
            ->allowEmpty('slug');

        $validator
            ->integer('property_count')
            ->requirePresence('property_count', 'create')
            ->notEmpty('property_count');

        return $validator;
    }
}
