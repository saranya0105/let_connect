<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Listyourproperty Entity
 *
 * @property int $id
 * @property string $service
 * @property string $name
 * @property string $number
 * @property string $email
 * @property string $country
 * @property string $property_lcoatoon
 * @property string $property_type
 * @property string $property_name
 * @property string $bed
 * @property string $vacant
 * @property string $vacant_date
 * @property string $title_deed
 * @property string $passport_copy
 * @property string $additional_iformation
 * @property \Cake\I18n\FrozenTime $date
 * @property string $ip_address
 */
class Listyourproperty extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
