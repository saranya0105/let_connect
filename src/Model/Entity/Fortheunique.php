<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Fortheunique Entity
 *
 * @property int $id
 * @property string $fullname
 * @property string $email
 * @property string $phone
 * @property \Cake\I18n\FrozenTime $DATE
 */
class Fortheunique extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
