<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Property Entity
 *
 * @property int $id
 * @property bool $is_featured
 * @property string $reference_no
 * @property string $unit_no
 * @property string $type
 * @property string $street_no
 * @property string $floor
 * @property int $category_id
 * @property int $emirate_id
 * @property int $location_id
 * @property int $sub_location_id
 * @property string $beds
 * @property string $baths
 * @property string $bua
 * @property string $plot
 * @property string $price
 * @property bool $is_rental
 * @property string $rental_frequency
 * @property int $cheques
 * @property float $price_per_sq_ft
 * @property string $parking
 * @property string $listing_title
 * @property string $description
 * @property string $furnished
 * @property string $view
 * @property string $latitude
 * @property string $longitude
 * @property string $date_listed
 * @property string $last_updated
 * @property int $agent_id
 * @property string $crm_ref
 * @property string $link_ref
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\Emirate $emirate
 * @property \App\Model\Entity\Location $location
 * @property \App\Model\Entity\SubLocation $sub_location
 * @property \App\Model\Entity\Agent $agent
 * @property \App\Model\Entity\Photo[] $photos
 * @property \App\Model\Entity\PropertyFeature[] $property_features
 */
class Property extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
