<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Career Entity
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $location
 * @property string $position
 * @property string $resume
 * @property string $skills
 * @property string $experience
 * @property \Cake\I18n\FrozenTime $date
 * @property string $ip_address
 */
class Career extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
