<?php

namespace App\View\Helper;


use Cake\View\Helper;
use Cake\Core\Configure;

class OmniyatHelper extends Helper {



	public $helpers = [
		'Form',
        // 'Html', 
         'Url',
        // 'Time',
        // 'Text',
    ] ;

    public function displayTimeByNum($number,$format = 'ga'){
    	return date($format, strtotime("$number:00"));
    }

	public function input($fieldName, array $options = []){


		$options['label'] = false;
		$options['templates'] = 'app_form';
		$options['class'] = isset($options['class']) ? $options['class'] : 'inpt';

		return $this->Form->input($fieldName,$options);
	}

	
	                      public function getPriceList($isRent,$type = 'min'){
		$priceList = array();
		if($isRent){
			if($type == 'min'){
				$priceList[40000] = '40000';
				$priceList[50000] = '50000';
				$priceList[60000] = '60000';
				$priceList[70000] = '70000';
				$priceList[80000] = '80000';
				$priceList[90000] = '90000';
				$priceList[100000] = '100000';
				$priceList[125000] = '125000';
				$priceList[150000] = '150000';
				$priceList[200000] = '200000';
				$priceList[350000] = '350000';
				$priceList[400000] = '400000';
                                $priceList[500000] = '500000';
			}elseif ($type == 'max') {
				//$priceList[40000] = '40000';
				$priceList[50000] = '50000';
				$priceList[60000] = '60000';
				$priceList[70000] = '70000';
				$priceList[80000] = '80000';
				$priceList[900000] = '900000';
				$priceList[100000] = '100000';
				$priceList[125000] = '125000';
				$priceList[150000] = '150000';
				$priceList[200000] = '200000';
				$priceList[350000] = '350000';
				$priceList[400000] = '400000';
				$priceList[500000] = '500000';
				$priceList[600000] = '600000';
				$priceList[700000] = '700000';
				$priceList[800000] = '800000';
				$priceList[900000] = '900000';
				$priceList[1000000] = '1000000';
				$priceList[1250000] = '1250000';
				$priceList[1500000] = '1500000';
				$priceList[2000000] = '2000000';
			}
		}else{
			if($type == 'min'){
				$priceList[500000] = '500000';
				$priceList[600000] = '600000';
				$priceList[700000] = '700000';
				$priceList[800000] = '800000';
				$priceList[900000] = '900000';
				$priceList[1000000] = '1000000';
				$priceList[1500000] = '1500000';
				$priceList[2000000] = '2000000';
				$priceList[2500000] = '2500000';
				$priceList[3000000] = '3000000';
				$priceList[3500000] = '3500000';
				$priceList[4000000] = '4000000';
				$priceList[4500000] = '4500000';
				$priceList[5000000] = '5000000';
				$priceList[5500000] = '5500000';
				$priceList[6000000] = '6000000';
                                $priceList[6500000] = '6000000';
                                $priceList[7000000] = '7000000';
				$priceList[7500000] = '7500000';
				$priceList[8000000] = '8000000';
                                $priceList[8500000] = '8500000';
                                $priceList[9000000] = '9000000';
				$priceList[9500000] = '9500000';
				$priceList[10000000] = '10000000';
                                $priceList[10500000] = '10500000';
				$priceList[11000000] = '11m';
				$priceList[12000000] = '12m';
				$priceList[13000000] = '13m';
				$priceList[14000000] = '14m';
				$priceList[15000000] = '15m';
				$priceList[16000000] = '16m';
				$priceList[17000000] = '17m';
				$priceList[18000000] = '18m';
				$priceList[19000000] = '19m';
				$priceList[20000000] = '20m';
				$priceList[25000000] = '25m';

			}elseif ($type == 'max') {
				$priceList[1000000] = '1000000';
				$priceList[1500000] = '1500000';
				$priceList[2000000] = '2000000';
				$priceList[2500000] = '2500000';
				$priceList[3000000] = '3000000';
				$priceList[3500000] = '3500000';
				$priceList[4000000] = '4000000';
				$priceList[4500000] = '4500000';
				$priceList[5000000] = '5000000';
				$priceList[5500000] = '5500000';
				$priceList[6000000] = '6000000';
                                $priceList[6500000] = '6500000';
                                $priceList[7000000] = '7000000';
				$priceList[7500000] = '7500000';
				$priceList[8000000] = '8000000';
                                $priceList[8500000] = '8500000';
                                $priceList[9000000] = '9000000';
				$priceList[9500000] = '9500000';
				$priceList[10000000] = '10000000';
                                $priceList[10500000] = '10500000';
				$priceList[11000000] = '11m';
				$priceList[12000000] = '12m';
				$priceList[13000000] = '13m';
				$priceList[14000000] = '14m';
				$priceList[15000000] = '15m';
				$priceList[16000000] = '16m';
				$priceList[17000000] = '17m';
				$priceList[18000000] = '18m';
				$priceList[19000000] = '19m';
				$priceList[20000000] = '20m';
				$priceList[25000000] = '25m';
				$priceList[30000000] = '30m';
				$priceList[40000000] = '40m';
				$priceList[50000000] = '50m';
				$priceList[60000000] = '60m';
				$priceList[70000000] = '70m';
				$priceList[100000000] = '100m';
				$priceList[125000000] = '125m';
				$priceList[150000000] = '150m';
				$priceList[200000000] = '200m';
				$priceList[300000000] = '300m';
				$priceList[400000000] = '400m';
				$priceList[500000000] = '500m';
			}
		}
		return $priceList;
	}



}