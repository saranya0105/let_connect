<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Mailer\Email;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function display(...$path)
    {
        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        if (in_array('..', $path, true) || in_array('.', $path, true)) {
            throw new ForbiddenException();
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        $this->set(compact('page', 'subpage'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
        $this->loadModel('Categories');
        $this->loadModel('Properties');
        $featuredProperties = $this->Properties->find('all',['conditions'=>['is_featured' => 1]]);
        $categories = $this->Categories->find('list', ['limit' => 200]);
        $this->set('categories', $categories);
        $this->set('featuredProperties', $featuredProperties);
            if($path[0] == 'contactus')
            {
                if ($this->request->is('post')){
                    $this->loadModel('EmailData');
                    $emaildata = $this->EmailData->newEntity();
                    $this->request->data['phone'] = $this->request->data['ph_ext'].' '.$this->request->data['phone'];
                   
                    $emaildata = $this->EmailData->patchEntity($emaildata, $this->request->data);
                    $savedData  = $this->EmailData->save($emaildata);
                    $email = new Email();
                    $email
                    ->viewVars(['data'=>$this->request->data])
                    ->template('welcome', 'fancy')
                    ->emailFormat('html')
                    ->subject('Contact Us')
                    ->to(array('bhanuprakash.kolluru@liv.ae','balaji@numerique360.com'))
                    ->from('rilwan@numerique.ae')
                    ->send();
                    $reply_email = new Email();
                    $reply_email
                    ->viewVars(['data'=>$this->request->data])
                    ->template('customer_reply', 'fancy')
                    ->emailFormat('html')
                    ->subject('Thanks for contacting LIV')
                    ->to($this->request->data['sender_email'])
                    ->from('rilwan@numerique.ae')
                    ->send();

                    $this->Flash->success(__('Email sent successfully.'));
                    return $this->redirect(['action' => 'contactus']);

            }  
        }
        
    }

    public function about(){

    }

    public function gallery(){
        
    }

    public function home(){
        $this->loadModel('Locations');
        $locations = $this->Locations->find('list', ['limit' => 200]);
        $this->set('locations', $locations);
        $this->loadModel('Categories');
        $this->loadModel('Properties');
        $featuredProperties = $this->Properties->find('all',['conditions'=>['is_featured' => 1],'contain' => ['Emirates', 'Locations']]);

        $categories = $this->Categories->find('list', ['limit' => 200]);
        $this->set('categories', $categories);
        $this->set('featuredProperties', $featuredProperties);


       
    }

    public function project(){

    }

    public function contactus(){

    }
}
