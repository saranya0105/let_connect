<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;

/**
 * Properties Contro ller
 *
 * @property \App\Model\Table\PropertiesTable $Properties
 *
 * @method \App\Model\Entity\Property[] paginate($object = null, array $settings = [])
 */
class PropertiesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index($sale='')
    {

        $filterGet = $_GET;
        $habtmConditions = [];
        $conditions = [];
        if(!empty($filterGet)) {
            //pr($filterGet); exit;
            $habtmFilters = [
                'feature_id'       => 'PropertyFeatures',
                ];

            foreach($filterGet as $filterKey => $filter){
                if($filter){
                    if(!isset($habtmFilters[$filterKey])){
                        $conditions[$filterKey] = $filter;
                    }else{
                        $habtmConditions[$habtmFilters[$filterKey]] = $filter;
                        $filterGet[$filterKey] = explode(',', $filter);
                    }
                }
               
            }
            
            if(isset($conditions['price_min'])){
                $conditions['price >='] = $conditions['price_min'];
                unset($conditions['price_min']);
            }
             if(isset($conditions['price_max'])){
                $conditions['price <='] = $conditions['price_max'];
                unset($conditions['price_max']);
            }
            if(isset($conditions['bedroom'])){
                $conditions['beds ='] = $conditions['bedroom'];
                unset($conditions['bedroom']);
            }
            if(isset($conditions['bathroom'])){
                $conditions['baths ='] = $conditions['bathroom'];
                unset($conditions['bathroom']);
            }
            if(isset($conditions['carparking'])){
                $conditions['parking ='] = $conditions['carparking'];
                unset($conditions['carparking']);
            }
             if(isset($conditions['location_id'])){
                $conditions['Properties.location_id ='] = $conditions['location_id'];
                unset($conditions['location_id']);
            }
            if(isset($conditions['type'])){
                $conditions['type ='] = $conditions['type'];
                unset($conditions['type']);
            }

            if($sale == 'Rent'){
                $conditions['is_rental ='] = 1;
            }else if($sale == 'Sale'){
                 $conditions['is_rental = '] = 0;
            }
            if(isset($conditions['page'])){
                unset($conditions['page']);
            }
            if(isset($conditions['sort'])){
                unset($conditions['sort']);
            }
            if(isset($conditions['direction'])){
                unset($conditions['direction']);
            }
           
            //pr($conditions); exit;
            $this->request->data = $filterGet;
        }
         if($sale == 'Rent'){
                $conditions['is_rental'] = 1;
            } else if($sale == 'Sale'){
                 $conditions['is_rental'] = 0;
            }
        //pr($conditions); exit;
        $this->paginate = [
            'contain' => ['Categories', 'Emirates', 'Locations', 'SubLocations', 'Agents','Photos'],
            'conditions' => $conditions,
        ];
        $proObj =  $this->Properties->find()->group(['Properties.id']);      

        if(!empty($habtmConditions)){
            
            foreach($habtmConditions as $model => $value){
                //pr($habtmCondition); exit;

                $proObj->matching($model, function(\Cake\ORM\Query $q) use ($model,$value) {
                        return $q->where([
                            $model.'.id IN ('.$value.')'
                        ]);
                    });
            }
        }
        //pr($proObj); exit;
        $properties = $this->paginate($proObj);
       // $properties = $this->paginate($this->Properties);
        $categories = $this->Properties->Categories->find('list', ['limit' => 200]);
        $propertyFeatures = $this->Properties->PropertyFeatures->find('list', ['limit' => 200]);
        $features = array();
        foreach ( $propertyFeatures as $feature ) {
              $features[] = array(
                'name' => $feature
            );
        }
        $this->set('featurecount',count($features));
        $this->set('propertyFeatures', $propertyFeatures);
        $this->set('categories', $categories);
		//echo count($properties);exit;
		////pr($properties);exit;
        $this->set(compact('properties'));
        $this->set('_serialize', ['properties']);
        $this->set('type',$sale);
    }

    /*
     * View method
     *
     * @param string|null $id Property id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        if ($this->request->is('post')){
           $email = new Email();
           $email
                ->viewVars(['data'=>$this->request->data])
                ->template('welcome', 'fancy')
                ->emailFormat('html')
                ->subject('Contact Us')
                ->to('saranya0105@gmail.com')
                ->from('rilwan@numerique.ae')
                ->send();
                $this->Flash->success(__('Email sent successfully.'));
                return $this->redirect(['action' => 'view',$id]);

        }
        $property = $this->Properties->get($id, [
            'contain' => ['Categories', 'Emirates', 'Locations', 'SubLocations', 'Agents', 'PropertyFeatures', 'Photos']
        ]);
        $this->paginate = [
            'contain' => ['Categories', 'Emirates', 'Locations', 'SubLocations', 'Agents','Photos']
        ];
        $relatedproperties = $this->Properties->find('all',['limit'=>8,'contain' => ['Categories', 'Emirates', 'Locations', 'SubLocations', 'Agents','Photos']]);
       
        $this->set('property', $property);
        $this->set('relatedProperties', $relatedproperties);
        $this->set('_serialize', ['property']);
        

    }

    public function mailtest(){
        $email = new Email('default');
        $email->from(['rilwan@numerique.ae' => 'My Site'])
        ->to('saranya0105@gmail.com')
        ->subject('About')
        ->send('My message');exit;
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $property = $this->Properties->newEntity();
        if ($this->request->is('post')) {
            $property = $this->Properties->patchEntity($property, $this->request->getData());
            //pr($property);exit;
            $savedData  = $this->Properties->save($property);
                if ($savedData) {
                    $this->_uploadImage($this->request->data,$savedData->id);
                    $this->Flash->success(__('The property has been saved.'));
                    return $this->redirect(['action' => 'index']);
                } else { 
                    $this->Flash->error(__('The property could not be saved. Please, try again.'));
            }
           
        }
        $categories = $this->Properties->Categories->find('list', ['limit' => 200]);
        $emirates = $this->Properties->Emirates->find('list', ['limit' => 200]);
        $locations = $this->Properties->Locations->find('list', ['limit' => 200]);
        $subLocations = $this->Properties->SubLocations->find('list', ['limit' => 200]);
        $agents = $this->Properties->Agents->find('list', ['limit' => 200]);
        $propertyFeatures = $this->Properties->PropertyFeatures->find('list', ['limit' => 200]);
        $this->set(compact('property', 'categories', 'emirates', 'locations', 'subLocations', 'agents', 'propertyFeatures'));
        $this->set('_serialize', ['property']);
    }

    private function _uploadImage($data,$id){
            if (isset($data['image']['tmp_name']) && $data['image']['tmp_name']) {
            //print_r(WWW_ROOT . 'files' . DS .  $id.'.jpg'); exit;
            move_uploaded_file($data['image']['tmp_name'], WWW_ROOT . 'img' . DS . 'property' . DS . $id . '.jpg');

            }
        }
    /**
     * Edit method
     *
     * @param string|null $id Property id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $property = $this->Properties->get($id, [
            'contain' => ['PropertyFeatures']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $property = $this->Properties->patchEntity($property, $this->request->getData());
            if ($this->Properties->save($property)) {
                $this->Flash->success(__('The property has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The property could not be saved. Please, try again.'));
        }
        $categories = $this->Properties->Categories->find('list', ['limit' => 200]);
        $emirates = $this->Properties->Emirates->find('list', ['limit' => 200]);
        $locations = $this->Properties->Locations->find('list', ['limit' => 200]);
        $subLocations = $this->Properties->SubLocations->find('list', ['limit' => 200]);
        $agents = $this->Properties->Agents->find('list', ['limit' => 200]);
        $propertyFeatures = $this->Properties->PropertyFeatures->find('list', ['limit' => 200]);
        $this->set(compact('property', 'categories', 'emirates', 'locations', 'subLocations', 'agents', 'propertyFeatures'));
        $this->set('_serialize', ['property']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Property id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $property = $this->Properties->get($id);
        if ($this->Properties->delete($property)) {
            $this->Flash->success(__('The property has been deleted.'));
        } else {
            $this->Flash->error(__('The property could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function get_location_gmap() {
        $this->loadModel('Locations');
        $locationsList = $this->Locations->find( 'all', array('recursive' => - 1,'conditions' => array('longitude >' => 0,
        'latitude >' => 0)));
       // pr($locationsList);

        $locations = array();

        foreach ( $locationsList as $location ) {
            $locations[] = array(

            'name' => $location['name'],

            'latitude' => $location['latitude'],

            'longitude' => $location['longitude'],

            );

        }
        echo json_encode( $locations );

        exit;

        // pr($locations); exit;

    }

    public function properties_map( $longitude1, $longitude2, $latitude1, $latitude2 ) {

        $conditions = $this->Session->read( 'Property.Conditions' );

        //pr($conditions); exit;

        App::import( 'Helper', 'Property' );

        //$view = $this->View();

        $propertyHelper = new PropertyHelper( new View() );

        //pr($propertyHelper); exit;

        $conditionsLocation = array(

            'Property.longitude > ' => $longitude1,

            'Property.longitude < ' => $longitude2,

            'Property.latitude > ' => $latitude1,

            'Property.latitude < ' => $latitude2,

        );

        $conditions = am( $conditions, $conditionsLocation );

        //pr($conditions); exit;

        $properties = $this->Property->find( 'all',

            array(

                'recursive' => 1,

                'limit' => 30,

                'conditions' => $conditions,

            )

        );

        $newProperties = array();

        if ( ! empty( $properties ) ) {


            foreach ( $properties as $property ) {

                $newProperties[] = array(

                    'price' => $propertyHelper->getProperyPrice( $property['Property']['price'] ),

                    'price_show' => $this->restyle_text( $property['Property']['price'] * $this->currencyConveror ),

                    'property_link' => $propertyHelper->getPropertyURL( $property ),

                    'sub_location' => $property['SubLocation']['name'],

                    'sub_location_id' => $property['SubLocation']['id'],

                    'location' => $property['Location']['name'],

                    'Emirate' => $property['Emirate']['name'],

                    'bua' => number_format( $property['Property']['bua'] ),

                    'image' => isset( $property['Photo'][0]['image_url'] ) ? $property['Photo'][0]['image_url'] : '',

                    'reference_no' => $property['Property']['reference_no'],

                    'longitude' => $property['Property']['longitude'],

                    'latitude' => $property['Property']['latitude'],

                    'is_shown' => 0,

                );

            }

        }

        // pr($newProperties); exit;

        echo json_encode( $newProperties );

        exit;

    }

    private function restyle_text( $input ) {

        $input = number_format( $input );

        $input_count = substr_count( $input, ',' );

        if ( $input_count != '0' ) {

            if ( $input_count == '1' ) {

                return substr( $input, 0, - 4 ) . ' K';

            } else if ( $input_count == '2' ) {

                return substr( $input, 0, - 8 ) . ' M';

            } else if ( $input_count == '3' ) {

                return substr( $input, 0, - 12 ) . ' B';

            } else {

                return;

            }

        } else {

            return $input;

        }

    }

}
